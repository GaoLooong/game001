#ifndef _TYPECHOOSE_H__
#define _TYPECHOOSE_H__
#include"cocos2d.h"
#include"HeroChoose.h"
class TypeScene :public cocos2d::Scene
{
public:
	static cocos2d::Scene* createTypeScene();

	virtual bool init();

	HeroChooseScene *herochoose_scene;
	void AloneButtonCallBack(cocos2d::Ref* pSender);
	void OnlineButtonCallBack(cocos2d::Ref* pSender);

	CREATE_FUNC(TypeScene);
};
#endif // _TYPECHOOSE_H__

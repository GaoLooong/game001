#include "cocos2d.h"

USING_NS_CC;

class Tower:public cocos2d::CCNode
{
public:
	Sprite* tower;
	cocos2d::Sprite* enemy_hero;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	Sprite* mubiao_sprite;

	float hpLimit;//最大血量
	float hp_now;//现在血量
	bool is_attacking;
	bool if_check_hurt;
	void init_tower(int x,int y);//用来初始化防御塔
	Sprite* Tower::get_sprite();
	Rect Tower::get_attack_rect();
	Rect Tower::get_attacked_rect();
	int mubiao;
	void Tower::getBloodbar(Sprite* tower, float hpLimit, float hp_now);
	Vec2 Tower::get_position();
	void Tower::hurt_check();
	void Tower::attack(Sprite* enemy);
	CREATE_FUNC(Tower);
};
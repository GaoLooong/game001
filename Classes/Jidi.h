#include "cocos2d.h"

USING_NS_CC;

class Jidi :public cocos2d::CCNode
{
public:
	Sprite* jidi;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	float hpLimit;
	float hp_now;

	void init_jidi(int x, int y);//������ʼ��������
	Sprite* Jidi::get_sprite();
	Rect Jidi::get_attack_rect();
	Rect Jidi::get_attacked_rect();
	void getBloodbar(Sprite* jidi, float hpLimit, float hp_now);

	Vec2 Jidi::get_position();
	CREATE_FUNC(Jidi);
};

#include "Hero.h"


void Hero::init_hero(int x, int y)
{
	hero = Sprite::create("hero/hero.png");
	hero->setPosition(Vec2(x, y));

	//iv = 200;//iv是移动速度
	//hpLimit = hp_now = 10000;
	EXPLimit = 1000;
	EXP_now = 0;
	level_my="0";
	if_check_hurt = true;
	//level = "0";
	rect_attack = Rect(hero->getPositionX()-200 , hero->getPositionY()-200 , 400, 400);
	rect_attacked = Rect(hero->getPositionX()-50 , hero->getPositionY()-50 , 100, 100);
	this->getBloodbar(hero, hpLimit, hp_now);
	this->getEXPbar(hero, EXPLimit, EXP_now);
	addChild(hero);
}

Vec2 Hero::get_position()
{
	Vec2 bing_position = hero->getPosition();
	return bing_position;
}

Rect Hero::get_attack_rect()
{
	rect_attack = Rect(hero->getPositionX() - 200, hero->getPositionY() - 200, 400, 400);
	return rect_attack;
}
Rect Hero::get_attacked_rect()
{
	rect_attacked = Rect(hero->getPositionX() - 50, hero->getPositionY() - 50, 100, 100);
	return rect_attacked;
}
Sprite*Hero::get_sprite()
{
	return hero;
}

void Hero::getBloodbar(Sprite* hero, float hpLimit, float hp_now)
{
	Sprite *pBloodbeiSp = Sprite::create("pbeiSp.png");
	pBloodbeiSp->setPosition(Vec2(hero->getPositionX() - 150, hero->getPositionY() + 135));
	hero->addChild(pBloodbeiSp);
	Sprite *pBloodqianSp = Sprite::create("xuetiao.png");
	ProgressTimer *pBloodProGress = ProgressTimer::create(pBloodqianSp);
	pBloodProGress->setType(kCCProgressTimerTypeBar);
	pBloodProGress->setBarChangeRate(Vec2(1, 0));
	pBloodProGress->setMidpoint(Vec2(0, 0.5));
	pBloodProGress->setPosition(Vec2(hero->getPositionX() - 150, hero->getPositionY() + 135));
	pBloodProGress->setPercentage(100);
	hero->addChild(pBloodProGress, 1, 110); //血条tag设置为110

}

void Hero::hurt_check()
{

	auto progress = (ProgressTimer*)hero->getChildByTag(110);
	progress->setPercentage((hp_now / hpLimit) * 100); //

	if (progress->getPercentage() < 0)
	{
		if_check_hurt = false;

	}
}

void Hero::getEXPbar(Sprite* hero, float EXPLimit, float EXP_now)
{
	Sprite *pEXPbeiSp = Sprite::create("pbeiSp.png");
	pEXPbeiSp->setPosition(Vec2(hero->getPositionX() - 150, hero->getPositionY() + 190));
	hero->addChild(pEXPbeiSp);
	Sprite *pEXPqianSp = Sprite::create("xuetiao.png");
	ProgressTimer *pEXPProGress = ProgressTimer::create(pEXPqianSp);
	pEXPProGress->setType(kCCProgressTimerTypeBar);
	pEXPProGress->setBarChangeRate(Vec2(1, 0));
	pEXPProGress->setMidpoint(Vec2(0, 0.5));
	pEXPProGress->setPosition(Vec2(hero->getPositionX() - 150, hero->getPositionY() + 190));
	pEXPProGress->setPercentage(0);
	hero->addChild(pEXPProGress, 1, 210); //tag设置为110

}
int Hero::get_direction(int x, int y)//获得 行走的 方向
{
	/////////////////1-8是从12点方向顺时针//////////////
	int idirection = 0;
	if (x == 0 || y == 0)
		idirection = 1;
	else if (x > 0 && x * x / (y * y) > 10)		//RIGHT
		idirection = 3;
	else if (x < 0 && x * x / (y * y)>10)		//LEFT
		idirection = 7;
	else if (y > 0 && y * y / (x * x) > 10)	 //UP
		idirection = 1;
	else if (y < 0 && y * y / (x * x)  >10)	//DOWN
		idirection = 5;
	else if (x > 0 && y < 0)					    //RIGHT-DOWN
		idirection = 4;
	else if (x < 0 && y < 0)					   //LEFT-DOWN
		idirection = 6;
	else if (x < 0 && y > 0)			          //LEFT-UP
		idirection = 8;
	else if (x > 0 && y > 0)					 //RIGHT-UP
		idirection = 2;

	direction = idirection;
	return idirection;
}
Animate* Hero::create_animate_walk(int direction, const char* action, float time)//建立动作类动画
{
	Animation* animation = Animation::create();

	//Vector <SpriteFrame*> frameArray;
	if (action == "run")//奔跑时的动画
	{
		char  file_name[20] = { 0 };
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "hero/run_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else//八个方向上的最后站立
	{
		char  file_name[20] = { 0 };
		sprintf(file_name, "hero/stop_%d.png", direction);
		animation->addSpriteFrameWithFile(file_name);
	}

	animation->setDelayPerUnit(0.1f);
	if (action == "stop")
		animation->setLoops(1);
	else
		animation->setLoops(time / 0.8);//几张图片的走路动画就/0.1*几

	Animate* animate = Animate::create(animation);
	return animate;
}
Animate* Hero::create_animate_skill(float iv, float juli, int skill_kind)
{
	Animation* animation = Animation::create();
	char  file_name[20] = { 0 };
	if (skill_kind == 0)
	{
		sprintf(file_name, "skill/skill_0.png");
		animation->addSpriteFrameWithFile(file_name);
	}

	if (skill_kind == 1)
	{
		for (int i = 0; i < 2; i++)
		{
			sprintf(file_name, "skill/skill_%d.png", i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	/*if (skill_kind == 2)
	{

		sprintf(file_name, "skill/skill_2.png");
		animation->addSpriteFrameWithFile(file_name);

	}*/
	if (skill_kind == 3)
	{
		sprintf(file_name, "skill/skill_3.png");
		animation->addSpriteFrameWithFile(file_name);
	}
	animation->setDelayPerUnit(0.1f);
	animation->setLoops(juli * 5 / iv);
	Animate* animate = Animate::create(animation);
	return animate;

}

void Hero::animate_aoe()
{
	Sprite* skill = Sprite::create("skill/aoe/0.png");
	skill->setPosition(hero->getPosition());
	addChild(skill);
	Animation* animation = Animation::create();
	char  file_name[50] = { 0 };
	for (int i = 0; i < 9; i++)
	{
		sprintf(file_name, "skill/aoe/%d.png",i);
		animation->addSpriteFrameWithFile(file_name);
	}
	animation->setDelayPerUnit(0.1f);
	animation->setLoops(1);
	Animate* animate = Animate::create(animation);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(animate, xiaoshi, NULL);

	skill->runAction(sequence);
}

void Hero::get_level_my(Sprite* hero, std::string level_my)
{
	Label *label = Label::createWithTTF(level_my, "fonts/Marker Felt.ttf", 30);
	label->setPosition(Vec2(hero->getPositionX() - 230, hero->getPositionY() + 190));
	hero->addChild(label, 3, 150);//!!!!!!!!!!!!
}

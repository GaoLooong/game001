#include"Victory.h"
#include"HelloWorldScene.h"
#include"SimpleAudioEngine.h"

USING_NS_CC;

Scene* VictoryScene::createVictoryScene()
{
	return VictoryScene::create();
}

static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool VictoryScene::init()
{
	if (!Scene::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto background = Sprite::create("victory.jpg");
	if (background == nullptr)
	{
		problemLoading("'victory.jpg'");
	}
	auto size = background->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	background->setScale(scale);
	background->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(background);

	auto ok_button = MenuItemImage::create("ok1.png", "ok2.png",
		CC_CALLBACK_1(VictoryScene::OkButtonCallBack, this));
	ok_button->setPosition(Vec2(400, 80));

	Menu* ok_menu = Menu::create(ok_button, NULL);
	ok_menu->setPosition(Vec2::ZERO);
	this->addChild(ok_menu);
	return true;
}



void VictoryScene::OkButtonCallBack(Ref* pSender)
{
	auto sc = HelloWorld::createScene();
	auto reScene = TransitionFadeUp::create(1.0f, sc);
	Director::getInstance()->replaceScene(reScene);
}
#ifndef __Setting_SCENE_H__
#define __Setting_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

#define SOUND_KEY "sound_key"
#define MUSIC_KEY "music_key"
class Setting : public cocos2d::Scene
{
	bool isEffect;
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void BackButtonCallback(cocos2d::Ref* sPender);
	void menuSoundToggleCallback(cocos2d::Ref* pSender);
	void menuMusicToggleCallback(cocos2d::Ref* pSender);





	//void menuItem2Callback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(Setting);
};


#endif // __Setting_SCENE_H__
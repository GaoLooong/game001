#include"TypeChoose.h"
#include"HelloWorldScene.h"
#include"SimpleAudioEngine.h"
#include"HeroChoose.h"
#include"GameOnline.h"

USING_NS_CC;

Scene* TypeScene::createTypeScene()
{
	return TypeScene::create();
}

static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool TypeScene::init()
{
	if (!Scene::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto background = Sprite::create("type.jpg");
	if (background == nullptr)
	{
		problemLoading("'type.jpg'");
	}
	auto size = background->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	background->setScale(scale);
	background->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(background);

	auto alone_button = MenuItemImage::create("danji1.jpg", "danji2.jpg",
		CC_CALLBACK_1(TypeScene::AloneButtonCallBack, this));
	alone_button->setPosition(Vec2(400,500));
	alone_button->setScaleX(3.5f);
	alone_button->setScaleY(3.5f);

	auto online_button = MenuItemImage::create("lianji1.jpg", "lianji2.jpg",
		CC_CALLBACK_1(TypeScene::OnlineButtonCallBack, this));
	online_button->setPosition(Vec2(1600, 500));
	online_button->setScaleX(3.6f);
	online_button->setScaleY(3.6f);
	 
	Menu* menu = Menu::create(alone_button,online_button,NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);
	return true;
}



void TypeScene::AloneButtonCallBack(Ref* pSender)
{
    auto herochoose_scene = HeroChooseScene::createHeroChooseScene();
	auto reScene = TransitionFadeDown::create(2.0f,herochoose_scene);
	Director::getInstance()->replaceScene(reScene);
}

void TypeScene::OnlineButtonCallBack(Ref* pSender)
{
	auto scene = GameOnline::createScene();
	auto reScene = TransitionFadeDown::create(2.0f, scene);
	Director::getInstance()->replaceScene(reScene);
}
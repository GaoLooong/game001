#ifndef __Game_SCENE_H__
#define __Game_SCENE_H__

#include "cocos2d.h"
#include"Tower.h"
#include "Hero.h"
#include"Bing.h"
#include"HeroAi.h"
#include"Jidi.h"
#include"TowerEnemy.h"
#include"JidiEnemy.h"
#include"BingEnemy.h"
#include"zhanji.h"
#include"coin.h"
USING_NS_CC;


class Game : public cocos2d::Scene
{


public:
	static cocos2d::Scene* createScene();

	cocos2d::Vec2 runto_enemy;
	cocos2d::Vec2 runto_my;

	cocos2d::TMXLayer* pengzhuang_layer;
	cocos2d::TMXTiledMap* tile_map;

	Vector<Bing*> bing_my_vector;
	Vector<BingEnemy*> bing_enemy_vector;

	std::string level;
	//cocos2d::Sprite* hero_my;
	//cocos2d::Sprite* hero_enemy;

	Hero* hero_my;

	HeroAi*hero_enemy;

	//cocos2d::Sprite* tower_my;
	//cocos2d::Sprite* tower_enemy;
	TowerEnemy* tower_enemy;
	Tower* tower_my;

	JidiEnemy* jidi_enemy;
	Jidi* jidi_my;


	BingEnemy* bing_enemy;
	Bing* bing_my;

	//cocos2d::Sprite* bing_my;
	//cocos2d::Sprite* bing_enemy;
	Coin* coin_my;
	Zhanji* zhanji_my;

	cocos2d::Vec2 last_position;
	cocos2d::Vec2 touch_position;

	cocos2d::Vec2 tileCoordFromPosition(cocos2d::Vec2 position);

	bool  is_walk;

	bool if_attack;


	virtual bool init();

	//virtual void onEnter();

	//virtual void update(float dt);

	bool touchBegin(cocos2d::Touch* touch, cocos2d::Event* event);
	bool kb_begin(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void pengzhuang_check();
	bool if_peng_stop();

	void set_hero_map();
	void set_jianting();

	void updateView(float dt);

	void rect_check_tower(float dt);
	void rect_check_bing(float dt);
	void rect_check_heroAi(float dt);

	void Game::bing_shuaxin(float dt);

	void Game::hurt_check(float dt);
	void Game::hurt_check_hero_my();
	void Game::hurt_check_hero_enemy();
	void Game::hurt_check_bing_my();
	void Game::hurt_check_bing_enemy();
	void Game::hurt_check_tower_my();
	void Game::hurt_check_tower_enemy();
	void Game::hurt_check_jidi_my();
	void Game::hurt_check_jidi_enemy();


	void Game::get_coin_by_killing_bing(float dt);
	void Game::get_coin_by_killing_tower(float dt);
	void Game::get_coin_by_killing_hero(float dt);
	void Game::get_coin_by_time(float dt);

	void hero_attack(Vec2 attack, Vec2 attacked);

	bool Game::rect_contain_bing_enemy(Bing*it_attack);
	bool Game::rect_contain_bing_my(BingEnemy*it_attack);
	bool Game::Hero_ai_attack_bing(HeroAi*heri_ai);

	Animate* Game::animate_dead(int tag,int direction);

	void tower_attack_enemy();
	void tower_attack_my();

	void bing_attack_enemy();
	void bing_attack_my();
    void heroAi_attack();
	void EXP_change(float dt);
	
	void EXP_check(float dt);
	////////////////商店
	void menuShopItemOpenCallback(cocos2d::Ref* pSender);         //商店按钮的回调函数
	void menuShopItemCloseCallback(cocos2d::Ref* pSender);        //商店按钮的回调函数
	void buyDaoCallBack1(cocos2d::Ref* pSender);
	void buyDaoCallBack2(cocos2d::Ref* pSender);
	void buyDaoCallBack3(cocos2d::Ref* pSender);
	void buyDunCallBack1(cocos2d::Ref* pSender);
	void buyDunCallBack2(cocos2d::Ref* pSender);
	void buyGongCallBack1(cocos2d::Ref* pSender);
	void buyGongCallBack2(cocos2d::Ref* pSender);
	void buyKaiCallBack1(cocos2d::Ref* pSender);
	void buyKaiCallBack2(cocos2d::Ref* pSender);
	void buyKaiCallBack3(cocos2d::Ref* pSender);
	void buyKaiCallBack4(cocos2d::Ref* pSender);
	void buyKaiCallBack5(cocos2d::Ref* pSender);
	void buyShoeCallBack1(cocos2d::Ref* pSender);
	void buyShoeCallBack2(cocos2d::Ref* pSender);
	void buyShoeCallBack3(cocos2d::Ref* pSender);
	void buyFaCallBack1(cocos2d::Ref* pSender);
	void buyFaCallBack2(cocos2d::Ref* pSender);

    bool if_skill_kind_1;
	bool if_skill_kind_2;
	bool if_skill_kind_3;
	bool if_skill_kind_0;
    void Game::get_skill0();
    void Game::get_skill1();
    void Game::get_skill2();
    void Game::get_skill3();
    void Game::get_skill_cd0(float dt);
    void Game::get_skill_cd1(float dt);
    void Game::get_skill_cd2(float dt);
    void Game::get_skill_cd3(float dt);
    void get_time();
	void Game::get_timename();
	CREATE_FUNC(Game);
};


#endif // __Setting_SCENE_H__#pragma once;

#pragma once
#ifndef _COIN_H_
#define _COIN_H_

#include "cocos2d.h"
#include <string>

USING_NS_CC;

class Coin :public cocos2d::CCNode
{
public:
	Sprite* coin;
	void init_coin(int x, int y);
	std::string number;
	void get_coin(std::string number);

	CREATE_FUNC(Coin);
};

#endif // _COIN_H_


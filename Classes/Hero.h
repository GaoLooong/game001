#ifndef __HERO_H__
#define __HERO_H__

#include "cocos2d.h"
#include "string"
USING_NS_CC;

class Hero:public cocos2d::CCNode
{
public:
	Sprite* hero;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	bool if_check_hurt;
	float iv;
	float hpLimit;//最大血量
	float hp_now;//现在血量
	int direction;
	void init_hero(int x, int y);//用来初始化防御塔
	Vec2 get_position();
	Rect get_attack_rect();
	Rect get_attacked_rect();
	Sprite* get_sprite();
	void getBloodbar(Sprite* hero, float hpLimit, float hp_now);
	CREATE_FUNC(Hero);
	int mubiao;
	float EXPLimit;
	float EXP_now;

	void getEXPbar(Sprite* hero, float EXPLimit, float EXP_now);
	void Hero::hurt_check();
	void Hero::restart(int rex, int rey);
	int Hero::get_direction(int x, int y);
	Animate* Hero::create_animate_walk(int direction, const char* action, float time);
	Animate* Hero::create_animate_skill(float iv, float juli, int skill_kind);
	void Hero::animate_aoe();
	Point restartp;//重生点
	
	void Hero::get_level_my(Sprite* hero, std::string level_my);
	std::string level_my;

};
#endif // __HERO_H__

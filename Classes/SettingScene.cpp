#include "HelloWorldScene.h"
#include "SettingScene.h"

USING_NS_CC;
using namespace CocosDenshion;

Scene* Setting::createScene()
{
	return Setting::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool Setting::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
//////////添加自己的代码
	auto background = Sprite::create("setting background.jpg");
	if (background == nullptr)
	{
		problemLoading("'setting background.jpg'");
	}
	auto size = background->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	background->setScale(scale);
	background->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(background,0);
	auto effect= Sprite::create("effect.png");
	effect->setPosition(Vec2(900, 800));
	auto music= Sprite::create("music.png");
	music->setPosition(Vec2(900, 600));
	this->addChild(effect,1);
	this->addChild(music, 1);
	auto soundOnMenuItem = MenuItemImage::create(
		"on1.png",
		"on1.png");
	auto soundOffMenuItem = MenuItemImage::create(
		"off1.png",
		"off1.png");

	auto soundToggleMenuItem = MenuItemToggle::createWithCallback(
		CC_CALLBACK_1(Setting::menuSoundToggleCallback, this),
		soundOnMenuItem,
		soundOffMenuItem,
		NULL);
	 soundToggleMenuItem->setPosition(Vec2(1300,800));


	 auto musicOnMenuItem = MenuItemImage::create(
		 "on1.png",
		 "on1.png");
	 auto musicOffMenuItem = MenuItemImage::create(
		 "off1.png",
		 "off1.png");
	 auto musicToggleMenuItem = MenuItemToggle::createWithCallback(
		 CC_CALLBACK_1(Setting::menuMusicToggleCallback, this),
		 musicOnMenuItem,
		 musicOffMenuItem,
		 NULL);
	 musicToggleMenuItem->setPosition(Vec2(1300,600));
	
	//返回按钮
	auto back_button = MenuItemImage::create("back_button1.png",
		"back_button2.png",
		CC_CALLBACK_1(Setting::BackButtonCallback,this));
	back_button->setPosition(Vec2(400,80));

	Menu* setting_menu = Menu::create(soundToggleMenuItem, musicToggleMenuItem,back_button, NULL);
	
	setting_menu->setPosition(Vec2::ZERO);

	this->addChild(setting_menu,1);
	UserDefault *defaults = UserDefault::getInstance();


	if (defaults->getBoolForKey(MUSIC_KEY)) {
			musicToggleMenuItem->setSelectedIndex(0);                                
	}
	else {
		musicToggleMenuItem->setSelectedIndex(1);                                
	}


	if (defaults->getBoolForKey(SOUND_KEY)) {
		
			soundToggleMenuItem->setSelectedIndex(0);                               
	}
	else {
		soundToggleMenuItem->setSelectedIndex(1);                               
	}

	return true;
}
void Setting::BackButtonCallback(Ref*pSender)
{
	Director::getInstance()->popScene();
	if (isEffect) {
		SimpleAudioEngine::getInstance()->playEffect("effect.wav");					
	}
}

void Setting::menuSoundToggleCallback(Ref* pSender)						
{
	auto soundToggleMenuItem = (MenuItemToggle*)pSender;
	log("soundToggleMenuItem %d", soundToggleMenuItem->getSelectedIndex());


	UserDefault *defaults = UserDefault::getInstance();
	if (defaults->getBoolForKey(SOUND_KEY)) {
			defaults->setBoolForKey(SOUND_KEY, false);                               
	}
	else {
		defaults->setBoolForKey(SOUND_KEY, true);                                
			SimpleAudioEngine::getInstance()->playEffect("effect.wav");                  
	}
}


void Setting::menuMusicToggleCallback(Ref* pSender)							
{
	auto musicToggleMenuItem = (MenuItemToggle*)pSender;
	log("musicToggleMenuItem %d", musicToggleMenuItem->getSelectedIndex());

	UserDefault *defaults = UserDefault::getInstance();
	if (defaults->getBoolForKey(MUSIC_KEY)) {
			defaults->setBoolForKey(MUSIC_KEY, false);
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	}
	else {
		defaults->setBoolForKey(MUSIC_KEY, true);
		SimpleAudioEngine::getInstance()->playBackgroundMusic("sound.mp3");
	}
}





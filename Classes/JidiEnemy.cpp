#include "JidiEnemy.h"

void JidiEnemy::init_jidi(int x, int y)
{
	jidi = Sprite::create("jidi/jidi.png");
	jidi->setPosition(Vec2(x, y));
	hpLimit = hp_now = 2000;

	rect_attack = Rect(jidi->getPositionX() - 200, jidi->getPositionY() - 200, 400, 400);
	rect_attacked = Rect(jidi->getPositionX() - 100, jidi->getPositionY() - 100, 200, 200);

	this->getBloodbar(jidi, hpLimit, hp_now);
	addChild(jidi);
}

Vec2 JidiEnemy::get_position()
{
	Vec2 tower_position = jidi->getPosition();
	return tower_position;
}
Rect JidiEnemy::get_attack_rect()
{
	rect_attack = Rect(jidi->getPositionX() - 200, jidi->getPositionY() - 200, 400, 400);
	return rect_attack;
}
Rect JidiEnemy::get_attacked_rect()
{
	rect_attacked = Rect(jidi->getPositionX() - 60, jidi->getPositionY() - 60, 120, 120);
	return rect_attacked;
}
Sprite* JidiEnemy::get_sprite()
{
	return jidi;
}
void JidiEnemy::getBloodbar(Sprite* jidi, float hpLimit, float hp_now)
{
	Sprite *pBloodbeiSp = Sprite::create("pbeiSp.png");
	pBloodbeiSp->setPosition(Vec2(jidi->getPositionX() - 150, jidi->getPositionY() + 135));
	jidi->addChild(pBloodbeiSp);
	Sprite *pBloodqianSp = Sprite::create("xuetiao.png");
	ProgressTimer *pBloodProGress = ProgressTimer::create(pBloodqianSp);
	pBloodProGress->setType(kCCProgressTimerTypeBar);
	pBloodProGress->setBarChangeRate(Vec2(1, 0));
	pBloodProGress->setMidpoint(Vec2(0, 0.5));
	pBloodProGress->setPosition(Vec2(jidi->getPositionX() - 150, jidi->getPositionY() + 135));
	pBloodProGress->setPercentage(100);
	jidi->addChild(pBloodProGress, 1, 110); //Ѫ��tag����Ϊ110
}
#include "Bing.h"


void Bing::init_bing(int x, int y, float runto_x, float runto_y, int itag)
{
	if (itag % 3 == 0)
	{
		bing = Sprite::create("paobing/stand/stand_2.png");

		bing->setPosition(Vec2(x, y));
		hpLimit = hp_now = 30;

		iv = 60;//iv是移动速度

		rect_attack = Rect(bing->getPositionX() - 160, bing->getPositionY() - 160, 320, 320);
		rect_attacked = Rect(bing->getPositionX() - 30, bing->getPositionY() - 30, 60, 60);
		this->getBloodbar(bing, hpLimit, hp_now);
	}
	else if ((itag + 1) % 3 == 0)
	{
		bing = Sprite::create("yuanbing/stand/stand_2.png");

		bing->setPosition(Vec2(x, y));
		hpLimit = hp_now = 30;

		iv = 60;//iv是移动速度

		rect_attack = Rect(bing->getPositionX() - 120, bing->getPositionY() - 120, 240, 240);
		rect_attacked = Rect(bing->getPositionX() - 30, bing->getPositionY() - 30, 60, 60);
		this->getBloodbar(bing, hpLimit, hp_now);
	}
	else
	{
		bing = Sprite::create("jinbing/stand/stand_2.png");

		bing->setPosition(Vec2(x, y));
		hpLimit = hp_now = 30;

		iv = 60;//iv是移动速度

		rect_attack = Rect(bing->getPositionX() - 80, bing->getPositionY() - 80, 160, 160);
		rect_attacked = Rect(bing->getPositionX() - 30, bing->getPositionY() - 30, 60, 60);
		this->getBloodbar(bing, hpLimit, hp_now);
	}
	tag = itag;
	is_attacking = false;
	if_check_hurt = true;
	runto_position.x = runto_x;
	runto_position.y = runto_y;
	direction = this->get_direction(runto_x - x, runto_y - y);

	addChild(bing);
}

Vec2 Bing::get_position()
{
	Vec2 bing_position = bing->getPosition();
	return bing_position;
}

int Bing::get_direction(int x, int y)//获得 行走的 方向
{
	/////////////////1-8是从12点方向顺时针//////////////
	int idirection = 0;
	if (x == 0 || y == 0)
		idirection = 1;
	else if (x > 0 && x * x / (y * y) > 10)		//RIGHT
		idirection = 3;
	else if (x < 0 && x * x / (y * y)>10)		//LEFT
		idirection = 7;
	else if (y > 0 && y * y / (x * x) > 10)	 //UP
		idirection = 1;
	else if (y < 0 && y * y / (x * x)  >10)	//DOWN
		idirection = 5;
	else if (x > 0 && y < 0)					    //RIGHT-DOWN
		idirection = 4;
	else if (x < 0 && y < 0)					   //LEFT-DOWN
		idirection = 6;
	else if (x < 0 && y > 0)			          //LEFT-UP
		idirection = 8;
	else if (x > 0 && y > 0)					 //RIGHT-UP
		idirection = 2;

	direction = idirection;
	return idirection;
}

Animate* Bing::animate_walk(int idirection, const char* action, float time)//建立动作类动画
{
	Animation* animation = Animation::create();

	if (action == "run")//奔跑时的动画
	{
		char  file_name[50] = { 0 };
		if (tag % 3 == 0)
		{
			for (int i = 0; i < 16; i++)
			{
				sprintf(file_name, "paobing/run/run_%d_%d.png", idirection, i);
				animation->addSpriteFrameWithFile(file_name);
			}
		}
		else if ((tag + 1) % 3 == 0)
		{
			for (int i = 0; i < 16; i++)
			{
				sprintf(file_name, "yuanbing/run/run_%d_%d.png", idirection, i);
				animation->addSpriteFrameWithFile(file_name);
			}
		}
		else
		{
			for (int i = 0; i < 16; i++)
			{
				sprintf(file_name, "jinbing/run/run_%d_%d.png", idirection, i);
				animation->addSpriteFrameWithFile(file_name);
			}
		}
	}
	else//八个方向上的最后站立
	{
		char  file_name[50] = { 0 };
		if (tag % 3 == 0)
			sprintf(file_name, "paobing/stand/stand_%d.png", idirection);
		else if ((tag + 1) % 3 == 0)
			sprintf(file_name, "yuanbing/stand/stand_%d.png", idirection);
		else
			sprintf(file_name, "jinbing/stand/stand_%d.png", idirection);
		animation->addSpriteFrameWithFile(file_name);
	}

	animation->setDelayPerUnit(0.05f);

	if (action == "stop")
		animation->setLoops(1);
	else
		animation->setLoops(time / 1.6);

	Animate* animate = Animate::create(animation);
	return animate;
}

void Bing::run(Vec2 position)
{
	bing->stopAllActions();
	Vec2 bing_position = bing->getPosition();
	Vec2 diff = position - bing_position;
	float juli = sqrt(diff.x * diff.x + diff.y * diff.y);

	auto* move_action = MoveTo::create(juli / iv, position);
	auto* run_animate = animate_walk(get_direction(diff.x, diff.y), "run", juli / iv);
	auto* stop_animate = animate_walk(get_direction(diff.x, diff.y), "stop", 0);
	auto* sequence = Sequence::create(move_action, stop_animate, NULL);
	bing->runAction(run_animate);
	bing->runAction(sequence);
}




void Bing::auto_runto()
{
	bing->stopAllActions();
	run(runto_position);
}

Rect Bing::get_attack_rect()
{
	if (tag % 3 == 0)
		rect_attack = Rect(bing->getPositionX() - 160, bing->getPositionY() - 160, 320, 320);
	else if ((tag + 1) % 3 == 0)
		rect_attack = Rect(bing->getPositionX() - 120, bing->getPositionY() - 120, 240, 240);
	else
		rect_attack = Rect(bing->getPositionX() - 80, bing->getPositionY() - 80, 160, 160);

	return rect_attack;
}
Rect Bing::get_attacked_rect()
{
	rect_attacked = Rect(bing->getPositionX() - 30, bing->getPositionY() - 30, 60, 60);
	return rect_attacked;
}

Sprite* Bing::get_sprite()
{
	return bing;
}
void Bing::getBloodbar(Sprite* bing, float hpLimit, float hp_now)
{
	Sprite *pBloodbeiSp = Sprite::create("pbeiSp.png");
	pBloodbeiSp->setPosition(Vec2(bing->getPositionX() - 500, bing->getPositionY() -200));
	bing->addChild(pBloodbeiSp);
	Sprite *pBloodqianSp = Sprite::create("xuetiao.png");
	ProgressTimer *pBloodProGress = ProgressTimer::create(pBloodqianSp);
	pBloodProGress->setType(kCCProgressTimerTypeBar);
	pBloodProGress->setBarChangeRate(Vec2(1, 0));
	pBloodProGress->setMidpoint(Vec2(0, 0.5));
	pBloodProGress->setPosition(Vec2(bing->getPositionX() - 500, bing->getPositionY() -200));
	pBloodProGress->setPercentage(100);
	bing->addChild(pBloodProGress, 1, 110); //血条tag设置为110

}
void Bing::hurt_check()
{

	auto progress = (ProgressTimer*)bing->getChildByTag(110);
	progress->setPercentage((hp_now /hpLimit) * 100); //

	if (progress->getPercentage() < 0)
	{
		if_check_hurt = false;

	}
}
void Bing::attack(Sprite* enemy, Rect rect)
{
	Sprite* attacked = enemy;
	Rect  rect_attacked = rect;
	Vec2 attacked_position = attacked->getPosition();
	Vec2 bing_position = this->get_position();
	Vec2 diff = attacked_position - bing_position;//是触点和和hero之间的距离

	//bing_enemy->run(diff);

	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	if (rect_attacked.containsPoint(this->get_position()))//如果进入了对方受攻击范围,停止
	{
		bing->stopAllActions();
		auto* stop = this->animate_walk(this->get_direction(diff.x, diff.y), "stop", 0);
		this->get_sprite()->runAction(stop);

	}
	else
	{
		bing->stopAllActions();
		this->run(attacked_position);
	}

	bing->stopAllActions();///////////兵攻击动画
	Animation* animation = Animation::create();
	char  file_name[50] = { 0 };
	for (int i = 0; i < 8; i++)
	{
		if (tag % 3 == 0)//攻击动画
			sprintf(file_name, "paobing/attack/attack_%d_%d.png", direction, i);
		else if ((tag + 1) % 3 == 0)
			sprintf(file_name, "yuanbing/attack/attack_%d_%d.png", direction, i);
		else
			sprintf(file_name, "jinbing/attack/attack_%d_%d.png", direction, i);

		animation->addSpriteFrameWithFile(file_name);
	}
	if (tag % 3 == 0)//攻击动画
		animation->setDelayPerUnit(0.03f);
	else if ((tag + 1) % 3 == 0)
		animation->setDelayPerUnit(0.05f);
	else
		animation->setDelayPerUnit(0.05f);
	animation->setLoops(1);
	Animate* animate = Animate::create(animation);
	bing->runAction(animate);

	if (tag % 3 == 0)//技能动画
	{
		Sprite* skill = Sprite::create("paobing/skill/0.png");
		skill->setPosition(bing_position);
		addChild(skill);
		Animation* animation = Animation::create();
		char  file_name[50] = { 0 };
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "paobing/skill/%d.png", i);
			animation->addSpriteFrameWithFile(file_name);
		}
		animation->setDelayPerUnit(0.04f);
		animation->setLoops(1);
		Animate* animate = Animate::create(animation);
		auto* move_action = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
		auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

		skill->runAction(animate);
		skill->runAction(sequence);
	}
	else if ((tag + 1) % 3 == 0)
	{
		Sprite* skill = Sprite::create("yuanbing/skill/0.png");
		skill->setPosition(bing_position);
		addChild(skill);
		Animation* animation = Animation::create();
		char  file_name[50] = { 0 };
		for (int i = 0; i < 6; i++)
		{
			sprintf(file_name, "yuanbing/skill/%d.png", i);
			animation->addSpriteFrameWithFile(file_name);
		}
		animation->setDelayPerUnit(0.04f);
		animation->setLoops(1);
		Animate* animate = Animate::create(animation);
		auto* move_action = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
		auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

		skill->runAction(animate);
		skill->runAction(sequence);
	}

}

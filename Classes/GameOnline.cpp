﻿#include "HelloWorldScene.h"
#include "GameOnline.h"
#include"client.h"
#include"math.h"

USING_NS_CC;
using namespace std;

////////////////
// 1为我方英雄
// 2为敌方英雄
// 3为我方小兵
// 4为敌方小兵
// 5为我方防御塔
// 6为敌方防御塔
////////////////

Scene* GameOnline::createScene()
{
	return GameOnline::create();
}

static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool GameOnline::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	bing_enemy_tag = 0;
	bing_my_tag = 0;
	timecheck = "0";
	level = "0";
	levelenemy = "0";
	equip[9] = { 0 };
	equipNumber = 0;

	cdmax = 100.0000;
	cd1 = cd2 = cd3 = cd0 = 100.0000;
	power_wu_enemy = 100;
	power_wu_my = 100;
	power_fa_enemy = 100;
	power_fa_my = 100;
	defence_fa_my = defence_fa_enemy = defence_wu_my = defence_wu_enemy = 0;
	hp_my = 4000;
	hp_enemy = 4000;
	v_my = 70;
	v_enemy = 70;


	GameOnline::get_timename();

	GameOnline::get_skill0();

	GameOnline::get_skill1();

	GameOnline::get_skill2();

	GameOnline::get_skill3();

	this->schedule(schedule_selector(GameOnline::check_begin), 0.1f);

		this->schedule(schedule_selector(GameOnline::updateView), 0.01f);

		this->schedule(schedule_selector(GameOnline::rect_check_tower), 1.0f);

		this->schedule(schedule_selector(GameOnline::rect_check_bing), 0.8f);

		this->schedule(schedule_selector(GameOnline::hurt_check), 0.1f);

		this->schedule(schedule_selector(GameOnline::bing_shuaxin), 10.0f);

		this->schedule(schedule_selector(GameOnline::EXP_check), 1.0f);

		this->schedule(schedule_selector(GameOnline::EXP_change), 1.0f);


		this->schedule(schedule_selector(GameOnline::get_coin_by_time), 1.0f);

		this->schedule(schedule_selector(GameOnline::get_skill_cd0), 0.2f);

		this->schedule(schedule_selector(GameOnline::get_skill_cd1), 0.5f);

		this->schedule(schedule_selector(GameOnline::get_skill_cd2), 1.0f);

		this->schedule(schedule_selector(GameOnline::get_skill_cd3), 1.5f);

		this->schedule(schedule_selector(GameOnline::get_time_change), 1.0f);



	client1 = new client();
	id = client1->get_id();
	//log(id);
	if_begin = false;
	set_hero_map_0();
	
	return true;
}

void GameOnline::set_hero_map_0()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	////////////////创建地图
	tile_map = TMXTiledMap::create("map/map_tiled.tmx");//添加瓦片地图
	auto size = tile_map->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	tile_map->setScale(scale);

	addChild(tile_map, 0, 100);//将瓦片地图放在第0层

	TMXObjectGroup* group = tile_map->getObjectGroup("object");//获取对象层信息

	////////////////创建防御塔
	tower_enemy_ol = new TowerEnemy();
	tower_enemy_ol->init_tower(10, 10);
	this->addChild(tower_enemy_ol);//将塔放在三层

	tower_my_ol = new Tower();
	tower_my_ol->init_tower(10, 10);
	this->addChild(tower_my_ol);

	////////////////创建基地

	jidi_enemy_ol = new JidiEnemy();
	jidi_enemy_ol->init_jidi(10, 10);
	this->addChild(jidi_enemy_ol);//将塔放在二层

	jidi_my_ol = new Jidi();
	jidi_my_ol->init_jidi(10, 10);
	this->addChild(jidi_my_ol);

	/////////////创建英雄


	hero_my_ol = new Hero();
	hero_my_ol->init_hero(10, 10);
	hero_my_ol->hp_now = hp_my;
	this->addChild(hero_my_ol, 1);


	hero_enemy_ol = new Hero();
	hero_enemy_ol->init_hero(10, 10);
	hero_enemy_ol->hp_now = hp_my;
	this->addChild(hero_enemy_ol, 1);



	///////////////创建小兵
	for (int i = 0; i < 3; i++)
	{
		bing_enemy_ol = new BingEnemy();
		bing_enemy_tag++;
		bing_enemy_ol->init_bing(10,10,10,10,10);
		bing_enemy_vector.pushBack(bing_enemy_ol);
		this->addChild(bing_enemy_ol);
	}

	for (int i = 0; i < 3; i++)
	{
		auto x_runto =0;
		auto y_runto =0;
		bing_my_ol = new Bing();
		bing_my_tag++;
		bing_my_ol->init_bing(10,10,10,10,10);
		bing_my_vector.pushBack(bing_my_ol);

		this->addChild(bing_my_ol);
	}



	zhanji_my_ol = new Zhanji();
	zhanji_my_ol->init_zhanji(1900, 60);
	this->addChild(zhanji_my_ol, 2);

	coin_my_ol = new Coin();
	coin_my_ol->init_coin(60, 800);
	this->addChild(coin_my_ol, 10);

	///////////商店界面
	MenuItemImage* shop_button = MenuItemImage::create("shop_button1.png", "shop_button2.png",
		CC_CALLBACK_1(GameOnline::menuShopItemOpenCallback, this));//商店按钮
	shop_button->setPosition(Vec2(50, visibleSize.height - 50));
	shop_button->setScale(0.8f);

	auto menuShop = Menu::create(shop_button, NULL);
	menuShop->setPosition(Vec2::ZERO);
	this->addChild(menuShop, 1);
}

void GameOnline::set_jianting()
{
	//////////////触摸监听器
	auto touch = EventListenerTouchOneByOne::create();//触摸事件
	touch->onTouchBegan = CC_CALLBACK_2(GameOnline::touchBegin, this);

	EventDispatcher* touch_jianting = Director::getInstance()->getEventDispatcher();//触摸事件 监听器

	touch_jianting->addEventListenerWithSceneGraphPriority(touch, hero_my_ol);

	////////////键盘监听器
	auto keyboard = EventListenerKeyboard::create();
	keyboard->onKeyReleased = [this](EventKeyboard::KeyCode keyCode, Event* event)
	{
		if (keyCode == EventKeyboard::KeyCode::KEY_A && (if_skill_kind_0 == true))
		{
			hero_my_ol->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my_ol->get_position();
			auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
			hero_my_ol->get_sprite()->runAction(stop_animate);
			if_attack = true;
			skill_kind = 0;
			skill = 1;

		}
		if (keyCode == EventKeyboard::KeyCode::KEY_S && (if_skill_kind_1 == true))
		{
			hero_my_ol->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my_ol->get_position();
			auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
			hero_my_ol->get_sprite()->runAction(stop_animate);
			if_attack = true;
			skill_kind = 1;
			skill = 2;
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_D && (if_skill_kind_2 == true))
		{
			v_my = 150;
			//hero_my_ol->animate_aoe003();
			if_skill_kind_2 = 0;
			cd2 = 0.00000;
			skill_kind = 2;
			skill = 3;
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_R && (if_skill_kind_3 == true))
		{
			skill = 4;
			hero_my_ol->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my_ol->get_position();
			auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
			hero_my_ol->get_sprite()->runAction(stop_animate);

			hero_my_ol->animate_aoe();

			if (hero_my_ol->get_attack_rect().containsPoint(hero_enemy_ol->get_position()))
			{
				hero_enemy_ol->hp_now -= 3 * power_wu_my - defence_wu_enemy;
				if (hero_enemy_ol->hp_now <= 0)
				{
					if_attack = false;
					hero_enemy_ol->get_sprite()->setPosition(0, 0);
					removeChild(hero_enemy_ol);
					hero_my_ol->EXP_now += 700;

					int coin_num;
					coin_num = std::stoi(coin_my_ol->number);
					coin_num += 500;
					std::stringstream ss;
					ss << coin_num;
					ss >> coin_my_ol->number;
					coin_my_ol->coin->removeChildByTag(110);
					coin_my_ol->get_coin(coin_my_ol->number);
				}
			}
			for (auto it_attacked = bing_enemy_vector.begin(); it_attacked != bing_enemy_vector.end(); it_attacked++)
			{
				if (hero_my_ol->get_attack_rect().containsPoint((*it_attacked)->get_position()))
				{
					(*it_attacked)->hp_now -= 3 * power_wu_my;
					cd3 = 0.000;
					if_skill_kind_3 = false;
					if ((*it_attacked)->hp_now <= 0)
					{
						hero_my_ol->EXP_now += 100;
						int coin_num;
						coin_num = std::stoi(coin_my_ol->number);
						coin_num += 200;
						std::stringstream ss;
						ss << coin_num;
						ss >> coin_my_ol->number;
						coin_my_ol->coin->removeChildByTag(110);
						coin_my_ol->get_coin(coin_my_ol->number);
						if_attack = false;
						int itag = (*it_attacked)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it_attacked)->direction);
						dead_man->setPosition((*it_attacked)->get_position());
						addChild(dead_man);

						(*it_attacked)->get_sprite()->setPosition(0, 0);
						removeChild((*it_attacked));

						//if (bing_enemy_vector.end() - bing_enemy_vector.begin() != 1)
							//it_attacked = bing_enemy_vector.erase(it_attacked);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);
					}

				}

			}
			skill_kind = 3;
		}
	};

	EventDispatcher* eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->addEventListenerWithSceneGraphPriority(keyboard, this);

}




void GameOnline::set_hero_map()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	////////////////创建地图
	tile_map = TMXTiledMap::create("map/map_tiled.tmx");//添加瓦片地图
	auto size = tile_map->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	tile_map->setScale(scale);

	addChild(tile_map, 0, 100);//将瓦片地图放在第0层
	pengzhuang_layer = tile_map->getLayer("pengzhuang");//将wall层设置为碰撞层

	TMXObjectGroup* group = tile_map->getObjectGroup("object");//获取对象层信息

	////////////////创建防御塔
	auto spawnPoint = group->getObject("tower_enemy");
	auto x = spawnPoint["x"].asFloat();
	auto y = spawnPoint["y"].asFloat();
	if(id==1)
	 spawnPoint = group->getObject("tower_enemy");
	else
	 spawnPoint = group->getObject("tower_my");

	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	tower_enemy_ol = new TowerEnemy();
	tower_enemy_ol->init_tower(x, y);
	this->addChild(tower_enemy_ol);//将塔放在三层

	if(id==1)
		spawnPoint = group->getObject("tower_my");
	else
		spawnPoint = group->getObject("tower_enemy");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	tower_my_ol = new Tower();
	tower_my_ol->init_tower(x, y);
	this->addChild(tower_my_ol);

	////////////////创建基地
	if (id == 1)
		spawnPoint = group->getObject("jidi_enemy");
	if (id == 2)
		spawnPoint = group->getObject("jidi_my");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	jidi_enemy_ol = new JidiEnemy();
	jidi_enemy_ol->init_jidi(x, y);
	this->addChild(jidi_enemy_ol);//将塔放在二层

	if(id==1)
	spawnPoint = group->getObject("jidi_my");
	if(id==2)
	spawnPoint = group->getObject("jidi_enemy");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	jidi_my_ol = new Jidi();
	jidi_my_ol->init_jidi(x, y);
	this->addChild(jidi_my_ol);

	/////////////创建英雄
	
	if (id == 1)
		spawnPoint = group->getObject("hero_my");
	else if (id == 2)
		spawnPoint = group->getObject("hero_enemy");
		x = spawnPoint["x"].asFloat();
		y = spawnPoint["y"].asFloat();

		hero_my_ol = new Hero();
		hero_my_ol->init_hero(x, y);
		hero_my_ol->hp_now = hp_my;
		this->addChild(hero_my_ol, 1, 110);

		if(id==1)
		spawnPoint = group->getObject("hero_enemy");
		else if(id==2)
			spawnPoint = group->getObject("hero_my");
		x = spawnPoint["x"].asFloat();
		y = spawnPoint["y"].asFloat();

		hero_enemy_ol = new Hero();
		hero_enemy_ol->init_hero(x, y);
		hero_enemy_ol->hp_now = hp_my;
		this->addChild(hero_enemy_ol, 1);



	///////////////创建小兵

		auto spawnPoint_runto = group->getObject("tower_my");
		auto x_runto = spawnPoint_runto["x"].asFloat();
		auto y_runto = spawnPoint_runto["y"].asFloat();
	////////////////
		if (id == 1)
		{
			for (int i = 0; i < 3; i++)
			{
				//auto spawnPoint = group->getObject("bing_enemy_jin");
				if (i == 0)
					spawnPoint = group->getObject("bing_enemy_jin");
				if (i == 1)
					spawnPoint = group->getObject("bing_enemy_yuan");
				if (i == 2)
					spawnPoint = group->getObject("bing_enemy_pao");

				x = spawnPoint["x"].asFloat();
				y = spawnPoint["y"].asFloat();

				spawnPoint_runto = group->getObject("tower_my");
				x_runto = spawnPoint_runto["x"].asFloat();
				y_runto = spawnPoint_runto["y"].asFloat();
				bing_enemy_ol = new BingEnemy();
				bing_enemy_tag++;
				bing_enemy_ol->init_bing(x, y, x_runto, y_runto, bing_enemy_tag);
				bing_enemy_vector.pushBack(bing_enemy_ol);
				this->addChild(bing_enemy_ol);
			}

			for (int i = 0; i < 3; i++)
			{
				//auto spawnPoint = group->getObject("bing_my_jin");
				if (i == 0)
					spawnPoint = group->getObject("bing_my_jin");
				if (i == 1)
					spawnPoint = group->getObject("bing_my_yuan");
				if (i == 2)
					spawnPoint = group->getObject("bing_my_pao");

				x = spawnPoint["x"].asFloat();
				y = spawnPoint["y"].asFloat();

				spawnPoint_runto = group->getObject("tower_enemy");
				x_runto = spawnPoint_runto["x"].asFloat();
				y_runto = spawnPoint_runto["y"].asFloat();
				bing_my_ol = new Bing();
				bing_my_tag++;
				bing_my_ol->init_bing(x, y, x_runto, y_runto, bing_my_tag);
				bing_my_vector.pushBack(bing_my_ol);

				this->addChild(bing_my_ol);
			}
		}
		else if (id == 2)
		{
			for (int i = 0; i < 3; i++)
			{
				//auto spawnPoint = group->getObject("bing_my_jin");
				if (i == 0)
					spawnPoint = group->getObject("bing_my_jin");
				if (i == 1)
					spawnPoint = group->getObject("bing_my_yuan");
				if (i == 2)
					spawnPoint = group->getObject("bing_my_pao");

				x = spawnPoint["x"].asFloat();
				y = spawnPoint["y"].asFloat();

				spawnPoint_runto = group->getObject("tower_enemy");
				x_runto = spawnPoint_runto["x"].asFloat();
				y_runto = spawnPoint_runto["y"].asFloat();
				bing_enemy_ol = new BingEnemy();
				bing_enemy_tag++;
				bing_enemy_ol->init_bing(x, y, x_runto, y_runto, bing_enemy_tag);
				bing_enemy_vector.pushBack(bing_enemy_ol);
				this->addChild(bing_enemy_ol);
			}

			for (int i = 0; i < 3; i++)
			{
				//auto spawnPoint = group->getObject("bing_enemy_jin");
				if (i == 0)
					spawnPoint = group->getObject("bing_enemy_jin");
				if (i == 1)
					spawnPoint = group->getObject("bing_enemy_yuan");
				if (i == 2)
					spawnPoint = group->getObject("bing_enemy_pao");

				x = spawnPoint["x"].asFloat();
				y = spawnPoint["y"].asFloat();

				spawnPoint_runto = group->getObject("tower_my");
				x_runto = spawnPoint_runto["x"].asFloat();
				y_runto = spawnPoint_runto["y"].asFloat();
				bing_my_ol = new Bing();
				bing_my_tag++;
				bing_my_ol->init_bing(x, y, x_runto, y_runto, bing_my_tag);
				bing_my_vector.pushBack(bing_my_ol);

				this->addChild(bing_my_ol);
			}
		}



	zhanji_my_ol = new Zhanji();
	zhanji_my_ol->init_zhanji(1900, 60);
	this->addChild(zhanji_my_ol, 2);

	coin_my_ol = new Coin();
	coin_my_ol->init_coin(60, 800);
	this->addChild(coin_my_ol, 10);

	///////////商店界面
	MenuItemImage* shop_button = MenuItemImage::create("shop_button1.png", "shop_button2.png",
		CC_CALLBACK_1(GameOnline::menuShopItemOpenCallback, this));//商店按钮
	shop_button->setPosition(Vec2(50, visibleSize.height - 50));
	shop_button->setScale(0.8f);

	auto menuShop = Menu::create(shop_button, NULL);
	menuShop->setPosition(Vec2::ZERO);
	this->addChild(menuShop, 1);

}

Vec2 GameOnline::tileCoordFromPosition(Vec2 pos)
{
	int x = pos.x / tile_map->getTileSize().width;
	int y = ((tile_map->getMapSize().height * tile_map->getTileSize().height) - pos.y)
		/ tile_map->getTileSize().height;
	return Vec2(x, y);
}

void GameOnline::hero_attack(Vec2 attack, Vec2 attacked)
{
	Vec2 attack_position = attack;
	Vec2 attacked_position = attacked;
	Vec2 diff = attack_position - attacked_position;//是触点和和hero之间的距离
	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));
	if (skill_kind == 0)
	{
		Sprite* skill0 = Sprite::create("skill/skill_0.png");
		skill0->setPosition(attack_position);
		addChild(skill0);

		auto* animate0 = hero_my_ol->create_animate_skill(iv, juli, 0);
		auto* move_action0 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi0 = CallFunc::create([skill0, this]() {skill0->removeFromParent(); });
		auto* sequence0 = Sequence::create(move_action0, xiaoshi0, NULL);

		skill0->runAction(animate0);
		skill0->runAction(sequence0);
	}
	//第一个技能
	if (skill_kind == 1)
	{
		Sprite* skill1 = Sprite::create("skill/skill_1.png");
		skill1->setPosition(attack_position);
		addChild(skill1);

		auto* animate1 = hero_my_ol->create_animate_skill(iv, juli, 1);
		auto* move_action1 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi1 = CallFunc::create([skill1, this]() {skill1->removeFromParent(); });
		auto* sequence1 = Sequence::create(move_action1, xiaoshi1, NULL);

		skill1->runAction(animate1);
		skill1->runAction(sequence1);
	}
}

bool GameOnline::touchBegin(Touch* touch, Event* event)

{
	//Vec2 current_position = hero_my->getPosition();
	touch_position = touch->getLocation();
	Vec2 diff = touch_position - hero_my_ol->get_position();//是触点和和hero之间的距离

	int skill_linshi = skill;
	qingling();
	skill = skill_linshi;
	tx = touch_position.x;
	ty = touch_position.y;
	hp = hero_my_ol->hp_now;
	hp_max = hp_my;
	gongji = power_wu_my;
	yisu = v_my;
	dengji = std::stoi(level);
	hp = hero_my_ol->hp_now;

	client1->Sending(to_char());///////每次点击发送自己的信息




	if (if_attack == false)//没按键,走路
	{
		hero_my_ol->get_sprite()->stopAllActions();
		float iv = v_my;//iv是移动速度
		float juli = sqrt(diff.x * diff.x + diff.y * diff.y);

		auto* move_action = MoveTo::create(juli / iv, touch_position);
		auto* run_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "run", juli / iv);
		auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
		auto* sequence = Sequence::create(move_action, stop_animate, NULL);

		Vec2 tileCoord = this->tileCoordFromPosition(touch_position);
		//获得瓦片的GID
		int tileGid = pengzhuang_layer->getTileGIDAt(tileCoord);//只有碰撞层时
		if (tileGid > 0)
		{
			//is_walk = false;
			hero_my_ol->get_sprite()->stopAllActions();
			auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
			hero_my_ol->get_sprite()->runAction(stop_animate);
			//return true;
		}
		hero_my_ol->get_sprite()->runAction(run_animate);
		hero_my_ol->get_sprite()->runAction(sequence);
	}
	if (if_attack == true)//按键攻击后再次按键释放技能
	{
		auto* stop_animate = hero_my_ol->create_animate_walk(hero_my_ol->get_direction(diff.x, diff.y), "stop", 0);
		hero_my_ol->get_sprite()->runAction(stop_animate);

		if (hero_my_ol->get_attack_rect().containsPoint(touch_position))//如果攻击在攻击范围内
		{
			for (auto it_attacked = bing_enemy_vector.begin();
				it_attacked != bing_enemy_vector.end(); it_attacked++)
			{
				if (hero_enemy_ol->get_attacked_rect().containsPoint(touch_position))//如果英雄2在技能范围内
				{
					/////////添加受攻击动画，用一个深色帧就好
					/////////伤害判断kk
					if (skill_kind == 0 && (if_skill_kind_0 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						cd0 = 0.000;
						if_skill_kind_0 = false;
						hero_enemy_ol->hp_now -= power_wu_my - defence_wu_enemy;
					}
					if (skill_kind == 1 && (if_skill_kind_1 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						cd1 = 0.000;
						if_skill_kind_1 = false;
						hero_enemy_ol->hp_now -= power_wu_my * 2 - defence_wu_enemy;

					}
					/*if (skill_kind == 2)
						hero_enemy->hp_now -= 300;

					/*if (skill_kind == 3)
						hero_enemy->hp_now -= 400;*/
					if (hero_enemy_ol->hp_now <= 0)
					{
						if_attack = false;
						hero_enemy_ol->get_sprite()->setPosition(0, 0);
						removeChild(hero_enemy_ol);
						hero_my_ol->EXP_now += 700;
						int coin_num;
						coin_num = std::stoi(coin_my_ol->number);
						coin_num += 500;
						std::stringstream ss;
						ss << coin_num;
						ss >> coin_my_ol->number;
						coin_my_ol->coin->removeChildByTag(110);
						coin_my_ol->get_coin(coin_my_ol->number);
						//敌方重生！！！！！！！！！！！！！！！
					}
				}
				if (tower_enemy_ol->rect_attacked.containsPoint(touch_position))
				{
					if (skill_kind == 0 && (if_skill_kind_0 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						cd0 = 0.000;
						if_skill_kind_0 = false;
						tower_enemy_ol->hp_now -= power_wu_my;;
					}
					if (skill_kind == 1 && (if_skill_kind_1 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						cd1 = 0.000;
						if_skill_kind_1 = false;
						tower_enemy_ol->hp_now -= power_wu_my * 2;
					}
					if (tower_enemy_ol->hp_now <= 0)
					{
						if_attack = false;
						tower_enemy_ol->get_sprite()->setPosition(0, 0);
						removeChild(tower_enemy_ol);
						hero_my_ol->EXP_now += 1500;
						int coin_num;
						coin_num = std::stoi(coin_my_ol->number);
						coin_num += 500;
						std::stringstream ss;
						ss << coin_num;
						ss >> coin_my_ol->number;
						coin_my_ol->coin->removeChildByTag(110);
						coin_my_ol->get_coin(coin_my_ol->number);
					}
				}
				if ((*it_attacked)->get_attacked_rect().containsPoint(touch_position))
				{
					if (skill_kind == 0 && (if_skill_kind_0 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						(*it_attacked)->hp_now -= power_wu_my;
						cd0 = 0.000;
						if_skill_kind_0 = false;
					}
					if (skill_kind == 1 && (if_skill_kind_1 == true))
					{
						hero_attack(hero_my_ol->get_position(), touch_position);
						(*it_attacked)->hp_now -= power_wu_my * 2;
						cd1 = 0.000;
						if_skill_kind_1 = false;
					}
					if ((*it_attacked)->hp_now <= 0)
					{
						if_attack = false;
						hero_my_ol->EXP_now += 100;
						int coin_num;
						coin_num = std::stoi(coin_my_ol->number);
						coin_num += 150;
						std::stringstream ss;
						ss << coin_num;
						ss >> coin_my_ol->number;
						coin_my_ol->coin->removeChildByTag(110);
						coin_my_ol->get_coin(coin_my_ol->number);
						int itag = (*it_attacked)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it_attacked)->direction);
						dead_man->setPosition((*it_attacked)->get_position());
						addChild(dead_man);

						(*it_attacked)->get_sprite()->setPosition(0, 0);
						removeChild((*it_attacked));

						//if (bing_enemy_vector.end()- bing_enemy_vector.begin()!= 1)
							//it_attacked = bing_enemy_vector.erase(it_attacked);
						//it_attacked = bing_enemy_vector.erase(it_attacked);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);
					}
				}


			}
			if_attack = false;
		}
		else//如果超出了自己的攻击范围
			if_attack = false;
	}
	return true;
}

void  GameOnline::updateView(float dt)
{
	//pengzhuang_check();
	hero_my_ol->hpLimit = hp_my;
	hero_my_ol->iv = v_my;
	hero_enemy_ol->hpLimit = hp_enemy;
	hero_enemy_ol->iv = v_enemy;
	hero_my_ol->level_my = level;
	hero_enemy_ol->level_my = levelenemy;
}

void GameOnline::bing_shuaxin(float dt)
{
	TMXObjectGroup* group = tile_map->getObjectGroup("object");//获取对象层信息
	auto spawnPoint_runto = group->getObject("tower_my");
	auto x_runto = spawnPoint_runto["x"].asFloat();
	auto y_runto = spawnPoint_runto["y"].asFloat();

	auto spawnPoint = group->getObject("tower_enemy");
	auto x = spawnPoint["x"].asFloat();
	auto y = spawnPoint["y"].asFloat();
	////////////////
	if (id == 1)
	{
		for (int i = 0; i < 3; i++)
		{
			//auto spawnPoint = group->getObject("bing_enemy_jin");
			if (i == 0)
				spawnPoint = group->getObject("bing_enemy_jin");
			if (i == 1)
				spawnPoint = group->getObject("bing_enemy_yuan");
			if (i == 2)
				spawnPoint = group->getObject("bing_enemy_pao");

			x = spawnPoint["x"].asFloat();
			y = spawnPoint["y"].asFloat();

			spawnPoint_runto = group->getObject("tower_my");
			x_runto = spawnPoint_runto["x"].asFloat();
			y_runto = spawnPoint_runto["y"].asFloat();
			bing_enemy_ol = new BingEnemy();
			bing_enemy_tag++;
			bing_enemy_ol->init_bing(x, y, x_runto, y_runto, bing_enemy_tag);
			bing_enemy_vector.pushBack(bing_enemy_ol);
			this->addChild(bing_enemy_ol);
		}

		for (int i = 0; i < 3; i++)
		{
			//auto spawnPoint = group->getObject("bing_my_jin");
			if (i == 0)
				spawnPoint = group->getObject("bing_my_jin");
			if (i == 1)
				spawnPoint = group->getObject("bing_my_yuan");
			if (i == 2)
				spawnPoint = group->getObject("bing_my_pao");

			x = spawnPoint["x"].asFloat();
			y = spawnPoint["y"].asFloat();

			spawnPoint_runto = group->getObject("tower_enemy");
			x_runto = spawnPoint_runto["x"].asFloat();
			y_runto = spawnPoint_runto["y"].asFloat();
			bing_my_ol = new Bing();
			bing_my_tag++;
			bing_my_ol->init_bing(x, y, x_runto, y_runto, bing_my_tag);
			bing_my_vector.pushBack(bing_my_ol);

			this->addChild(bing_my_ol);
		}
	}
	else if (id == 2)
	{
		for (int i = 0; i < 3; i++)
		{
			//auto spawnPoint = group->getObject("bing_my_jin");
			if (i == 0)
				spawnPoint = group->getObject("bing_my_jin");
			if (i == 1)
				spawnPoint = group->getObject("bing_my_yuan");
			if (i == 2)
				spawnPoint = group->getObject("bing_my_pao");

			x = spawnPoint["x"].asFloat();
			y = spawnPoint["y"].asFloat();

			spawnPoint_runto = group->getObject("tower_enemy");
			x_runto = spawnPoint_runto["x"].asFloat();
			y_runto = spawnPoint_runto["y"].asFloat();
			bing_enemy_ol = new BingEnemy();
			bing_enemy_tag++;
			bing_enemy_ol->init_bing(x, y, x_runto, y_runto, bing_enemy_tag);
			bing_enemy_vector.pushBack(bing_enemy_ol);
			this->addChild(bing_enemy_ol);
		}

		for (int i = 0; i < 3; i++)
		{
			//auto spawnPoint = group->getObject("bing_enemy_jin");
			if (i == 0)
				spawnPoint = group->getObject("bing_enemy_jin");
			if (i == 1)
				spawnPoint = group->getObject("bing_enemy_yuan");
			if (i == 2)
				spawnPoint = group->getObject("bing_enemy_pao");

			x = spawnPoint["x"].asFloat();
			y = spawnPoint["y"].asFloat();

			spawnPoint_runto = group->getObject("tower_my");
			x_runto = spawnPoint_runto["x"].asFloat();
			y_runto = spawnPoint_runto["y"].asFloat();
			bing_my_ol = new Bing();
			bing_my_tag++;
			bing_my_ol->init_bing(x, y, x_runto, y_runto, bing_my_tag);
			bing_my_vector.pushBack(bing_my_ol);

			this->addChild(bing_my_ol);
		}
	}
}

bool GameOnline::rect_contain_bing_enemy(Bing* it_attack)//查看对方兵是否在攻击范围内
{

	for (auto it_attacked = bing_enemy_vector.begin();
		it_attacked != bing_enemy_vector.end(); it_attacked++)
	{
		/*Rect rect = Rect(it_attack->bing->getPositionX()-100,
			it_attack->bing->getPositionY() - 100,200,200);
		Vec2 position = (*it_attacked)->bing->getPosition();*/
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{


					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));

					//if (bing_enemy_vector.end() - bing_enemy_vector.begin() != 1)
						//it_attacked = bing_enemy_vector.erase(it_attacked);
					//it_attacked = bing_enemy_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);

				}
			}
			return true;
		}
	}
	return true;
}

bool GameOnline::rect_contain_bing_my(BingEnemy* it_attack)//查看我是否在对方攻击范围内
{
	for (auto it_attacked = bing_my_vector.begin();
		it_attacked != bing_my_vector.end(); it_attacked++)
	{
		/*Rect rect = Rect(it_attack->bing->getPositionX() - 100,
			it_attack->bing->getPositionY() - 100, 200, 200);
		Vec2 position = (*it_attacked)->bing->getPosition();*/
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{
					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));
					//it_attacked = bing_my_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);
				}
			}
			return true;
		}
	}
	return false;
}

void GameOnline::rect_check_tower(float dt)//判定塔要攻击谁
{
	///////////////////敌方防御塔的判定
	if (tower_enemy_ol->mubiao_sprite != NULL)
	{
		if (tower_enemy_ol->get_attack_rect().containsPoint(tower_enemy_ol->mubiao_sprite->getPosition()) == false)
			//如果目标超出攻击范围
		{
			(tower_enemy_ol)->get_sprite()->stopAllActions();
			tower_enemy_ol->is_attacking = false;
		}
	}
	if (tower_my_ol->mubiao_sprite != NULL)
	{
		if (tower_my_ol->get_attack_rect().containsPoint((tower_my_ol)->mubiao_sprite->getPosition()) == false)
			//如果目标超出攻击范围
		{
			(tower_my_ol)->get_sprite()->stopAllActions();
			tower_my_ol->is_attacking = false;
		}
	}

	////////////注意顺序不能改变，顺序代表着攻击优先级
	//if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if (tower_enemy_ol->rect_attack.containsPoint((*it)->get_position()))//小兵为第一优先级
			{
				if (tower_enemy_ol->is_attacking == false)//只在这一次改变目标
				{
					tower_enemy_ol->mubiao = 3;
					tower_enemy_ol->mubiao_sprite = (*it)->get_sprite();
					tower_enemy_ol->is_attacking = true;
				}
				if (tower_enemy_ol->mubiao_sprite == (*it)->get_sprite())
				{
					tower_enemy_ol->attack((*it)->get_sprite());
					(*it)->hp_now -= 100;
					if ((*it)->hp_now <= 0)///如果attacked死亡
					{
						int itag = (*it)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it)->direction);
						dead_man->setPosition((*it)->get_position());
						addChild(dead_man);

						tower_my_ol->is_attacking = false;
						(*it)->get_sprite()->setPosition(0, 0);
						removeChild((*it));
						//it = bing_my_vector.erase(it);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);

					}
				}
			}
			else if (tower_enemy_ol->rect_attack.containsPoint(hero_my_ol->get_position()))//将英雄放在第一个
			{
				if (tower_enemy_ol->is_attacking == false)//只在这一次改变目标
				{
					tower_enemy_ol->mubiao = 1;
					tower_enemy_ol->mubiao_sprite = hero_my_ol->get_sprite();
					tower_enemy_ol->is_attacking = true;
				}
				if (tower_enemy_ol->mubiao_sprite == hero_my_ol->get_sprite())
				{
					tower_enemy_ol->attack(hero_my_ol->get_sprite());
					hero_my_ol->hp_now -= 100;
					if (hero_my_ol->hp_now <= 0)///如果attacked死亡
					{
						tower_enemy_ol->is_attacking = false;
						hero_my_ol->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_my_ol);

						//this->scheduleOnce(schedule_selector(GameOnline::Restart_my), 5.0f);
					}
				}
			}
		}
	}
	//////////////在后面添加己方防御塔判断
	//if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
			if (tower_my_ol->rect_attack.containsPoint((*it)->get_position()))//小兵为第一优先级
			{
				if (tower_my_ol->is_attacking == false)//只在这一次改变目标
				{
					tower_my_ol->mubiao = 4;
					tower_my_ol->mubiao_sprite = (*it)->get_sprite();
					tower_my_ol->is_attacking = true;
				}
				if (tower_my_ol->mubiao_sprite == (*it)->get_sprite())
				{
					tower_my_ol->attack((*it)->get_sprite());
					(*it)->hp_now -= 100;

					int itag = (*it)->tag;
					int idirection = (*it)->direction;
					if ((*it)->hp_now <= 0)///如果attacked死亡
					{
						int itag = (*it)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it)->direction);
						dead_man->setPosition((*it)->get_position());
						addChild(dead_man);

						tower_my_ol->is_attacking = false;
						(*it)->get_sprite()->setPosition(0, 0);
						removeChild((*it));
						//it = bing_enemy_vector.erase(it);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);


					}
				}

			}
			else if (tower_my_ol->rect_attack.containsPoint(hero_enemy_ol->get_position()))//将英雄放在第一个
			{
				if (tower_my_ol->is_attacking == false)//只在这一次改变目标
				{
					tower_my_ol->mubiao_sprite = hero_enemy_ol->get_sprite();
					tower_my_ol->is_attacking = true;
				}
				if (tower_my_ol->mubiao_sprite == hero_enemy_ol->get_sprite())
				{
					tower_my_ol->attack(hero_enemy_ol->get_sprite());
					hero_enemy_ol->hp_now -= 100;
					if (hero_enemy_ol->hp_now <= 0)///如果attacked死亡
					{
						tower_my_ol->is_attacking = false;
						hero_enemy_ol->get_sprite()->setPosition(0, 0);
						removeChild(hero_enemy_ol);

					}
				}
			}
		}
	}

}

void GameOnline::rect_check_bing(float dt)
{
	///////////////////敌方小兵的判定
	if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
			if ((*it)->is_attacking == false ||
				((*it)->get_attack_rect().containsPoint
				((*it)->mubiao_sprite->getPosition())) == false)
				//如果没有在攻击 或者 目标超出攻击范围
			{
				(*it)->get_sprite()->stopAllActions();
				(*it)->is_attacking = false;//一定要加，因为还有超出范围的判定
				(*it)->auto_runto();
			}
		}
	}
	if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if ((*it)->is_attacking == false ||
				((*it)->get_attack_rect().containsPoint
				((*it)->mubiao_sprite->getPosition())) == false)
				//如果没有在攻击 或者 目标超出攻击范围
			{
				(*it)->get_sprite()->stopAllActions();
				(*it)->is_attacking = false;//一定要加，因为还有超出范围的判定
				(*it)->auto_runto();
			}
		}
	}
	////////////////////敌方小兵
	/////注意：顺序代表着攻击优先级
	if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
			if ((*it)->get_attack_rect().containsPoint(tower_my_ol->get_position()))//将小兵放在第一个
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 5;
					(*it)->mubiao_sprite = tower_my_ol->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == tower_my_ol->get_sprite())
				{
					(*it)->attack(tower_my_ol->get_sprite(), tower_my_ol->get_attacked_rect());
					tower_my_ol->hp_now -= 100;
					if (tower_my_ol->hp_now <= 0)///如果attacked死亡
					{

						(*it)->is_attacking = false;
						tower_my_ol->get_sprite()->setPosition(0, 0);
						removeChild(tower_my_ol);
					}
				}

			}
			else if (rect_contain_bing_my(*it))
			{

			}
			else if ((*it)->get_attack_rect().containsPoint(hero_my_ol->get_position()))
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 1;
					(*it)->mubiao_sprite = hero_my_ol->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == hero_my_ol->get_sprite())
				{
					(*it)->attack(hero_my_ol->get_sprite(), hero_my_ol->get_attacked_rect());
					hero_my_ol->hp_now -= 100;
					if (hero_my_ol->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						hero_my_ol->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_my_ol);

						//this->scheduleOnce(schedule_selector(GameOnline::Restart_my), 5.0f);
					}
				}
			}
		}
	}
	/////////////后面添加我方小兵判定
	if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if ((*it)->get_attack_rect().containsPoint(tower_enemy_ol->get_position()))//将小兵放在第一个
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 6;
					(*it)->mubiao_sprite = tower_enemy_ol->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == tower_enemy_ol->get_sprite())
				{
					(*it)->attack(tower_enemy_ol->get_sprite(), tower_enemy_ol->get_attacked_rect());
					tower_enemy_ol->hp_now -= 100;
					if (tower_enemy_ol->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						tower_enemy_ol->get_sprite()->setPosition(0, 0);
						removeChild(tower_enemy_ol);
					}
				}

			}
			else if (rect_contain_bing_enemy(*it))
			{

			}
			else if ((*it)->get_attack_rect().containsPoint(hero_enemy_ol->get_position()))
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 1;
					(*it)->mubiao_sprite = hero_enemy_ol->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == hero_enemy_ol->get_sprite())
				{
					(*it)->attack(hero_enemy_ol->get_sprite(), hero_enemy_ol->get_attacked_rect());
					hero_enemy_ol->hp_now -= 100;
					if (hero_enemy_ol->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						hero_enemy_ol->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_enemy_ol);

						//this->scheduleOnce(schedule_selector(GameOnline::Restart_enemy), 5.0f);
					}
				}
			}
		}
	}
}

bool GameOnline::Hero_ai_attack_bing(HeroAi* it_attack)
{
	for (auto it_attacked = bing_my_vector.begin();
		it_attacked != bing_my_vector.end(); it_attacked++)
	{
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{
					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));
					//it_attacked = bing_my_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);
				}
			}
		}
		return true;
	}
	return false;
}

void GameOnline::hurt_check(float dt)
{
	if (hero_my_ol->if_check_hurt == true)
	{
		hero_my_ol->hurt_check();
	}
	if (hero_enemy_ol->if_check_hurt == true)
	{
		hero_enemy_ol->hurt_check();
	}
	if (bing_my_ol->if_check_hurt == true)
	{
		bing_my_ol->hurt_check();
	}
	if (bing_enemy_ol->if_check_hurt == true)
	{
		bing_enemy_ol->hurt_check();
	}
	if (tower_my_ol->if_check_hurt == true)
	{
		tower_my_ol->hurt_check();
	}
	if (tower_enemy_ol->if_check_hurt == true)
	{
		tower_enemy_ol->hurt_check();
	}

}

Animate* GameOnline::animate_dead(int tag, int direction)
{
	Animation* animation = Animation::create();

	char  file_name[50] = { 0 };
	if (tag % 3 == 0)
	{
		for (int i = 0; i < 7; i++)
		{
			sprintf(file_name, "paobing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else if ((tag + 1) % 3 == 0)
	{
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "yuanbing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else
	{
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "jinbing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}

	animation->setDelayPerUnit(0.1f);
	animation->setLoops(1);
	Animate* animate = Animate::create(animation);
	return animate;
}

void GameOnline::get_coin_by_time(float dt)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	coin_num += 20;
	std::stringstream ss;
	ss << coin_num;
	ss >> coin_my_ol->number;
	coin_my_ol->coin->removeChildByTag(110);
	coin_my_ol->get_coin(coin_my_ol->number);
}

void GameOnline::EXP_change(float dt)
{
	hero_my_ol->EXP_now += 10;
}

void GameOnline::EXP_check(float dt)
{

	auto progress = (ProgressTimer*)hero_my_ol->hero->getChildByTag(210);
	progress->setPercentage((hero_my_ol->EXP_now / hero_my_ol->EXPLimit) * 100);

	if (progress->getPercentage() >= 100)
	{

		int level_my_num;
		level_my_num = std::stoi(level);
		level_my_num += 1;
		power_wu_my += 100;
		power_fa_my += 100;
		defence_wu_my += 100;
		defence_fa_my += 100;
		hp_my += 400;
		std::stringstream ss;
		ss << level_my_num;
		ss >> level;
		hero_my_ol->level_my = level;////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		hero_my_ol->hero->removeChildByTag(150);
		hero_my_ol->get_level_my(hero_my_ol->hero, hero_my_ol->level_my);
		hero_my_ol->EXP_now -= hero_my_ol->EXPLimit;
		hero_my_ol->EXPLimit = hero_my_ol->EXPLimit + 300;

	}
}

void GameOnline::menuShopItemOpenCallback(cocos2d::Ref* pSender)
{
	//创建商店图标
	auto visibleSize = Director::getInstance()->getVisibleSize();

	Layer* shoplayer = Layer::create();
	auto shopCloseButton = MenuItemImage::create("close_button1.png", "close_button2.png",
		CC_CALLBACK_1(GameOnline::menuShopItemCloseCallback, this));                //商店按钮
	shopCloseButton->setAnchorPoint(Vec2::ZERO);
	shopCloseButton->setPosition(500, 395);
	shopCloseButton->setScaleX(0.02f);
	shopCloseButton->setScaleY(0.02f);

	//itemdao
	auto shopItem = Sprite::create("shopItem.png");
	shopItem->setAnchorPoint(Vec2::ZERO);
	shopItem->setPosition(50, 25);
	shopItem->setScaleX(3.5f);
	shopItem->setScaleY(2.4f);
	shoplayer->addChild(shopItem, 9);
	auto itemDao1 = MenuItemImage::create("/equip/dao_lan.png", "/equip/dao_lan.png", CC_CALLBACK_1(GameOnline::buyDaoCallBack1, this));
	itemDao1->setAnchorPoint(Vec2::ZERO);
	itemDao1->setPosition(Vec2(343.5, 294.5));
	itemDao1->setScaleX(0.51f);
	itemDao1->setScaleY(0.5f);
	auto itemDao2 = MenuItemImage::create("/equip/dao_long.jpg", "/equip/dao_long.jpg", CC_CALLBACK_1(GameOnline::buyDaoCallBack2, this));
	itemDao2->setAnchorPoint(Vec2::ZERO);
	itemDao2->setPosition(Vec2(381, 294.5));
	itemDao2->setScaleX(0.55f);
	itemDao2->setScaleY(0.56f);
	auto itemDao3 = MenuItemImage::create("/equip/dao_lv.jpg", "/equip/dao_lv.jpg", CC_CALLBACK_1(GameOnline::buyDaoCallBack3, this));
	itemDao3->setAnchorPoint(Vec2::ZERO);
	itemDao3->setAnchorPoint(Vec2::ZERO);
	itemDao3->setPosition(Vec2(418.5, 294.5));
	itemDao3->setScaleX(0.55f);
	itemDao3->setScaleY(0.56f);
	////////////////////////dun
	auto itemDun1 = MenuItemImage::create("/equip/dun_mu.png", "/equip/dun_mu.png", CC_CALLBACK_1(GameOnline::buyDunCallBack1, this));
	itemDun1->setAnchorPoint(Vec2::ZERO);
	itemDun1->setPosition(Vec2(343.5, 256.5));
	itemDun1->setScaleX(0.51f);
	itemDun1->setScaleY(0.5f);
	auto itemDun2 = MenuItemImage::create("/equip/dun_tie.png", "/equip/dun_tie.png", CC_CALLBACK_1(GameOnline::buyDunCallBack2, this));
	itemDun2->setAnchorPoint(Vec2::ZERO);
	itemDun2->setPosition(Vec2(381, 256.5));
	itemDun2->setScaleX(0.51f);
	itemDun2->setScaleY(0.5f);


	/////////////////////////gongjian
	auto itemGong1 = MenuItemImage::create("/equip/gong_huang.jpg", "/equip/gong_huang.jpg", CC_CALLBACK_1(GameOnline::buyGongCallBack1, this));
	itemGong1->setAnchorPoint(Vec2::ZERO);
	itemGong1->setPosition(Vec2(418.5, 256.5));
	itemGong1->setScaleX(0.55f);
	itemGong1->setScaleY(0.56f);
	auto itemGong2 = MenuItemImage::create("/equip/gong_lv.png", "/equip/gong_lv.png", CC_CALLBACK_1(GameOnline::buyGongCallBack2, this));
	itemGong2->setAnchorPoint(Vec2::ZERO);
	itemGong2->setPosition(Vec2(456, 256.5));
	itemGong2->setScaleX(0.51f);
	itemGong2->setScaleY(0.5f);


	///////////////////////////kaijia
	auto itemKai1 = MenuItemImage::create("/equip/Kai_bin.png", "/equip/Kai_bin.png", CC_CALLBACK_1(GameOnline::buyKaiCallBack1, this));
	itemKai1->setAnchorPoint(Vec2::ZERO);
	itemKai1->setPosition(Vec2(343.5, 218.5));
	itemKai1->setScaleX(0.51f);
	itemKai1->setScaleY(0.5f);
	auto itemKai2 = MenuItemImage::create("/equip/Kai_feng.jpg", "/equip/Kai_feng.png", CC_CALLBACK_1(GameOnline::buyKaiCallBack2, this));
	itemKai2->setAnchorPoint(Vec2::ZERO);
	itemKai2->setPosition(Vec2(381, 218.5));
	itemKai2->setScaleX(0.55f);
	itemKai2->setScaleY(0.56f);
	auto itemKai3 = MenuItemImage::create("/equip/Kai_fuhuo.png", "/equip/Kai_fuhuo.png", CC_CALLBACK_1(GameOnline::buyKaiCallBack3, this));
	itemKai3->setAnchorPoint(Vec2::ZERO);
	itemKai3->setPosition(Vec2(418.5, 218.5));
	itemKai3->setScaleX(0.51f);
	itemKai3->setScaleY(0.5f);
	auto itemKai4 = MenuItemImage::create("/equip/Kai_lan.png", "/equip/Kai_lan.png", CC_CALLBACK_1(GameOnline::buyKaiCallBack4, this));
	itemKai4->setAnchorPoint(Vec2::ZERO);
	itemKai4->setPosition(Vec2(456, 218.5));
	itemKai4->setScaleX(0.51f);
	itemKai4->setScaleY(0.5f);
	auto itemKai5 = MenuItemImage::create("/equip/Kai_bu.png", "/equip/Kai_bu.png", CC_CALLBACK_1(GameOnline::buyKaiCallBack5, this));
	itemKai5->setAnchorPoint(Vec2::ZERO);
	itemKai5->setPosition(Vec2(456, 294.5));
	itemKai5->setScaleX(0.51f);
	itemKai5->setScaleY(0.5f);

	/////////////////Shoe
	auto itemShoe1 = MenuItemImage::create("/equip/Shoe_hui.jpg", "/equip/Shoe_hui.jpg", CC_CALLBACK_1(GameOnline::buyShoeCallBack1, this));
	itemShoe1->setAnchorPoint(Vec2::ZERO);
	itemShoe1->setPosition(Vec2(343.5, 180.5));
	itemShoe1->setScaleX(0.51f);
	itemShoe1->setScaleY(0.5f);
	auto itemShoe2 = MenuItemImage::create("/equip/Shoe_lan.jpg", "/equip/Shoe_lan.jpg", CC_CALLBACK_1(GameOnline::buyShoeCallBack2, this));
	itemShoe2->setAnchorPoint(Vec2::ZERO);
	itemShoe2->setPosition(Vec2(381, 180.5));
	itemShoe2->setScaleX(0.55f);
	itemShoe2->setScaleY(0.56f);
	auto itemShoe3 = MenuItemImage::create("/equip/Shoe_lv.jpg", "/equip/Shoe_lv.jpg", CC_CALLBACK_1(GameOnline::buyShoeCallBack3, this));
	itemShoe3->setAnchorPoint(Vec2::ZERO);
	itemShoe3->setPosition(Vec2(418.5, 180.5));
	itemShoe3->setScaleX(0.55f);
	itemShoe3->setScaleY(0.56f);
	/////////////////fa
	auto itemFa1 = MenuItemImage::create("/equip/fashu.png", "/equip/fashu.png", CC_CALLBACK_1(GameOnline::buyFaCallBack1, this));
	itemFa1->setAnchorPoint(Vec2::ZERO);
	itemFa1->setPosition(Vec2(456, 180.5));
	itemFa1->setScaleX(0.51f);
	itemFa1->setScaleY(0.5f);
	auto itemFa2 = MenuItemImage::create("/equip/fashu_feng.jpg", "/equip/fashu_feng.jpg", CC_CALLBACK_1(GameOnline::buyFaCallBack2, this));
	itemFa2->setAnchorPoint(Vec2::ZERO);
	itemFa2->setPosition(Vec2(343.5, 142.5));
	itemFa2->setScaleX(0.55f);
	itemFa2->setScaleY(0.56f);

	auto menu = Menu::create(shopCloseButton, itemDao1, itemDao2, itemDao3, itemDun1, itemDun2, itemGong1, itemGong2, itemKai1, itemKai2, itemKai3, itemKai4,
		itemKai5, itemShoe1, itemShoe2, itemShoe3, itemFa1, itemFa2, NULL);
	menu->setPosition(Vec2::ZERO);
	shopItem->addChild(menu, 1);

	auto zhuangBei1 = Sprite::create("gezi.png");
	if (equip[1] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei1->addChild(equipment, 10);
	}
	else if (equip[1] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	else if (equip[1] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei1->addChild(equipment, 1);
	}
	zhuangBei1->setPosition(124.7, 330);
	shopItem->addChild(zhuangBei1, 1);

	/////////////////////////////////////
	auto zhuangBei2 = Sprite::create("gezi.png");
	if (equip[2] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	else if (equip[2] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei2->addChild(equipment, 1);
	}
	zhuangBei2->setPosition(124.7, 291.5);
	shopItem->addChild(zhuangBei2, 1);

	//////////////////////////////////////
	auto zhuangBei3 = Sprite::create("gezi.png");
	if (equip[3] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	else if (equip[3] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei3->addChild(equipment, 1);
	}
	zhuangBei3->setPosition(124.7, 254);
	shopItem->addChild(zhuangBei3, 1);

	///////////////////////////////////////
	auto zhuangBei4 = Sprite::create("gezi.png");
	if (equip[4] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	else if (equip[4] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei4->addChild(equipment, 1);
	}
	zhuangBei4->setPosition(124.7, 216.4);
	shopItem->addChild(zhuangBei4, 1);

	//////////////////////////////////////
	auto zhuangBei5 = Sprite::create("gezi.png");
	if (equip[5] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	else if (equip[5] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei5->addChild(equipment, 1);
	}
	zhuangBei5->setPosition(124.7, 179.4);
	shopItem->addChild(zhuangBei5, 1);

	/////////////////////////////////////
	auto zhuangBei6 = Sprite::create("gezi.png");
	if (equip[6] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	else if (equip[6] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei6->addChild(equipment, 1);
	}
	zhuangBei6->setPosition(124.7, 141.6);
	shopItem->addChild(zhuangBei6, 1);

	/////////////////////////////////////
	auto zhuangBei7 = Sprite::create("gezi.png");
	if (equip[7] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	else if (equip[7] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei7->addChild(equipment, 1);
	}
	zhuangBei7->setPosition(124.7, 104);
	shopItem->addChild(zhuangBei7, 1);

	/////////////////////////////////////
	auto zhuangBei8 = Sprite::create("gezi.png");
	if (equip[8] == 1)
	{
		auto equipment = Sprite::create("/equip/dao_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(1.9, 1.9);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 2)
	{
		auto equipment = Sprite::create("/equip/dao_long.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 3)
	{
		auto equipment = Sprite::create("/equip/dao_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 4)
	{
		auto equipment = Sprite::create("/equip/dun_mu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 5)
	{
		auto equipment = Sprite::create("/equip/dun_tie.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 6)
	{
		auto equipment = Sprite::create("/equip/gong_huang.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 7)
	{
		auto equipment = Sprite::create("/equip/gong_lv.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 8)
	{
		auto equipment = Sprite::create("/equip/Kai_bin.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 9)
	{
		auto equipment = Sprite::create("/equip/Kai_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 10)
	{
		auto equipment = Sprite::create("/equip/Kai_fuhuo.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 11)
	{
		auto equipment = Sprite::create("/equip/Kai_lan.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 12)
	{
		auto equipment = Sprite::create("/equip/Kai_bu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 13)
	{
		auto equipment = Sprite::create("/equip/Shoe_hui.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 14)
	{
		auto equipment = Sprite::create("/equip/Shoe_lan.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 15)
	{
		auto equipment = Sprite::create("/equip/Shoe_lv.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 16)
	{
		auto equipment = Sprite::create("/equip/fashu.png");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.51f);
		equipment->setScaleY(0.5f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	else if (equip[8] == 17)
	{
		auto equipment = Sprite::create("/equip/fashu_feng.jpg");
		equipment->setAnchorPoint(Vec2::ZERO);
		equipment->setScaleX(0.55f);
		equipment->setScaleY(0.56f);
		equipment->setPosition(2, 2.2);
		zhuangBei8->addChild(equipment, 1);
	}
	zhuangBei8->setPosition(124.7, 66.8);
	shopItem->addChild(zhuangBei8, 1);

	this->addChild(shoplayer, 8, 911);
}

void GameOnline::menuShopItemCloseCallback(cocos2d::Ref* pSender)
{
	this->getChildByTag(911)->removeFromParentAndCleanup(true);
}

void GameOnline::buyDaoCallBack1(cocos2d::Ref* pSender)
{
	auto DaoLabel11 = Label::createWithSystemFont("money:$250", "Arial", 30);
	auto DaoLabel12 = Label::createWithSystemFont("wuli_attack+20", "Arial", 30);
	DaoLabel11->setPosition(Vec2(1400, 300));
	DaoLabel12->setPosition(Vec2(1400, 350));
	this->addChild(DaoLabel11, 9, 21);
	this->addChild(DaoLabel12, 9, 22);
	auto itembuydao1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyDaoOkBack1, this));
	auto menu = Menu::create(itembuydao1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 23);
}


void GameOnline::buyDaoCallBack2(cocos2d::Ref* pSender)
{
	auto DaoLabel21 = Label::createWithSystemFont("money:$900", "Arial", 30);
	auto DaoLabel22 = Label::createWithSystemFont("wuli_attack+80", "Arial", 30);
	DaoLabel21->setPosition(Vec2(1400, 300));
	DaoLabel22->setPosition(Vec2(1400, 350));
	this->addChild(DaoLabel21, 9, 31);
	this->addChild(DaoLabel22, 9, 32);
	auto itembuydao2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyDaoOkBack2, this));
	auto menu = Menu::create(itembuydao2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 33);
}

void GameOnline::buyDaoCallBack3(cocos2d::Ref* pSender)
{
	auto DaoLabel31 = Label::createWithSystemFont("money:$2000", "Arial", 30);
	auto DaoLabel32 = Label::createWithSystemFont("wuli_attack+100,defend_fa+100", "Arial", 30);
	DaoLabel31->setPosition(Vec2(1400, 300));
	DaoLabel32->setPosition(Vec2(1400, 350));
	this->addChild(DaoLabel31, 9, 41);
	this->addChild(DaoLabel32, 9, 42);
	auto itembuydao3 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyDaoOkBack3, this));
	auto menu = Menu::create(itembuydao3, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 43);
}

void GameOnline::buyDunCallBack1(cocos2d::Ref* pSender)
{
	auto DunLabel11 = Label::createWithSystemFont("money:$900", "Arial", 30);
	auto DunLabel12 = Label::createWithSystemFont("defence_wu+110,cd-10%", "Arial", 30);
	DunLabel11->setPosition(Vec2(1400, 300));
	DunLabel12->setPosition(Vec2(1400, 350));
	this->addChild(DunLabel11, 9, 121);
	this->addChild(DunLabel12, 9, 122);
	auto itembuydun1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyDunOkBack1, this));
	auto menu = Menu::create(itembuydun1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 123);
}

void GameOnline::buyDunCallBack2(cocos2d::Ref* pSender)
{
	auto DunLabel21 = Label::createWithSystemFont("money:$730", "Arial", 30);
	auto DunLabel22 = Label::createWithSystemFont("wuli_attack+210", "Arial", 30);
	DunLabel21->setPosition(Vec2(1400, 300));
	DunLabel22->setPosition(Vec2(1400, 350));
	this->addChild(DunLabel21, 9, 131);
	this->addChild(DunLabel22, 9, 132);
	auto itembuydun2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyDunOkBack2, this));
	auto menu = Menu::create(itembuydun2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 133);
}

void GameOnline::buyGongCallBack1(cocos2d::Ref* pSender)
{
	auto GongLabel11 = Label::createWithSystemFont("money:$1100", "Arial", 30);
	auto GongLabel12 = Label::createWithSystemFont("wuli_attack+80", "Arial", 30);
	GongLabel11->setPosition(Vec2(1400, 300));
	GongLabel12->setPosition(Vec2(1400, 350));
	this->addChild(GongLabel11, 9, 221);
	this->addChild(GongLabel12, 9, 222);
	auto itembuygong1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyGongOkBack1, this));
	auto menu = Menu::create(itembuygong1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 223);
}

void GameOnline::buyGongCallBack2(cocos2d::Ref* pSender)
{
	auto GongLabel21 = Label::createWithSystemFont("money:$3400", "Arial", 30);
	auto GongLabel22 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	GongLabel21->setPosition(Vec2(1400, 300));
	GongLabel22->setPosition(Vec2(1400, 350));
	this->addChild(GongLabel21, 9, 231);
	this->addChild(GongLabel22, 9, 232);
	auto itembuygong2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyGongOkBack2, this));
	auto menu = Menu::create(itembuygong2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 233);
}

void GameOnline::buyKaiCallBack1(cocos2d::Ref* pSender)
{
	auto KaiLabel11 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto KaiLabel12 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	KaiLabel11->setPosition(Vec2(1400, 300));
	KaiLabel12->setPosition(Vec2(1400, 350));
	this->addChild(KaiLabel11, 9, 321);
	this->addChild(KaiLabel12, 9, 322);
	auto itembuykai1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyKaiOkBack1, this));
	auto menu = Menu::create(itembuykai1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 323);
}

void GameOnline::buyKaiCallBack2(cocos2d::Ref* pSender)
{
	auto KaiLabel21 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto KaiLabel22 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	KaiLabel21->setPosition(Vec2(1400, 300));
	KaiLabel22->setPosition(Vec2(1400, 350));
	this->addChild(KaiLabel21, 9, 331);
	this->addChild(KaiLabel22, 9, 332);
	auto itembuykai2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyKaiOkBack2, this));
	auto menu = Menu::create(itembuykai2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 333);
}

void GameOnline::buyKaiCallBack3(cocos2d::Ref* pSender)
{
	auto KaiLabel31 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto KaiLabel32 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	KaiLabel31->setPosition(Vec2(1400, 300));
	KaiLabel32->setPosition(Vec2(1400, 350));
	this->addChild(KaiLabel31, 9, 341);
	this->addChild(KaiLabel32, 9, 342);
	auto itembuykai3 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyKaiOkBack3, this));
	auto menu = Menu::create(itembuykai3, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 343);
}

void GameOnline::buyKaiCallBack4(cocos2d::Ref* pSender)
{
	auto KaiLabel41 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto KaiLabel42 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	KaiLabel41->setPosition(Vec2(1400, 300));
	KaiLabel42->setPosition(Vec2(1400, 350));
	this->addChild(KaiLabel41, 9, 351);
	this->addChild(KaiLabel42, 9, 352);
	auto itembuykai4 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyKaiOkBack4, this));
	auto menu = Menu::create(itembuykai4, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 353);
}

void GameOnline::buyKaiCallBack5(cocos2d::Ref* pSender)
{
	auto KaiLabel51 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto KaiLabel52 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	KaiLabel51->setPosition(Vec2(1400, 300));
	KaiLabel52->setPosition(Vec2(1400, 350));
	this->addChild(KaiLabel51, 9, 361);
	this->addChild(KaiLabel52, 9, 362);
	auto itembuykai5 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyKaiOkBack5, this));
	auto menu = Menu::create(itembuykai5, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 363);
}

void GameOnline::buyShoeCallBack1(cocos2d::Ref* pSender)
{
	auto ShoeLabel11 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto ShoeLabel12 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	ShoeLabel11->setPosition(Vec2(1400, 300));
	ShoeLabel12->setPosition(Vec2(1400, 350));
	this->addChild(ShoeLabel11, 9, 421);
	this->addChild(ShoeLabel12, 9, 422);
	auto itembuyshoe1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyShoeOkBack1, this));
	auto menu = Menu::create(itembuyshoe1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 423);
}

void GameOnline::buyShoeCallBack2(cocos2d::Ref* pSender)
{
	auto ShoeLabel21 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto ShoeLabel22 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	ShoeLabel21->setPosition(Vec2(1400, 300));
	ShoeLabel22->setPosition(Vec2(1400, 350));
	this->addChild(ShoeLabel21, 9, 431);
	this->addChild(ShoeLabel22, 9, 432);
	auto itembuyshoe2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyShoeOkBack2, this));
	auto menu = Menu::create(itembuyshoe2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 433);
}

void GameOnline::buyShoeCallBack3(cocos2d::Ref* pSender)
{
	auto ShoeLabel31 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto ShoeLabel32 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	ShoeLabel31->setPosition(Vec2(1400, 300));
	ShoeLabel32->setPosition(Vec2(1400, 350));
	this->addChild(ShoeLabel31, 9, 441);
	this->addChild(ShoeLabel32, 9, 442);
	auto itembuyshoe3 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyShoeOkBack3, this));
	auto menu = Menu::create(itembuyshoe3, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 443);
}

void GameOnline::buyFaCallBack1(cocos2d::Ref* pSender)
{
	auto FaLabel11 = Label::createWithSystemFont("money:$1000", "Arial", 30);
	auto FaLabel12 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	FaLabel11->setPosition(Vec2(1400, 300));
	FaLabel12->setPosition(Vec2(1400, 350));
	this->addChild(FaLabel11, 9, 521);
	this->addChild(FaLabel12, 9, 522);
	auto itembuyfa1 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyFaOkBack1, this));
	auto menu = Menu::create(itembuyfa1, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 523);
}

void GameOnline::buyFaCallBack2(cocos2d::Ref* pSender)
{
	auto FaLabel21 = Label::createWithSystemFont("money:$100", "Arial", 30);
	auto FaLabel22 = Label::createWithSystemFont("wuli_attack+100", "Arial", 30);
	FaLabel21->setPosition(Vec2(1400, 300));
	FaLabel22->setPosition(Vec2(1400, 350));
	this->addChild(FaLabel21, 9, 531);
	this->addChild(FaLabel22, 9, 532);
	auto itembuyfa2 = MenuItemImage::create("buy1.png", "buy2.png", CC_CALLBACK_1(GameOnline::buyFaOkBack2, this));
	auto menu = Menu::create(itembuyfa2, NULL);
	menu->setPosition(Vec2(1500, 250));
	this->addChild(menu, 9, 533);
}

void GameOnline::buyDaoOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 250 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 1;
		coin_num -= 250;
		power_wu_my += 20;
		this->getChildByTag(21)->removeFromParentAndCleanup(true);
		this->getChildByTag(22)->removeFromParentAndCleanup(true);
		this->getChildByTag(23)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyDaoOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 910 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 2;
		coin_num -= 910;
		power_wu_my += 80;
		cdmax -= 10;
		this->getChildByTag(31)->removeFromParentAndCleanup(true);
		this->getChildByTag(32)->removeFromParentAndCleanup(true);
		this->getChildByTag(33)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyDaoOkBack3(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 2000 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 3;
		coin_num -= 2000;
		power_wu_my += 100;
		defence_fa_my += 100;
		this->getChildByTag(41)->removeFromParentAndCleanup(true);
		this->getChildByTag(42)->removeFromParentAndCleanup(true);
		this->getChildByTag(43)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyDunOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 900 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 4;
		coin_num -= 900;
		defence_wu_my += 110;
		cdmax -= 10;
		this->getChildByTag(121)->removeFromParentAndCleanup(true);
		this->getChildByTag(122)->removeFromParentAndCleanup(true);
		this->getChildByTag(123)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyDunOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 730 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 5;
		coin_num -= 730;
		defence_wu_my += 210;
		this->getChildByTag(131)->removeFromParentAndCleanup(true);
		this->getChildByTag(132)->removeFromParentAndCleanup(true);
		this->getChildByTag(133)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyGongOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 1100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 6;
		coin_num -= 1100;
		power_wu_my += 80;
		cdmax -= 10;
		this->getChildByTag(221)->removeFromParentAndCleanup(true);
		this->getChildByTag(222)->removeFromParentAndCleanup(true);
		this->getChildByTag(223)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyGongOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 7;
		coin_num -= 100;
		power_wu_my += 250;
		defence_fa_my += 100;
		cdmax -= 10;
		this->getChildByTag(231)->removeFromParentAndCleanup(true);
		this->getChildByTag(232)->removeFromParentAndCleanup(true);
		this->getChildByTag(233)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyKaiOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 8;
		coin_num -= 100;
		defence_wu_my += 300;
		this->getChildByTag(321)->removeFromParentAndCleanup(true);
		this->getChildByTag(322)->removeFromParentAndCleanup(true);
		this->getChildByTag(323)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyKaiOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 9;
		coin_num -= 100;
		defence_wu_my += 200;
		defence_fa_my += 200;
		this->getChildByTag(331)->removeFromParentAndCleanup(true);
		this->getChildByTag(332)->removeFromParentAndCleanup(true);
		this->getChildByTag(333)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}



void GameOnline::buyKaiOkBack3(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 10;
		coin_num -= 100;
		defence_fa_my += 100;
		defence_wu_my += 100;
		hp_my += 500;
		this->getChildByTag(341)->removeFromParentAndCleanup(true);
		this->getChildByTag(342)->removeFromParentAndCleanup(true);
		this->getChildByTag(343)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyKaiOkBack4(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 11;
		coin_num -= 100;
		hp_my += 2000;
		this->getChildByTag(351)->removeFromParentAndCleanup(true);
		this->getChildByTag(352)->removeFromParentAndCleanup(true);
		this->getChildByTag(353)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyKaiOkBack5(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 12;
		coin_num -= 100;
		defence_fa_my += 400;
		hp_my += 800;
		this->getChildByTag(361)->removeFromParentAndCleanup(true);
		this->getChildByTag(362)->removeFromParentAndCleanup(true);
		this->getChildByTag(363)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyShoeOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 13;
		coin_num -= 100;
		v_my += 60;
		this->getChildByTag(421)->removeFromParentAndCleanup(true);
		this->getChildByTag(422)->removeFromParentAndCleanup(true);
		this->getChildByTag(423)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyShoeOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 14;
		coin_num -= 100;
		defence_wu_my += 100;
		v_my += 30;
		this->getChildByTag(431)->removeFromParentAndCleanup(true);
		this->getChildByTag(432)->removeFromParentAndCleanup(true);
		this->getChildByTag(433)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyShoeOkBack3(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 15;
		coin_num -= 100;
		defence_fa_my += 100;
		v_my += 30;
		this->getChildByTag(441)->removeFromParentAndCleanup(true);
		this->getChildByTag(442)->removeFromParentAndCleanup(true);
		this->getChildByTag(443)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyFaOkBack1(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 16;
		coin_num -= 100;
		power_fa_my += 500;
		this->getChildByTag(521)->removeFromParentAndCleanup(true);
		this->getChildByTag(522)->removeFromParentAndCleanup(true);
		this->getChildByTag(523)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

void GameOnline::buyFaOkBack2(cocos2d::Ref* pSender)
{
	int coin_num;
	coin_num = std::stoi(coin_my_ol->number);
	if (coin_num >= 100 && equipNumber <= 8)
	{
		equipNumber++;
		equip[equipNumber] = 17;
		coin_num -= 100;
		power_fa_my += 400;
		hp_my += 500;
		this->getChildByTag(531)->removeFromParentAndCleanup(true);
		this->getChildByTag(532)->removeFromParentAndCleanup(true);
		this->getChildByTag(533)->removeFromParentAndCleanup(true);
		std::stringstream ss;
		ss << coin_num;
		ss >> coin_my_ol->number;
	}
}

//������ȴ
void GameOnline::get_skill0()
{
	Sprite* pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 12 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite* pscdqianSp = Sprite::create("skill/skill_0.png");
	ProgressTimer* pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 12 + 150, 60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 200); //tag����Ϊ200

}
void GameOnline::get_skill1()
{
	Sprite* pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 13 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite* pscdqianSp = Sprite::create("skill/skill_1.png");
	ProgressTimer* pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 13 + 150, 60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 210); //tag����Ϊ210

}
void GameOnline::get_skill2()
{
	Sprite* pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 14 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite* pscdqianSp = Sprite::create("skill/aoe003/0_stand_0.png");
	ProgressTimer* pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 14 + 150, 60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 220); //tag����Ϊ220

}
void GameOnline::get_skill3()
{
	Sprite* pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 15 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite* pscdqianSp = Sprite::create("skill/aoe/0.png");
	ProgressTimer* pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 15 + 150, 60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 230); //tag����Ϊ230

}
void GameOnline::get_skill_cd0(float dt)
{
	auto progress = (ProgressTimer*)getChildByTag(200);
	progress->setPercentage((cd0 / cdmax) * 100);

	if (cd0 >= cdmax)
	{
		if_skill_kind_0 = true;
	}
	if (cd0 < cdmax)
	{
		cd0 += 10.000;
	}
}

void GameOnline::get_skill_cd1(float dt)
{
	auto progress = (ProgressTimer*)getChildByTag(210);
	progress->setPercentage((cd1 / cdmax) * 100);
	if (cd1 >= cdmax)
	{
		if_skill_kind_1 = true;

	}
	if (cd1 < cdmax)
	{
		cd1 += 10.000;
	}
}

void GameOnline::get_skill_cd2(float dt)
{
	auto progress = (ProgressTimer*)getChildByTag(220);
	progress->setPercentage((cd2 / cdmax) * 100);
	if (cd2 >= cdmax)
	{
		v_my = 70;
		if_skill_kind_2 = true;
	}
	if (cd2 < cdmax)
	{
		cd2 += 10.000;
	}
}

void GameOnline::get_skill_cd3(float dt)
{
	auto progress = (ProgressTimer*)getChildByTag(230);
	progress->setPercentage((cd3 / cdmax) * 100);
	if (cd3 >= cdmax)
	{

		if_skill_kind_3 = true;
	}
	if (cd3 < cdmax)
	{
		cd3 += 10.000;
	}
}


void GameOnline::get_time_change(float dt)
{
	int time_num;
	time_num = std::stoi(timecheck);
	time_num += 1;
	std::stringstream ss;
	ss << time_num;
	ss >> timecheck;
	removeChildByTag(300);
	get_time();
}


void GameOnline::get_time()
{
	Label* labeltime = Label::createWithTTF(timecheck, "fonts/Marker Felt.ttf", 60);
	labeltime->setPosition(Vec2(1140, 50));
	this->addChild(labeltime, 3, 300);//!!!!!!!!!!!!
}

void GameOnline::get_timename()
{
	Label* labeltime1 = Label::createWithTTF("Get Time By Seconds:", "fonts/Marker Felt.ttf", 60);
	labeltime1->setPosition(Vec2(880, 55));
	this->addChild(labeltime1, 3, 310);//!!!!!!!!!!!!

}


void GameOnline::qingling()
{
	tx = 0;//位置x
	ty = 0;//位置y
	hp = 0;//血量
	dengji = 0;//等级
	gongji = 0;//攻击力
	yisu = 0;//移速
	skill = 0;//技能类型
	//if_move = false;
}

/*void GameOnline::to_int(char arr[])
{
	int i, i1;
	char* ptr;
	char c_weizhi[16] = { 0 };
	if (arr[0] != 0)
	{
		for (i1 = 0, i = 0; arr[i] != '|'; i1++, i++)//位置x
		{
			c_weizhi[i] = arr[i];
		}
		tx_recv = strtod(c_weizhi, &ptr);

		char c_weizhi2[16] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//位置y
		{
			c_weizhi2[i1] = arr[i];
		}
		ty_recv = strtod(c_weizhi2, &ptr);

		char c[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//血量
		{
			c[i1] = arr[i];
		}
		hp_recv = strtod(c, &ptr);

		char c2[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//等级
		{
			c2[i1] = arr[i];
		}
		dengji_recv = strtod(c2, &ptr);

		char c3[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//移速
		{
			c3[i1] = arr[i];
		}
		yisu_recv = strtod(c3, &ptr);

		i++;
		skill = arr[i] - 48;
	}//技能类型

}*/

char* GameOnline::to_char()
{
	memset(data_s, '/0', 64);
	char weizhi[15] = { 0 };
	int i, i1;
	sprintf(weizhi, "%f", tx);
	for (i = 0, i1 = 0;; i1++, i++)
	{
		if (weizhi[i1] != 0)
			data_s[i] = weizhi[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}

	char weizhi2[15] = { 0 };
	sprintf(weizhi2, "%f", ty);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (weizhi2[i1] != 0)
			data_s[i] = weizhi2[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}

	char c1[6] = { 0 };
	sprintf(c1, "%d", hp);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (c1[i1] != 0)
			data_s[i] = c1[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}
	//log(arr);
	char c2[6] = { 0 };
	sprintf(c2, "%d", dengji);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (c2[i1] != 0)
			data_s[i] = c2[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}
	//log(arr);
	char c3[6] = { 0 };
	sprintf(c3, "%d", yisu);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (c3[i1] != 0)
			data_s[i] = c3[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}

	char c4[6] = { 0 };
	sprintf(c4, "%d", gongji);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (c4[i1] != 0)
			data_s[i] = c4[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}//

	char c5[6] = { 0 };
	sprintf(c5, "%d", hp_max);
	for (i++, i1 = 0;; i1++, i++)
	{
		if (c5[i1] != 0)
			data_s[i] = c5[i1];
		else
		{
			data_s[i] = '|';
			break;
		}
	}

	i++;
	data_s[i] = skill + 48;
	i++;
	data_s[i] = '|';
	return data_s;
}

void GameOnline::check_begin(float dt)
{
	if ((client1->get_id() ==1|| client1->get_id() == 2))
	{
		if (if_begin == false)
		{
			id = client1->get_id();

			log(id);

			if_begin = true;

			set_hero_map();

			set_jianting();
		}
		else 
		{
			jieshou_message();
		}

	}
}

void GameOnline::jieshou_message()
{
	if (if_move == true)
	{
		if (skill_recv == 0)
		{
			hero_enemy_ol->get_sprite()->stopAllActions();
			int x = hero_enemy_ol->get_position().x;
			int y = hero_enemy_ol->get_position().y;
			float iv = 70;//iv是移动速度
			Vec2 diff = Vec2(tx_recv, ty_recv) - hero_enemy_ol->get_position();
			float juli = sqrt(diff.x * diff.x + diff.y * diff.y);

			auto* move_action = MoveTo::create(juli / iv, Vec2(tx_recv, ty_recv));
			auto* run_animate = hero_enemy_ol->create_animate_walk(hero_enemy_ol->get_direction(diff.x, diff.y), "run", juli / iv);
			auto* stop_animate = hero_enemy_ol->create_animate_walk(hero_enemy_ol->get_direction(diff.x, diff.y), "stop", 0);
			auto* sequence = Sequence::create(move_action, stop_animate, NULL);

			hero_enemy_ol->get_sprite()->runAction(run_animate);
			hero_enemy_ol->get_sprite()->runAction(sequence);
		}
		/*if (skill_recv == 1)
		{
			skill = 0;
			hero_enemy_ol->get_sprite()->stopAllActions();
			Vec2 diff = Vec2(tx_recv,ty_recv)- hero_enemy_ol->get_position();
			auto* stop_animate = hero_enemy_ol->create_animate_walk(hero_enemy_ol->get_direction(diff.x, diff.y), "stop", 0);
			hero_enemy_ol->get_sprite()->runAction(stop_animate);
			skill_kind1 = 0;
			hero_attack(hero_enemy_ol->get_position(), Vec2(tx_recv, ty_recv));
			skill_recv = 0;
		}*/


		if_move = false;


	}


}









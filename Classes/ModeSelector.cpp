#include "ModeSelector.h"
#include "GameScene.h"
#include "SocketClient.h"

Client Cli;
Scene* Mode::createScene()
{
	return Mode::create();
}

bool Mode::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->setHuman();
	this->schedule(schedule_selector(Mode::ListenIfStart), 0.001f);

	MenuItemFont::setFontName("Arial");
	MenuItemFont::setFontSize(100);

	MenuItemFont *ready = MenuItemFont::create("Ready", CC_CALLBACK_1(Mode::menuReadyCallback, this));
	ready->setPosition(origin.x + visibleSize.width / 2.0, origin.y + visibleSize.height / 3.0);

	Menu *menu = Menu::create(ready, NULL);
	menu->setPosition(Vec2::ZERO);
	//	log("%f,%f", back->getPosition().x, back->getPosition().y);
	this->addChild(menu, 1);

	return true;
}

void Mode::menuReadyCallback(cocos2d::Ref* pSender)
{
	log("ready\n");

	Cli.MessageSending("Start");
	if (Mode::isAI())
		IfStart = 1;

}

void Mode::ListenIfStart(float dt)
{
	if (IfStart == 1)
	{
		IfStart = 0;
		auto gs = Game::createScene();
		Director::getInstance()->replaceScene(gs);
	}
}
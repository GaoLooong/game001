#include"HeroChoose.h"
#include"HelloWorldScene.h"
#include"SimpleAudioEngine.h"
#include"GameScene.h"
#include"Hero.h"

USING_NS_CC;

int heroNumber = 1;

Scene* HeroChooseScene::createHeroChooseScene()
{
	return HeroChooseScene::create();
}

static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool HeroChooseScene::init()
{
	if (!Scene::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto background = Sprite::create("herochoose.jpg");
	if (background == nullptr)
	{
		problemLoading("'herochoose.jpg'");
	}
	auto size = background->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	background->setScale(scale);
	background->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(background);

	auto hero1button = MenuItemImage::create("houyi.jpg", "houyi.jpg",
		CC_CALLBACK_1(HeroChooseScene::Hero1ButtonCallBack, this));
	hero1button->setPosition(Vec2(400, 500));
	hero1button->setScaleX(2.0f);
	hero1button->setScaleY(2.0f);

	auto hero2button = MenuItemImage::create("daji.jpg", "daji.jpg",
		CC_CALLBACK_1(HeroChooseScene::Hero2ButtonCallBack, this));
	hero2button->setPosition(Vec2(1100, 500));
	hero2button->setScaleX(2.1f);
	hero2button->setScaleY(1.9f);

	auto hero3button = MenuItemImage::create("yase.jpg", "yase.jpg",
		CC_CALLBACK_1(HeroChooseScene::Hero3ButtonCallBack, this));
	hero3button->setPosition(Vec2(1700, 500));
	hero3button->setScaleX(1.5f);
	hero3button->setScaleY(1.5f);

	Menu* menu = Menu::create(hero1button,hero2button,hero3button, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);
	return true;
}


void HeroChooseScene::Hero1ButtonCallBack(Ref* pSender)
{
	extern int heroNumber;
	heroNumber = 1;
	if (i == 0)
	{
		auto game_scene = Game::createScene();
		Director::getInstance()->pushScene(game_scene);
		i++;
	}
	if (i == 1)
	{
		auto game_scene1 = Game::createScene();
		Director::getInstance()->pushScene(game_scene1);
		i++;
	}
}

void HeroChooseScene::Hero2ButtonCallBack(Ref* pSender)
{
	extern int heroNumber;
	heroNumber = 2;
	auto game_scene = Game::createScene();
	Director::getInstance()->pushScene(game_scene);
}

void HeroChooseScene::Hero3ButtonCallBack(Ref* pSender)
{
	extern int heroNumber;
	heroNumber = 3;
	auto game_scene = Game::createScene();
	Director::getInstance()->pushScene(game_scene);
}
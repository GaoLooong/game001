#include "zhanji.h"

void Zhanji::init_zhanji(int x, int y)
{
	zhanji= Sprite::create("zhanji.png");
	zhanji->setPosition(Vec2(x, y));
	kill="0";
	death="0";
	this->get_zhanji(kill, death);
	addChild(zhanji);
}

Vec2 Zhanji::get_position()
{
	Vec2 zhanji_position = zhanji->getPosition();
	return zhanji_position;
}
void Zhanji::get_zhanji(std::string kill, std::string death)
{
	Label *label = Label::createWithTTF(kill, "fonts/Marker Felt.ttf", 40);
	label->setPosition(Vec2(75, 10));
	zhanji->addChild(label, 1, 110);
	Label *label1 = Label::createWithTTF(death, "fonts/Marker Felt.ttf", 40);
	label1->setPosition(Vec2(120, 10));
	zhanji->addChild(label1,1, 120);
}

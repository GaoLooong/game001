#ifndef _HEROCHOOSE_H__
#define _HEROCHOOSE_H__
#include"cocos2d.h"

class HeroChooseScene :public cocos2d::Scene
{
public:
	static cocos2d::Scene* createHeroChooseScene();

	virtual bool init();
	int i;

	void Hero1ButtonCallBack(cocos2d::Ref* pSender);
	void Hero2ButtonCallBack(cocos2d::Ref* pSender);
	void Hero3ButtonCallBack(cocos2d::Ref* pSender);
	CREATE_FUNC(HeroChooseScene);
};
#endif // _HEROCHOOSE_H__

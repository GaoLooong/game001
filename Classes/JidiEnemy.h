#pragma once
#include "cocos2d.h"

USING_NS_CC;

class JidiEnemy :public cocos2d::CCNode
{
public:
	Sprite* jidi;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	float hpLimit;
	float hp_now;

	void init_jidi(int x, int y);//������ʼ��������

	Sprite* JidiEnemy::get_sprite();
	Rect JidiEnemy::get_attack_rect();
	Rect JidiEnemy::get_attacked_rect();
	void getBloodbar(Sprite* jidi, float hpLimit, float hp_now);

	Vec2 JidiEnemy::get_position();
	CREATE_FUNC(JidiEnemy);
};

#include "cocos2d.h"
#include "BingEnemy.h"

USING_NS_CC;

class Bing :public cocos2d::CCNode
{
public:
	Sprite* bing;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	cocos2d::Vec2 runto_position;
	int mubiao;
	int direction;
	Sprite* mubiao_sprite;

	BingEnemy* mubiao_bing;

	bool is_attacking;
	bool if_check_hurt;
	float iv;
	int tag;
	void Bing::init_bing(int x, int y,float runto_x,float runto_y,int itag);//用来初始化防御塔
	void Bing::auto_runto();
	Vec2 Bing::get_position();
	Rect Bing::get_attack_rect();
	Rect Bing::get_attacked_rect();
	float hpLimit;//最大血量
	float hp_now;
	Sprite* Bing::get_sprite();
	int Bing::get_direction(int x, int y);
	Animate* Bing::animate_walk(int direction, const char* action, float time);
	void Bing::run(Vec2 diff);
	void Bing::getBloodbar(Sprite* bing, float hpLimit, float hp_now);
	void Bing::hurt_check();
	void Bing::attack(Sprite* enemy, Rect rect);

	CREATE_FUNC(Bing);
};

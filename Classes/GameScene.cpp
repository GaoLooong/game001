#include "HelloWorldScene.h"
#include "GameScene.h"
#include"math.h"
USING_NS_CC;

////////////////
// 1为我方英雄
// 2为敌方英雄
// 3为我方小兵
// 4为敌方小兵
// 5为我方防御塔
// 6为敌方防御塔
////////////////

int  skill_kind;
int bing_enemy_tag = 0;
int bing_my_tag = 0;
std::string timecheck="0";
std::string level = "0";
float cdmax,cd1,cd2,cd3,cd0;

float power_fa_my, power_wu_my, power_wu_enemy, power_fa_enemy, defence_fa_my, defence_fa_enemy, defence_wu_my, defence_wu_enemy, hp_my, hp_enemy,v_my,v_enemy;

//level == "0";
Scene* Game::createScene()
{
	return Game::create();
}

static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

bool Game::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
	
	
	cdmax=100.0000;
	cd1 = cd2 = cd3 = cd0 = 100.0000;
	power_wu_enemy = 100;
	power_wu_my = 10000;
	power_fa_enemy = power_fa_my = 100;
	defence_fa_my = defence_fa_enemy = defence_wu_my = defence_wu_enemy=0;
	hp_my = 1500;
	hp_enemy = 1500;
	v_my = 70;
	v_enemy = 70;
	
	Game::get_timename();

	set_hero_map();

	set_jianting();
	
	
	Game::get_skill0();

	Game::get_skill1();

	Game::get_skill2();

	Game::get_skill3();

	if_attack = false;

	tower_enemy->is_attacking = false;//保证防御塔攻击之后只攻击同一个人

	this->schedule(schedule_selector(Game::updateView), 0.01f);

	this->schedule(schedule_selector(Game::rect_check_tower), 1.0f);

	this->schedule(schedule_selector(Game::rect_check_bing), 0.8f);

	this->schedule(schedule_selector(Game::rect_check_heroAi), 0.5f);




	this->schedule(schedule_selector(Game::hurt_check), 0.1f);

	this->schedule(schedule_selector(Game::bing_shuaxin), 10.0f);

	this->schedule(schedule_selector(Game::EXP_check), 1.0f);

	this->schedule(schedule_selector(Game::EXP_change), 1.0f);

	this->schedule(schedule_selector(Game::get_coin_by_killing_bing), 1.0f);

	this->schedule(schedule_selector(Game::get_coin_by_killing_tower), 1.0f);

	this->schedule(schedule_selector(Game::get_coin_by_killing_hero), 1.0f);
	
	this->schedule(schedule_selector(Game::get_coin_by_time), 1.0f);
	
	this->schedule(schedule_selector(Game::get_skill_cd0), 0.2f);

	this->schedule(schedule_selector(Game::get_skill_cd1), 0.5f);

	this->schedule(schedule_selector(Game::get_skill_cd2), 1.0f);

	this->schedule(schedule_selector(Game::get_skill_cd3), 1.5f);

	this->schedule(schedule_selector(Game::get_time_change), 1.0f);


	return true;
}

/*int getDirection(int x, int y)//获得 行走的 方向
{
	/////////////////1-8是从12点方向顺时针//////////////
	int direction = 0;
	if (x == 0 || y == 0)
		direction = 1;
	else if (x > 0 && x * x / (y * y) > 10)		//RIGHT
		direction = 3;
	else if (x < 0 && x * x / (y * y)>10)		//LEFT
		direction = 7;
	else if (y > 0 && y * y / (x * x) > 10)	 //UP
		direction = 1;
	else if (y < 0 && y * y / (x * x)  >10)	//DOWN
		direction = 5;
	else if (x > 0 && y < 0)					    //RIGHT-DOWN
		direction = 4;
	else if (x < 0 && y < 0)					   //LEFT-DOWN
		direction = 6;
	else if (x < 0 && y > 0)			          //LEFT-UP
		direction = 8;
	else if (x > 0 && y > 0)					 //RIGHT-UP
		direction = 2;

	return direction;
}

Animate* create_animate_walk(int direction, const char* action, float time)//建立动作类动画
{
	Animation* animation = Animation::create();

	//Vector <SpriteFrame*> frameArray;
	if (action == "run")//奔跑时的动画
	{
		char  file_name[20] = { 0 };
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "hero/run_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else//八个方向上的最后站立
	{
		char  file_name[20] = { 0 };
		sprintf(file_name, "hero/stop_%d.png", direction);
		animation->addSpriteFrameWithFile(file_name);
	}

	animation->setDelayPerUnit(0.1f);
	if (action == "stop")
		animation->setLoops(1);
	else
		animation->setLoops(time / 0.8);//几张图片的走路动画就/0.1*几

	Animate* animate = Animate::create(animation);
	return animate;
}*/

void Game::set_jianting()
{
	//////////////触摸监听器
	auto touch = EventListenerTouchOneByOne::create();//触摸事件
	touch->onTouchBegan = CC_CALLBACK_2(Game::touchBegin, this);

	EventDispatcher* touch_jianting = Director::getInstance()->getEventDispatcher();//触摸事件 监听器

	touch_jianting->addEventListenerWithSceneGraphPriority(touch, hero_my);

	////////////键盘监听器
	auto keyboard = EventListenerKeyboard::create();
	keyboard->onKeyReleased = [this](EventKeyboard::KeyCode keyCode, Event* event)
	{
		if (keyCode == EventKeyboard::KeyCode::KEY_A&&(if_skill_kind_0 == true))
		{
			hero_my->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my->get_position();
			auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
			hero_my->get_sprite()->runAction(stop_animate);
			if_attack = true;
			skill_kind = 0;

		}
		if (keyCode == EventKeyboard::KeyCode::KEY_S&&(if_skill_kind_1 == true))
		{
			hero_my->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my->get_position();
			auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
			hero_my->get_sprite()->runAction(stop_animate);
			if_attack = true;
			skill_kind = 1;
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_D&&(if_skill_kind_2 == true))
		{
			/*hero_my->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my->get_position();
			auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
			hero_my->get_sprite()->runAction(stop_animate);
			if_attack = true;*/
			v_my=150;
			
			if_skill_kind_2=0;
			cd2=0.00000;
			skill_kind = 2;
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_R&&(if_skill_kind_3 == true))
		{
			hero_my->get_sprite()->stopAllActions();
			Vec2 diff = touch_position - hero_my->get_position();
			auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
			hero_my->get_sprite()->runAction(stop_animate);

			hero_my->animate_aoe();

			if (hero_my->get_attack_rect().containsPoint(hero_enemy->get_position()))
			{
				hero_enemy->hp_now -= 3*power_wu_my;
				if (hero_enemy->hp_now <= 0)
				{
					if_attack = false;
					hero_enemy->get_sprite()->setPosition(0, 0);
					removeChild(hero_enemy);
				}
			}
			for (auto it_attacked = bing_enemy_vector.begin(); it_attacked != bing_enemy_vector.end(); it_attacked++)
			{
				if (hero_my->get_attack_rect().containsPoint((*it_attacked)->get_position()))
				{
					(*it_attacked)->hp_now -= 3*power_wu_my;
					cd3=0.000;
					if_skill_kind_3=false;
					if ((*it_attacked)->hp_now <= 0)
					{
						if_attack = false;
						int itag = (*it_attacked)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it_attacked)->direction);
						dead_man->setPosition((*it_attacked)->get_position());
						addChild(dead_man);

						(*it_attacked)->get_sprite()->setPosition(0, 0);
						removeChild((*it_attacked));

						//if (bing_enemy_vector.end() - bing_enemy_vector.begin() != 1)
							//it_attacked = bing_enemy_vector.erase(it_attacked);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);
					}
					
				}
				
			}
			skill_kind = 3;
		}
	};

	EventDispatcher* eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->addEventListenerWithSceneGraphPriority(keyboard, this);

}

void Game::set_hero_map()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	////////////////创建地图
	tile_map = TMXTiledMap::create("map/map_tiled.tmx");//添加瓦片地图
	auto size = tile_map->getContentSize();
	float scaleX = visibleSize.width / size.width;
	float scaleY = visibleSize.height / size.height;
	float scale = scaleX > scaleY ? scaleX : scaleY;
	tile_map->setScale(scale);

	addChild(tile_map, 0, 100);//将瓦片地图放在第0层
	pengzhuang_layer = tile_map->getLayer("pengzhuang");//将wall层设置为碰撞层

	TMXObjectGroup* group = tile_map->getObjectGroup("object");//获取对象层信息

	////////////////创建防御塔
	auto spawnPoint = group->getObject("tower_enemy");
	float x = spawnPoint["x"].asFloat();
	float y = spawnPoint["y"].asFloat();

	tower_enemy = new TowerEnemy();
	tower_enemy->init_tower(x, y);
	this->addChild(tower_enemy);//将塔放在三层

	spawnPoint = group->getObject("tower_my");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	tower_my = new Tower();
	tower_my->init_tower(x, y);
	this->addChild(tower_my);

	////////////////创建基地
	spawnPoint = group->getObject("jidi_enemy");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	jidi_enemy = new JidiEnemy();
	jidi_enemy->init_jidi(x, y);
	this->addChild(jidi_enemy);//将塔放在二层

	spawnPoint = group->getObject("jidi_my");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	jidi_my = new Jidi();
	jidi_my->init_jidi(x, y);
	this->addChild(jidi_my);

	/////////////创建英雄
	spawnPoint = group->getObject("hero_my");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	hero_my = new Hero();
	hero_my->init_hero(x, y);
	hero_my->hp_now = hp_my;
	this->addChild(hero_my, 1,110);


	spawnPoint = group->getObject("hero_enemy");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	auto spawnPoint_runto = group->getObject("tower_my");
	auto x_runto = spawnPoint_runto["x"].asFloat();
	auto y_runto = spawnPoint_runto["y"].asFloat();

	hero_enemy = new HeroAi();
	hero_enemy->init_hero_ai(x,y,x_runto,y_runto);
	hero_enemy->hp_now = hp_my;
	this->addChild(hero_enemy, 1);



	///////////////创建小兵
	for (int i = 0; i < 3; i++)
	{
		auto spawnPoint = group->getObject("bing_enemy_jin");
		if (i == 0)
			spawnPoint = group->getObject("bing_enemy_jin");
		if (i == 1)
			spawnPoint = group->getObject("bing_enemy_yuan");
		if (i==2)
			spawnPoint = group->getObject("bing_enemy_pao");

		auto x = spawnPoint["x"].asFloat();
		auto y = spawnPoint["y"].asFloat();

		auto spawnPoint_runto = group->getObject("tower_my");
		auto x_runto = spawnPoint_runto["x"].asFloat();
		auto y_runto = spawnPoint_runto["y"].asFloat();
		bing_enemy = new BingEnemy();
		bing_enemy_tag++;
		bing_enemy->init_bing(x, y, x_runto, y_runto, bing_enemy_tag);
		bing_enemy_vector.pushBack(bing_enemy);
		this->addChild(bing_enemy);
	}

	for (int i = 0; i < 3; i++)
	{
		auto spawnPoint = group->getObject("bing_my_jin");
		if (i == 0)
			spawnPoint = group->getObject("bing_my_jin");
		if (i == 1)
			spawnPoint = group->getObject("bing_my_yuan");
		if (i == 2)
			spawnPoint = group->getObject("bing_my_pao");

		auto x = spawnPoint["x"].asFloat();
		auto y = spawnPoint["y"].asFloat();

		auto spawnPoint_runto = group->getObject("tower_enemy");
		auto x_runto = spawnPoint_runto["x"].asFloat();
		auto y_runto = spawnPoint_runto["y"].asFloat();
		bing_my = new Bing();
		bing_my_tag++;
		bing_my->init_bing(x, y, x_runto, y_runto, bing_my_tag);
		bing_my_vector.pushBack(bing_my);
		
		this->addChild(bing_my);
	}

	/*spawnPoint = group->getObject("bing_my");
	x = spawnPoint["x"].asFloat();
	y = spawnPoint["y"].asFloat();

	spawnPoint_runto = group->getObject("tower_enemy");
	x_runto = spawnPoint_runto["x"].asFloat();
	y_runto = spawnPoint_runto["y"].asFloat();
	bing_my = new Bing();
	bing_my->init_bing(x, y, x_runto, y_runto);
	this->addChild(bing_my, 3);*///将小兵放在第三层


	zhanji_my = new Zhanji();
	zhanji_my->init_zhanji(1900, 60);
	this->addChild(zhanji_my, 2);

	coin_my = new Coin();
	coin_my->init_coin(x - 500, y + 500);
	this->addChild(coin_my, 10);

	///////////商店界面
	MenuItemImage* shop_button = MenuItemImage::create("shop_button1.png", "shop_button2.png",
		CC_CALLBACK_1(Game::menuShopItemOpenCallback, this));//商店按钮
	shop_button->setPosition(Vec2(50, visibleSize.height - 50));
	shop_button->setScale(0.8f);

	auto menuShop = Menu::create(shop_button, NULL);
	menuShop->setPosition(Vec2::ZERO);
	this->addChild(menuShop, 1);

}

void Game::pengzhuang_check()//hero_position就是当前位置
{
	Vec2 position_now = hero_my->get_position();//hero_position是精灵时刻的位置
	Vec2 tile_position_now = this->tileCoordFromPosition(position_now);//从像素点 坐标转化为瓦片坐标
	int tile_gid_now = pengzhuang_layer->getTileGIDAt(tile_position_now);//获得英雄 瓦片的gid
	Vec2 diff = last_position - position_now;
	if (tile_gid_now > 0 && hero_my->hp_now>0)
	{
		hero_my->get_sprite()->stopAllActions();
		Vec2 diff_stop = touch_position - hero_my->get_position();
		auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff_stop.x, diff_stop.y), "stop", 0);
		hero_my->get_sprite()->runAction(stop_animate);
		hero_my->get_sprite()->setPosition(last_position);
	}
	else
		last_position = position_now;

}

Vec2 Game::tileCoordFromPosition(Vec2 pos)
{
	int x = pos.x / tile_map->getTileSize().width;
	int y = ((tile_map->getMapSize().height * tile_map->getTileSize().height) - pos.y)
		/ tile_map->getTileSize().height;
	return Vec2(x, y);
}

/*Animate* create_animate_attacked()
{
	Animation* animation_attacked = Animation::create();
	char  file_name[30] = { 0 };
	for (int i = 0; i < 2; i++)
	{

		sprintf(file_name, "hero/hero2_small_0.png");
		animation_attacked->addSpriteFrameWithFile(file_name);
	}
	animation_attacked->addSpriteFrameWithFile(file_name);
	animation_attacked->setDelayPerUnit(0.1f);
	animation_attacked->setLoops(1);
	animation_attacked->setRestoreOriginalFrame(true);
	Animate* animate_attacked = Animate::create(animation_attacked);
	return animate_attacked;
}*/

/*Animate* create_animate_skill(float iv, float juli, int skill_kind)
{
	Animation* animation = Animation::create();
	char  file_name[20] = { 0 };
	if (skill_kind == 0)
	{
		sprintf(file_name, "skill/skill_0.png");
		animation->addSpriteFrameWithFile(file_name);
	}

	if (skill_kind == 1)
	{
		for (int i = 0; i < 2; i++)
		{
			sprintf(file_name, "skill/skill_%d.png", i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	if (skill_kind == 2)
	{

		sprintf(file_name, "skill/skill_2.png");
		animation->addSpriteFrameWithFile(file_name);

	}
	if (skill_kind == 3)
	{
		sprintf(file_name, "skill/skill_3.png");
		animation->addSpriteFrameWithFile(file_name);
	}
	animation->setDelayPerUnit(0.1f);
	animation->setLoops(juli * 5 / iv);
	Animate* animate = Animate::create(animation);
	return animate;

}*/

void Game::hero_attack(Vec2 attack, Vec2 attacked)
{
	Vec2 attack_position = attack;
	Vec2 attacked_position = attacked;
	Vec2 diff = attack_position - attacked_position;//是触点和和hero之间的距离
	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));
	if (skill_kind == 0)
	{
		Sprite* skill0 = Sprite::create("skill/skill_0.png");
		skill0->setPosition(attack_position);
		addChild(skill0);

		auto* animate0 = hero_my->create_animate_skill(iv, juli, skill_kind);
		auto* move_action0 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi0 = CallFunc::create([skill0, this]() {skill0->removeFromParent(); });
		auto* sequence0 = Sequence::create(move_action0, xiaoshi0, NULL);

		skill0->runAction(animate0);
		skill0->runAction(sequence0);
	}
	//第一个技能
	if (skill_kind == 1)
	{
		Sprite* skill1 = Sprite::create("skill/skill_1.png");
		skill1->setPosition(attack_position);
		addChild(skill1);

		auto* animate1 = hero_my->create_animate_skill(iv, juli, skill_kind);
		auto* move_action1 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi1 = CallFunc::create([skill1, this]() {skill1->removeFromParent(); });
		auto* sequence1 = Sequence::create(move_action1, xiaoshi1, NULL);

		skill1->runAction(animate1);
		skill1->runAction(sequence1);
	}
	//第二个技能
	/*if (skill_kind == 2)
	{
		Sprite* skill2 = Sprite::create("skill/skill_2.png");
		skill2->setPosition(attack_position);
		addChild(skill2);

		auto* animate2 = hero_my->create_animate_skill(iv, juli, skill_kind);
		auto* move_action2 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi2 = CallFunc::create([skill2, this]() {skill2->removeFromParent(); });
		auto* sequence2 = Sequence::create(move_action2, xiaoshi2, NULL);

		skill2->runAction(animate2);
		skill2->runAction(sequence2);
	}*/
	//第三个技能
	/*if (skill_kind == 3)
	{
		Sprite* skill3 = Sprite::create("skill/skill_3.png");
		skill3->setPosition(attack_position);
		addChild(skill3);

		auto* animate3 = hero_my->create_animate_skill(iv, juli, skill_kind);
		auto* move_action3 = MoveTo::create(juli / iv, attacked_position);
		auto* xiaoshi3 = CallFunc::create([skill3, this]() {skill3->removeFromParent(); });
		auto* sequence3 = Sequence::create(move_action3, xiaoshi3, NULL);

		skill3->runAction(animate3);
		skill3->runAction(sequence3);

	}*/
}

bool Game::touchBegin(Touch* touch, Event* event)

{
	//Vec2 current_position = hero_my->getPosition();
	touch_position = touch->getLocation();
	Vec2 diff = touch_position - hero_my->get_position();//是触点和和hero之间的距离

	if (if_attack == false)//没按键,走路
	{
		hero_my->get_sprite()->stopAllActions();
		float iv = v_my;//iv是移动速度
		float juli = sqrt(diff.x * diff.x + diff.y * diff.y);

		auto* move_action = MoveTo::create(juli / iv, touch_position);
		auto* run_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "run", juli / iv);
		auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
		auto* sequence = Sequence::create(move_action, stop_animate, NULL);

		Vec2 tileCoord = this->tileCoordFromPosition(touch_position);
		//获得瓦片的GID
		int tileGid = pengzhuang_layer->getTileGIDAt(tileCoord);//只有碰撞层时
		if (tileGid > 0)
		{
			//is_walk = false;
			hero_my->get_sprite()->stopAllActions();
			auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
			hero_my->get_sprite()->runAction(stop_animate);
			//return true;
		}
		hero_my->get_sprite()->runAction(run_animate);
		hero_my->get_sprite()->runAction(sequence);
	}
	if (if_attack == true)//按键攻击后再次按键释放技能
	{
		auto* stop_animate = hero_my->create_animate_walk(hero_my->get_direction(diff.x, diff.y), "stop", 0);
		hero_my->get_sprite()->runAction(stop_animate);

			if (hero_my->get_attack_rect().containsPoint(touch_position))//如果攻击在攻击范围内
			{
				//hero_attack(hero_my->get_position(), touch_position);
				for (auto it_attacked = bing_enemy_vector.begin();
					it_attacked != bing_enemy_vector.end(); it_attacked++)
				{
					if (hero_enemy->get_attacked_rect().containsPoint(touch_position))//如果英雄2在技能范围内
					{
						/////////添加受攻击动画，用一个深色帧就好
						/////////伤害判断kk
						if (skill_kind == 0&&(if_skill_kind_0 == true))
						{
							cd0 = 0.000;
							if_skill_kind_0 = false;
							hero_enemy->hp_now -= power_wu_my;
                         }
						if (skill_kind == 1&&(if_skill_kind_1 == true))
						{
							cd1 = 0.000;
							if_skill_kind_1 = false;
							hero_enemy->hp_now -= power_wu_my*2;
						}
						/*if (skill_kind == 2)
							hero_enemy->hp_now -= 300;

						/*if (skill_kind == 3)
							hero_enemy->hp_now -= 400;*/
						if (hero_enemy->hp_now <= 0)
						{
							if_attack = false;
							hero_enemy->get_sprite()->setPosition(0, 0);
							removeChild(hero_enemy);
							
						}
					}
					if (tower_enemy->rect_attacked.containsPoint(touch_position))
					{
						if (skill_kind == 0&&(if_skill_kind_0 == true))
						{
							cd0 = 0.000;
							if_skill_kind_0 = false;
					    	tower_enemy->hp_now -= power_wu_my;;
					    }
						if (skill_kind == 1&&(if_skill_kind_1 == true))
						{
							cd1 = 0.000;
							if_skill_kind_1 = false;
							tower_enemy->hp_now -= power_wu_my*2;
						} 
						/*if (skill_kind == 2)
							tower_enemy->hp_now -= 300;
						/*if (skill_kind == 3)
							tower_enemy->hp_now -= 400;*/
						if (tower_enemy->hp_now <= 0)
						{
							if_attack = false;
							tower_enemy->get_sprite()->setPosition(0, 0);
							removeChild(tower_enemy);
						}
					}
					if ((*it_attacked)->get_attacked_rect().containsPoint(touch_position))
					{
						if (skill_kind == 0&&(if_skill_kind_0 == true))
						{
							(*it_attacked)->hp_now -= power_wu_my;
							cd0 = 0.000;
							if_skill_kind_0 = false;
						}
						if (skill_kind == 1&&(if_skill_kind_1 == true))
						{
							(*it_attacked)->hp_now -= power_wu_my*2;
							cd1 = 0.000;
							if_skill_kind_1 = false;
						}
						/*if (skill_kind == 2)
							(*it_attacked)->hp_now -= 300;
						/*if (skill_kind == 3)
							(*it_attacked)->hp_now -= 400;*/
						if ((*it_attacked)->hp_now <= 0)
						{
							if_attack = false;
							int itag = (*it_attacked)->tag;
							char  file_name[50] = { 0 };
							if (itag % 3 == 0)
								sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
							else if ((itag + 1) % 3 == 0)
								sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
							else
								sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
							Sprite* dead_man = Sprite::create(file_name);
							Animate* animate = animate_dead(itag, (*it_attacked)->direction);
							dead_man->setPosition((*it_attacked)->get_position());
							addChild(dead_man);

							(*it_attacked)->get_sprite()->setPosition(0, 0);
							removeChild((*it_attacked));

							//if (bing_enemy_vector.end()- bing_enemy_vector.begin()!= 1)
								//it_attacked = bing_enemy_vector.erase(it_attacked);
							//it_attacked = bing_enemy_vector.erase(it_attacked);

							auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
							auto* sequence = Sequence::create(animate, xiaoshi, NULL);
							dead_man->runAction(sequence);
						}
					}


				}	
				if_attack = false;
			}
		else//如果超出了自己的攻击范围
		if_attack = false;
	}
	return true;
}

void  Game::updateView(float dt)
{
	pengzhuang_check();
    hero_my->hpLimit = hp_my;
	hero_my->iv = v_my;
	hero_enemy->hpLimit = hp_enemy;
	hero_enemy->iv = v_enemy;
	
}

void Game::bing_shuaxin(float dt)
{
	TMXObjectGroup* group = tile_map->getObjectGroup("object");//获取对象层信息
	for (int i = 0; i < 3; i++)
	{
		auto spawnPoint = group->getObject("bing_enemy_jin");
		if(i==0)
			spawnPoint = group->getObject("bing_enemy_jin");
		if(i==1)
			spawnPoint = group->getObject("bing_enemy_yuan");
		if(i==2)
			spawnPoint = group->getObject("bing_enemy_pao");

		auto x = spawnPoint["x"].asFloat();
		auto y = spawnPoint["y"].asFloat();

		auto spawnPoint_runto = group->getObject("tower_my");
		auto x_runto = spawnPoint_runto["x"].asFloat();
		auto y_runto = spawnPoint_runto["y"].asFloat();
		bing_enemy = new BingEnemy();
		bing_enemy_tag++;
		bing_enemy->init_bing(x, y, x_runto, y_runto,bing_enemy_tag);
		bing_enemy_vector.pushBack(bing_enemy);
		
		this->addChild(bing_enemy, 3);
	}
	for (int i = 0; i < 3; i++)
	{
		auto spawnPoint = group->getObject("bing_my_jin");
		if (i == 0)
			spawnPoint = group->getObject("bing_my_jin");
		if (i == 1)
			spawnPoint = group->getObject("bing_my_yuan");
		if (i == 2)
			spawnPoint = group->getObject("bing_my_pao");

		auto x = spawnPoint["x"].asFloat();
		auto y = spawnPoint["y"].asFloat();

		auto spawnPoint_runto = group->getObject("tower_enemy");
		auto x_runto = spawnPoint_runto["x"].asFloat();
		auto y_runto = spawnPoint_runto["y"].asFloat();
		bing_my = new Bing();
		bing_my_tag++;
		bing_my->init_bing(x, y, x_runto, y_runto, bing_my_tag);
		bing_my_vector.pushBack(bing_my);
		this->addChild(bing_my, 3);
	}
}

/*void Game::tower_attack_my()
{
	Sprite* attacked;
	switch (tower_my->mubiao)
	{
	case 2:
		attacked = hero_enemy->get_sprite();
		break;
	case 4:
		attacked = bing_enemy->get_sprite();
		break;
	}
	Vec2 current_position = attacked->getPosition();
	Vec2 tower_position = tower_my->get_position();
	Vec2 diff = tower_position - current_position;//是触点和和hero之间的距离
	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));


	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(tower_position);
	addChild(skill);
	auto* animate = create_animate_skill(iv, juli, 0);
	auto* move_action = MoveTo::create(juli / iv, current_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);



	if (tower_my->mubiao == 2)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
		hero_enemy->hp_now -= 100;
	}
	if (tower_my->mubiao == 4)
	{
		bing_enemy->hp_now -= 100;
	}


	if (bing_enemy->hp_now <= 0)///如果attacked死亡
	{
		tower_my->is_attacking = false;

		bing_enemy->get_sprite()->setPosition(0, 0);
		removeChild(bing_enemy);
	}
	if (hero_enemy->hp_now <= 0)///如果attacked死亡
	{
		tower_my->is_attacking = false;

		hero_enemy->get_sprite()->setPosition(0, 0);
		removeChild(hero_enemy);
	}
	if (hero_enemy->hp_now == 0)
	{
		int kill_num;
		kill_num = std::stoi(zhanji_my->kill);
		kill_num++;
		std::stringstream ss;
		ss << kill_num;
		ss >> zhanji_my->kill;
		zhanji_my->zhanji->removeChildByTag(110);
		zhanji_my->get_zhanji(zhanji_my->kill, zhanji_my->death);

	}
	///////////////////////
	///////////添加伤害
	//////////////////////////

	////////////////添加死亡判断
	//if (1)///如果attacked死亡
	{
		//tower_enemy->is_attacking = false;
	}
}

void Game::tower_attack_enemy()
{
	Sprite* attacked;
	switch (tower_enemy->mubiao)
	{
	case 1:
		attacked = hero_my->get_sprite();

		break;
	case 3:
		attacked = bing_my->get_sprite();
		break;
	}
	Vec2 current_position = attacked->getPosition();
	Vec2 tower_position = tower_enemy->get_position();
	Vec2 diff = tower_position - current_position;//是触点和和hero之间的距离
	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));


	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(tower_position);
	addChild(skill);
	auto* animate = create_animate_skill(iv, juli, 0);
	auto* move_action = MoveTo::create(juli / iv, current_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);

	if (tower_enemy->mubiao == 1)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
		hero_my->hp_now -= 100;
	}
	if (tower_enemy->mubiao == 3)
	{
		bing_my->hp_now -= 100;
	}



	if (bing_my->hp_now <= 0)///如果attacked死亡
	{
		tower_enemy->is_attacking = false;

		bing_my->get_sprite()->setPosition(0, 0);
		removeChild(bing_my);
	}
	if (hero_my->hp_now <= 0)///如果attacked死亡
	{
		tower_enemy->is_attacking = false;

		hero_my->get_sprite()->setPosition(0.1, 0.1);
		removeChild(hero_my);

	}
	if (hero_my->hp_now == 0)
	{
		int death_num;
		death_num = std::stoi(zhanji_my->death);
		death_num++;
		std::stringstream ss;
		ss << death_num;
		ss >> zhanji_my->death;
		zhanji_my->zhanji->removeChildByTag(120);
		zhanji_my->get_zhanji(zhanji_my->kill, zhanji_my->death);
	}


	///////////////////////

	///////////添加属性

	//////////////////////////

	////////////////添加死亡判断
	//if (1)///如果attacked死亡

}

void Game::bing_attack_my()
{
	Sprite* attacked;
	Rect rect_attacked;
	switch (bing_my->mubiao)
	{
	case 6:
		attacked = tower_enemy->get_sprite();
		rect_attacked = tower_enemy->get_attacked_rect();
		break;
	case 4:
		attacked = bing_enemy->get_sprite();
		rect_attacked = bing_enemy->get_attacked_rect();
		break;
	case 2:
		attacked = hero_enemy->get_sprite();
		rect_attacked = hero_enemy->get_attacked_rect();
		break;
	}
	Vec2 attacked_position = attacked->getPosition();
	Vec2 bing_position = bing_my->get_position();
	Vec2 diff = attacked_position - bing_position;//是触点和和hero之间的距离

	//bing_enemy->run(diff);

	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	if (rect_attacked.containsPoint(bing_my->get_position()))//如果进入了对方受攻击范围,停止
	{
		bing_my->get_sprite()->stopAllActions();
		auto* stop = bing_my->animate_walk(bing_my->get_direction(diff.x, diff.y), "stop", 0);
		bing_my->get_sprite()->runAction(stop);
	}
	else
	{
		bing_my->get_sprite()->stopAllActions();
		bing_my->run(attacked_position);
	}

	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(bing_position);
	addChild(skill);
	auto* animate = create_animate_skill(iv, juli, 0);
	auto* move_action = MoveTo::create(juli / iv, attacked_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);

	if (bing_my->mubiao == 2)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
		hero_enemy->hp_now -= 100;
	}
	if (bing_my->mubiao == 4)
	{
		bing_enemy->hp_now -= 100;
	}

	if (bing_my->mubiao == 6)
	{
		tower_enemy->hp_now -= 100;
	}
	//////////////////////
	////////////////添加死亡判断
	if (bing_enemy->hp_now <= 0)///如果attacked死亡
	{
		bing_my->is_attacking = false;

		bing_enemy->get_sprite()->setPosition(0, 0);
		removeChild(bing_enemy);
	}
	if (tower_enemy->hp_now <= 0)///如果attacked死亡
	{
		bing_my->is_attacking = false;

		tower_enemy->get_sprite()->setPosition(0, 0);
		removeChild(tower_enemy);
	}
	if (hero_enemy->hp_now <= 0)///如果attacked死亡
	{
		bing_my->is_attacking = false;

		hero_enemy->get_sprite()->setPosition(0, 0);
		removeChild(hero_enemy);
	}
	if (jidi_enemy->hp_now <= 0)///如果attacked死亡
	{
		bing_my->is_attacking = false;

		jidi_enemy->get_sprite()->setPosition(0, 0);
		removeChild(jidi_enemy);
	}
}

void Game::bing_attack_enemy()
{
	Sprite* attacked;
	Rect rect_attacked;
	switch (bing_enemy->mubiao)
	{
	case 5:
		attacked = tower_my->get_sprite();
		rect_attacked = tower_my->get_attacked_rect();
		break;
	case 3:
		attacked = bing_my->get_sprite();
		rect_attacked = bing_my->get_attacked_rect();
		break;
	case 1:
		attacked = hero_my->get_sprite();
		rect_attacked = hero_my->get_attacked_rect();
		break;
	}
	Vec2 attacked_position = attacked->getPosition();
	Vec2 bing_position = bing_enemy->get_position();
	Vec2 diff = attacked_position - bing_position;//是触点和和hero之间的距离

	//bing_enemy->run(diff);

	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	if (rect_attacked.containsPoint(bing_enemy->get_position()))//如果进入了对方受攻击范围,停止
	{
		bing_enemy->get_sprite()->stopAllActions();
		auto* stop = bing_enemy->animate_walk(bing_enemy->get_direction(diff.x, diff.y), "stop", 0);
		bing_enemy->get_sprite()->runAction(stop);

	}
	else
	{
		bing_enemy->get_sprite()->stopAllActions();
		bing_enemy->run(attacked_position);
	}

	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(bing_position);
	addChild(skill);
	auto* animate = create_animate_skill(iv, juli, 0);
	auto* move_action = MoveTo::create(juli / iv, attacked_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);

	if (bing_enemy->mubiao == 1)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
		hero_my->hp_now -= 100;
	}
	if (bing_enemy->mubiao == 3)
	{
		bing_my->hp_now -= 100;
	}

	if (bing_enemy->mubiao == 5)
	{
		tower_my->hp_now -= 100;
	}


	if (bing_my->hp_now <= 0)///如果attacked死亡
	{
		bing_enemy->is_attacking = false;

		bing_my->get_sprite()->setPosition(0, 0);
		removeChild(bing_my);
	}
	if (tower_my->hp_now <= 0)///如果attacked死亡
	{
		bing_enemy->is_attacking = false;

		tower_my->get_sprite()->setPosition(0, 0);
		removeChild(tower_my);
	}
	if (hero_my->hp_now <= 0)///如果attacked死亡
	{
		bing_enemy->is_attacking = false;

		hero_my->get_sprite()->setPosition(0.1, 0.1);
		removeChild(hero_my);
	}
	if (jidi_my->hp_now <= 0)///如果attacked死亡
	{
		bing_enemy->is_attacking = false;

		jidi_my->get_sprite()->setPosition(0, 0);
		removeChild(jidi_my);
	}
	/////////////////////////////
	////////////////添加伤害
	////////////////////////////

	////////////////添加死亡判断
	//if (1)///如果attacked死亡
	{
		//bing_enemy->is_attacking = false;
	}
}
void Game::heroAi_attack()
{
	Sprite* attacked;
	Rect rect_attacked;
	switch (hero_enemy->mubiao)
	{
	case 1:
		attacked = hero_my->get_sprite();
		rect_attacked = hero_my->get_attacked_rect();
		break;
	case 3:
		attacked = bing_my->get_sprite();
		rect_attacked = bing_my->get_attacked_rect();
		break;
	case 5:
		attacked = tower_my->get_sprite();
		rect_attacked = tower_my->get_attacked_rect();
		break;

	}
	if (hero_enemy->mubiao == 1)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
		hero_my->hp_now -= 100;
	}
	if (hero_enemy->mubiao == 3)
	{
		bing_my->hp_now -= 100;
	}

	if (hero_enemy->mubiao == 5)
	{
		tower_my->hp_now -= 100;
	}
	Vec2 attacked_position = attacked->getPosition();
	Vec2 bing_position = hero_enemy->get_position();
	Vec2 diff = attacked_position - bing_position;//是触点和和hero之间的距离

	//bing_enemy->run(diff);

	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	if (rect_attacked.containsPoint(hero_enemy->get_position()))//如果进入了对方受攻击范围,停止
	{
		hero_enemy->get_sprite()->stopAllActions();
		auto* stop = hero_enemy->animate_walk(hero_enemy->get_direction(diff.x, diff.y), "stop", 0);
		hero_enemy->get_sprite()->runAction(stop);
	}
	else
	{
		hero_enemy->get_sprite()->stopAllActions();
		hero_enemy->run(attacked_position);
	}

	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(bing_position);
	addChild(skill);
	auto* animate = create_animate_skill(iv, juli, 0);
	auto* move_action = MoveTo::create(juli / iv, attacked_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);

	if (hero_enemy->mubiao == 1)/////////只有英雄有被攻击特效
	{
		auto animate_attacked = create_animate_attacked();
		attacked->runAction(animate_attacked);
	}


	if (bing_my->hp_now <= 0)///如果attacked死亡
	{
		hero_enemy->is_attacking = false;

		bing_my->get_sprite()->setPosition(0, 0);
		removeChild(bing_my);
	}
	if (tower_my->hp_now <= 0)///如果attacked死亡
	{
		hero_enemy->is_attacking = false;
		//bing_enemy->is_attacking = false;

		tower_my->get_sprite()->setPosition(0, 0);
		removeChild(tower_my);
		//unschedule(schedule_selector(Game::Dead_check_bing_enemy));
	}
	if (hero_my->hp_now <= 0)///如果attacked死亡
	{
		hero_enemy->is_attacking = false;
		//bing_enemy->is_attacking = false;

		hero_my->get_sprite()->setPosition(0.1, 0.1);
		removeChild(hero_my);
		//unschedule(schedule_selector(Game::Dead_check_bing_enemy));
	}
	if (jidi_my->hp_now <= 0)///如果attacked死亡
	{
		hero_enemy->is_attacking = false;
		//bing_enemy->is_attacking = false;

		jidi_my->get_sprite()->setPosition(0, 0);
		removeChild(jidi_my);
		//unschedule(schedule_selector(Game::Dead_check_bing_enemy));
	}
	/////////////////////////////
	////////////////添加伤害
	////////////////////////////

	////////////////添加死亡判断

	///////////////////在这里添加其他判断优化heroai，比如扣血多少就往回走

	//if (1)///如果attacked死亡
	{
		//bing_enemy->is_attacking = false;
	}
}*/

bool Game:: rect_contain_bing_enemy(Bing*it_attack)//查看对方兵是否在攻击范围内
{
	
	for (auto it_attacked = bing_enemy_vector.begin();
		it_attacked != bing_enemy_vector.end(); it_attacked++)
	{
		/*Rect rect = Rect(it_attack->bing->getPositionX()-100, 
			it_attack->bing->getPositionY() - 100,200,200);
		Vec2 position = (*it_attacked)->bing->getPosition();*/
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{


					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));

					//if (bing_enemy_vector.end() - bing_enemy_vector.begin() != 1)
						//it_attacked = bing_enemy_vector.erase(it_attacked);
					//it_attacked = bing_enemy_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);

				}
			}
			return true;
		}
	}
	return true;
}

bool Game::rect_contain_bing_my(BingEnemy*it_attack)//查看我是否在对方攻击范围内
{
	for (auto it_attacked = bing_my_vector.begin();
		it_attacked != bing_my_vector.end(); it_attacked++)
	{
		/*Rect rect = Rect(it_attack->bing->getPositionX() - 100,
			it_attack->bing->getPositionY() - 100, 200, 200);
		Vec2 position = (*it_attacked)->bing->getPosition();*/
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{
					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));
					//it_attacked = bing_my_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);
				}
			}
			return true;
		}
	}
	return false;
}

void Game::rect_check_tower(float dt)//判定塔要攻击谁
{
	///////////////////敌方防御塔的判定
	if (tower_enemy->mubiao_sprite != NULL)
	{
		if (tower_enemy->get_attack_rect().containsPoint(tower_enemy->mubiao_sprite->getPosition()) == false)
			//如果目标超出攻击范围
		{
			(tower_enemy)->get_sprite()->stopAllActions();
			tower_enemy->is_attacking = false;
		}
	}
	if (tower_my->mubiao_sprite != NULL)
	{
		if (tower_my->get_attack_rect().containsPoint((tower_my)->mubiao_sprite->getPosition()) == false)
			//如果目标超出攻击范围
		{
			(tower_my)->get_sprite()->stopAllActions();
			tower_my->is_attacking = false;
		}
	}

	////////////注意顺序不能改变，顺序代表着攻击优先级
	//if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if (tower_enemy->rect_attack.containsPoint((*it)->get_position()))//小兵为第一优先级
			{
				if (tower_enemy->is_attacking == false)//只在这一次改变目标
				{
					tower_enemy->mubiao = 3;
					tower_enemy->mubiao_sprite = (*it)->get_sprite();
					tower_enemy->is_attacking = true;
				}
				if (tower_enemy->mubiao_sprite == (*it)->get_sprite())
				{
					tower_enemy->attack((*it)->get_sprite());
					(*it)->hp_now -= 100;
					if ((*it)->hp_now <= 0)///如果attacked死亡
					{
						int itag = (*it)->tag;
						char  file_name[50] = { 0 };
						if (itag % 3 == 0)
							sprintf(file_name, "paobing/stand/stand_%d.png", (*it)->direction);
						else if ((itag + 1) % 3 == 0)
							sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it)->direction);
						else
							sprintf(file_name, "jinbing/stand/stand_%d.png", (*it)->direction);
						Sprite* dead_man = Sprite::create(file_name);
						Animate* animate = animate_dead(itag, (*it)->direction);
						dead_man->setPosition((*it)->get_position());
						addChild(dead_man);

						tower_my->is_attacking = false;
						(*it)->get_sprite()->setPosition(0, 0);
						removeChild((*it));
						//it = bing_my_vector.erase(it);

						auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
						auto* sequence = Sequence::create(animate, xiaoshi, NULL);
						dead_man->runAction(sequence);

					}
				}
			}
			else if (tower_enemy->rect_attack.containsPoint(hero_my->get_position()))//将英雄放在第一个
			{
				if (tower_enemy->is_attacking == false)//只在这一次改变目标
				{
					tower_enemy->mubiao = 1;
					tower_enemy->mubiao_sprite = hero_my->get_sprite();
					tower_enemy->is_attacking = true;
				}
				if (tower_enemy->mubiao_sprite == hero_my->get_sprite())
				{
					tower_enemy->attack(hero_my->get_sprite());
					hero_my->hp_now -= 100;
					if (hero_my->hp_now <= 0)///如果attacked死亡
					{
						tower_enemy->is_attacking = false;
						hero_my->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_my);
					}
				}
			}
		}
	}
	//////////////在后面添加己方防御塔判断
	//if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
				if (tower_my->rect_attack.containsPoint((*it)->get_position()))//小兵为第一优先级
				{
					if (tower_my->is_attacking == false)//只在这一次改变目标
					{
						tower_my->mubiao = 4;
						tower_my->mubiao_sprite = (*it)->get_sprite();
						tower_my->is_attacking = true;
					}
					if (tower_my->mubiao_sprite == (*it)->get_sprite())
					{
						tower_my->attack((*it)->get_sprite());
						(*it)->hp_now -= 100;

						int itag = (*it)->tag;
						int idirection = (*it)->direction;
						if ((*it)->hp_now <= 0)///如果attacked死亡
						{
							int itag = (*it)->tag;
							char  file_name[50] = { 0 };
							if (itag % 3 == 0)
								sprintf(file_name, "paobing/stand/stand_%d.png", (*it)->direction);
							else if ((itag + 1) % 3 == 0)
								sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it)->direction);
							else
								sprintf(file_name, "jinbing/stand/stand_%d.png", (*it)->direction);
							Sprite* dead_man = Sprite::create(file_name);
							Animate* animate = animate_dead(itag, (*it)->direction);
							dead_man->setPosition((*it)->get_position());
							addChild(dead_man);

							tower_my->is_attacking = false;
							(*it)->get_sprite()->setPosition(0, 0);
							removeChild((*it));
							//it = bing_enemy_vector.erase(it);

							auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
							auto* sequence = Sequence::create(animate, xiaoshi, NULL);
							dead_man->runAction(sequence);


						}
					}

				}
				else if (tower_my->rect_attack.containsPoint(hero_enemy->get_position()))//将英雄放在第一个
				{
					if (tower_my->is_attacking == false)//只在这一次改变目标
					{
						tower_my->mubiao_sprite = hero_enemy->get_sprite();
						tower_my->is_attacking = true;
					}
					if (tower_my->mubiao_sprite == hero_enemy->get_sprite())
					{
						tower_my->attack(hero_enemy->get_sprite());
						hero_enemy->hp_now -= 100;
						if (hero_enemy->hp_now <= 0)///如果attacked死亡
						{
							tower_my->is_attacking = false;
							hero_enemy->get_sprite()->setPosition(0, 0);
							removeChild(hero_enemy);

						}
					}
				}
			}
	}

}

void Game::rect_check_bing(float dt)
{
	///////////////////敌方小兵的判定
	if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
			if ((*it)->is_attacking == false ||
				((*it)->get_attack_rect().containsPoint
				((*it)->mubiao_sprite->getPosition())) == false)
				//如果没有在攻击 或者 目标超出攻击范围
			{
				(*it)->get_sprite()->stopAllActions();
				(*it)->is_attacking = false;//一定要加，因为还有超出范围的判定
				(*it)->auto_runto();
			}
		}
	}
	if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if ((*it)->is_attacking == false ||
				((*it)->get_attack_rect().containsPoint
				((*it)->mubiao_sprite->getPosition())) == false)
				//如果没有在攻击 或者 目标超出攻击范围
			{
				(*it)->get_sprite()->stopAllActions();
				(*it)->is_attacking = false;//一定要加，因为还有超出范围的判定
				(*it)->auto_runto();
			}
		}
	}
	////////////////////敌方小兵
	/////注意：顺序代表着攻击优先级
	if (!bing_enemy_vector.empty())
	{
		for (auto it = bing_enemy_vector.begin(); it != bing_enemy_vector.end(); it++)
		{
			if ((*it)->get_attack_rect().containsPoint(tower_my->get_position()))//将小兵放在第一个
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 5;
					(*it)->mubiao_sprite = tower_my->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == tower_my->get_sprite())
				{
					(*it)->attack(tower_my->get_sprite(), tower_my->get_attacked_rect());
					tower_my->hp_now -= 100;
					if (tower_my->hp_now <= 0)///如果attacked死亡
					{

						(*it)->is_attacking = false;
						tower_my->get_sprite()->setPosition(0, 0);
						removeChild(tower_my);
					}
				}

			}
			else if (rect_contain_bing_my(*it))
			{
	
			}
			else if ((*it)->get_attack_rect().containsPoint(hero_my->get_position()))
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 1;
					(*it)->mubiao_sprite = hero_my->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == hero_my->get_sprite())
				{
					(*it)->attack(hero_my->get_sprite(), hero_my->get_attacked_rect());
					hero_my->hp_now -= 100;
					if (hero_my->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						hero_my->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_my);
					}
				}
			}
		}
	}
	/////////////后面添加我方小兵判定
	if (!bing_my_vector.empty())
	{
		for (auto it = bing_my_vector.begin(); it != bing_my_vector.end(); it++)
		{
			if ((*it)->get_attack_rect().containsPoint(tower_enemy->get_position()))//将小兵放在第一个
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 6;
					(*it)->mubiao_sprite = tower_enemy->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == tower_enemy->get_sprite())
				{
					(*it)->attack(tower_enemy->get_sprite(), tower_enemy->get_attacked_rect());
					tower_enemy->hp_now -= 100;
					if (tower_enemy->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						tower_enemy->get_sprite()->setPosition(0, 0);
						removeChild(tower_enemy);
					}
				}

			}
			else if (rect_contain_bing_enemy(*it))
			{
				
			}
			else if ((*it)->get_attack_rect().containsPoint(hero_enemy->get_position()))
			{
				if ((*it)->is_attacking == false)//只在这一次改变目标
				{
					(*it)->mubiao = 1;
					(*it)->mubiao_sprite = hero_enemy->get_sprite();
					(*it)->is_attacking = true;
				}
				if ((*it)->mubiao_sprite == hero_enemy->get_sprite())
				{
					(*it)->attack(hero_enemy->get_sprite(), hero_enemy->get_attacked_rect());
					hero_enemy->hp_now -= 100;
					if (hero_enemy->hp_now <= 0)///如果attacked死亡
					{
						(*it)->is_attacking = false;
						hero_enemy->get_sprite()->setPosition(0.1, 0.1);
						removeChild(hero_enemy);
					}
				}
			}
		}
	}
}

bool Game::Hero_ai_attack_bing(HeroAi*it_attack)
{
	for (auto it_attacked = bing_my_vector.begin();
		it_attacked != bing_my_vector.end(); it_attacked++)
	{
		if (it_attack->get_attack_rect().containsPoint((*it_attacked)->get_position()))
		{
			if (it_attack->is_attacking == false)//只在这一次改变目标
			{
				it_attack->mubiao = 4;
				it_attack->mubiao_sprite = (*it_attacked)->get_sprite();
				it_attack->is_attacking = true;
			}
			if (it_attack->mubiao_sprite == (*it_attacked)->get_sprite())
			{
				it_attack->attack((*it_attacked)->get_sprite(), (*it_attacked)->get_attacked_rect());
				(*it_attacked)->hp_now -= 100;
				if ((*it_attacked)->hp_now <= 0)///如果attacked死亡
				{
					int itag = (*it_attacked)->tag;
					char  file_name[50] = { 0 };
					if (itag % 3 == 0)
						sprintf(file_name, "paobing/stand/stand_%d.png", (*it_attacked)->direction);
					else if ((itag + 1) % 3 == 0)
						sprintf(file_name, "yuanbing/stand/stand_%d.png", (*it_attacked)->direction);
					else
						sprintf(file_name, "jinbing/stand/stand_%d.png", (*it_attacked)->direction);
					Sprite* dead_man = Sprite::create(file_name);
					Animate* animate = animate_dead(itag, (*it_attacked)->direction);
					dead_man->setPosition((*it_attacked)->get_position());
					addChild(dead_man);


					it_attack->is_attacking = false;
					(*it_attacked)->get_sprite()->setPosition(0, 0);
					removeChild((*it_attacked));
					//it_attacked = bing_my_vector.erase(it_attacked);

					auto* xiaoshi = CallFunc::create([dead_man, this]() {dead_man->removeFromParent(); });
					auto* sequence = Sequence::create(animate, xiaoshi, NULL);
					dead_man->runAction(sequence);
				}
			}
		}
			return true;
	}
	return false;
}

void Game::rect_check_heroAi(float dt)
{
	///////////////////敌方ai的判定
	if (hero_enemy->is_attacking == false ||
		(hero_enemy->get_attack_rect().containsPoint
		(hero_enemy->mubiao_sprite->getPosition())) == false)
		//如果没有在攻击 或者 目标超出攻击范围
	{
		hero_enemy->get_sprite()->stopAllActions();
		hero_enemy->is_attacking = false;//一定要加，因为还有超出范围的判定
		hero_enemy->auto_runto();
	}
	////////////////////敌方hero_ai

	/////注意：顺序代表着攻击优先级
	if (hero_enemy->get_attack_rect().containsPoint(hero_my->get_position()))
	{
		if (hero_enemy->is_attacking == false)//只在这一次改变目标
		{
			hero_enemy->mubiao = 1;
			hero_enemy->mubiao_sprite = hero_my->get_sprite();
			hero_enemy->is_attacking = true;
		}
		if (hero_enemy->mubiao_sprite == hero_my->get_sprite())
		{
			hero_enemy->attack(hero_my->get_sprite(), hero_my->get_attacked_rect());
			hero_my->hp_now -= 100;
			if (hero_my->hp_now <= 0)///如果attacked死亡
			{
				hero_enemy->is_attacking = false;
				hero_my->get_sprite()->setPosition(0.1, 0.1);
				removeChild(hero_my);
			}
		}

	}
	else if (Hero_ai_attack_bing(hero_enemy))
	{

	}
	else if (hero_enemy->get_attack_rect().containsPoint(tower_my->get_position()))//将小兵放在第一个
	{
		if (hero_enemy->is_attacking == false)//只在这一次改变目标
		{
			hero_enemy->mubiao = 5;
			hero_enemy->mubiao_sprite = tower_my->get_sprite();
			hero_enemy->is_attacking = true;
		}
		if (hero_enemy->mubiao_sprite == tower_my->get_sprite())
		{
			hero_enemy->attack(tower_my->get_sprite(), tower_my->get_attacked_rect());
			tower_my->hp_now -= 100;
			if (tower_my->hp_now <= 0)///如果attacked死亡
			{
				hero_enemy->is_attacking = false;
				tower_my->get_sprite()->setPosition(0, 0);
				removeChild(tower_my);
			}
		}
	}

}

void Game::hurt_check(float dt)
{
	if (hero_my->if_check_hurt == true)
	{
		hero_my->hurt_check();
	}
	if (hero_enemy->if_check_hurt == true)
	{
		hero_enemy->hurt_check();
	}
	if (bing_my->if_check_hurt == true)
	{
		bing_my->hurt_check();
	}
	if (bing_enemy->if_check_hurt == true)
	{
		bing_enemy->hurt_check();
	}
	if (tower_my->if_check_hurt == true)
	{
		tower_my->hurt_check();
	}
	if (tower_enemy->if_check_hurt == true)
	{
		tower_enemy->hurt_check();
	}

}




Animate* Game::animate_dead(int tag,int direction)
{
	Animation* animation = Animation::create();

	char  file_name[50] = { 0 };
	if (tag % 3 == 0)
	{
		for (int i = 0; i < 7; i++)
		{
			sprintf(file_name, "paobing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else if ((tag + 1) % 3 == 0)
	{
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "yuanbing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else
	{
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "jinbing/dead/dead_%d_%d.png", direction, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}

	animation->setDelayPerUnit(0.1f);
	animation->setLoops(1);
	Animate* animate = Animate::create(animation);
	return animate;
}







void Game::get_coin_by_killing_bing(float dt)
{
	int coin_num;
	coin_num = std::stoi(coin_my->number);
	if (bing_enemy->hp_now <= 0)
	{
		coin_num += 50;
		unschedule(schedule_selector(Game::get_coin_by_killing_bing));
	}
	std::stringstream ss;
	ss << coin_num;
	ss >> coin_my->number;
	coin_my->coin->removeChildByTag(110);
	coin_my->get_coin(coin_my->number);
}

void Game::get_coin_by_killing_tower(float dt)
{

	int coin_num;
	coin_num = std::stoi(coin_my->number);

	if (tower_enemy->hp_now <= 0)
	{
		coin_num += 100;
		unschedule(schedule_selector(Game::get_coin_by_killing_tower));
	}
	std::stringstream ss;
	ss << coin_num;
	ss >> coin_my->number;
	coin_my->coin->removeChildByTag(110);
	coin_my->get_coin(coin_my->number);
}

void Game::get_coin_by_killing_hero(float dt)
{
	int coin_num;
	coin_num = std::stoi(coin_my->number);

	if (hero_enemy->hp_now <= 0)
	{
		coin_num += 150;
		unschedule(schedule_selector(Game::get_coin_by_killing_hero));
	}
	std::stringstream ss;
	ss << coin_num;
	ss >> coin_my->number;
	coin_my->coin->removeChildByTag(110);
	coin_my->get_coin(coin_my->number);
}


void Game::get_coin_by_time(float dt)
{
	int coin_num;
	coin_num = std::stoi(coin_my->number);
	coin_num += 20;
	if (bing_enemy->hp_now == 0)
	{
		coin_num += 50;
	}
	if (tower_enemy->hp_now == 0)
	{
		coin_num += 100;
	}
	if (hero_enemy->hp_now == 0)
	{
		coin_num += 150;
	}
	std::stringstream ss;
	ss << coin_num;
	ss >> coin_my->number;
	coin_my->coin->removeChildByTag(110);
	coin_my->get_coin(coin_my->number);
}

void Game::EXP_change(float dt)
{
	hero_my->EXP_now += 100;
}

void Game::EXP_check(float dt)
{

	auto progress = (ProgressTimer*)hero_my->hero->getChildByTag(210);
	progress->setPercentage((hero_my->EXP_now / hero_my->EXPLimit) * 100); //

	if (progress->getPercentage() >= 100)
	{

		hero_my->EXP_now -= hero_my->EXPLimit;

	}
}

void Game::menuShopItemOpenCallback(cocos2d::Ref* pSender)
{
	//创建商店图标
	auto visibleSize = Director::getInstance()->getVisibleSize();

	Layer* shoplayer = Layer::create();
	auto shopCloseButton = MenuItemImage::create("close_button1.png", "close_button2.png",
		CC_CALLBACK_1(Game::menuShopItemCloseCallback, this));                //商店按钮
	shopCloseButton->setAnchorPoint(Vec2::ZERO);
	shopCloseButton->setPosition(500, 395);
	shopCloseButton->setScaleX(0.02f);
	shopCloseButton->setScaleY(0.02f);

	//itemdao
	auto shopItem = Sprite::create("shopItem.png");
	shopItem->setAnchorPoint(Vec2::ZERO);
	shopItem->setPosition(50, 25);
	shopItem->setScaleX(3.5f);
	shopItem->setScaleY(2.4f);
	shoplayer->addChild(shopItem, 9);
	auto itemDao1 = MenuItemImage::create("/equip/dao_lan.png", "/equip/dao_lan.png", CC_CALLBACK_1(Game::buyDaoCallBack1, this));
	itemDao1->setAnchorPoint(Vec2::ZERO);
	itemDao1->setPosition(Vec2(343.5, 294.5));
	itemDao1->setScaleX(0.51f);
	itemDao1->setScaleY(0.5f);
	auto itemDao2 = MenuItemImage::create("/equip/dao_long.jpg", "/equip/dao_long.jpg", CC_CALLBACK_1(Game::buyDaoCallBack2, this));
	itemDao2->setAnchorPoint(Vec2::ZERO);
	itemDao2->setPosition(Vec2(381, 294.5));
	itemDao2->setScaleX(0.55f);
	itemDao2->setScaleY(0.56f);
	auto itemDao3 = MenuItemImage::create("/equip/dao_lv.jpg", "/equip/dao_lv.jpg", CC_CALLBACK_1(Game::buyDaoCallBack3, this));
	itemDao3->setAnchorPoint(Vec2::ZERO);
	itemDao3->setAnchorPoint(Vec2::ZERO);
	itemDao3->setPosition(Vec2(418.5, 294.5));
	itemDao3->setScaleX(0.55f);
	itemDao3->setScaleY(0.56f);
	////////////////////////dun
	auto itemDun1 = MenuItemImage::create("/equip/dun_mu.png", "/equip/dun_mu.png", CC_CALLBACK_1(Game::buyDunCallBack1, this));
	itemDun1->setAnchorPoint(Vec2::ZERO);
	itemDun1->setPosition(Vec2(343.5, 256.5));
	itemDun1->setScaleX(0.51f);
	itemDun1->setScaleY(0.5f);
	auto itemDun2 = MenuItemImage::create("/equip/dun_tie.png", "/equip/dun_tie.png", CC_CALLBACK_1(Game::buyDunCallBack2, this));
	itemDun2->setAnchorPoint(Vec2::ZERO);
	itemDun2->setPosition(Vec2(381, 256.5));
	itemDun2->setScaleX(0.51f);
	itemDun2->setScaleY(0.5f);


	/////////////////////////gongjian
	auto itemGong1 = MenuItemImage::create("/equip/gong_huang.jpg", "/equip/gong_huang.jpg", CC_CALLBACK_1(Game::buyGongCallBack1, this));
	itemGong1->setAnchorPoint(Vec2::ZERO);
	itemGong1->setPosition(Vec2(418.5, 256.5));
	itemGong1->setScaleX(0.55f);
	itemGong1->setScaleY(0.56f);
	auto itemGong2 = MenuItemImage::create("/equip/gong_lv.png", "/equip/gong_lv.png", CC_CALLBACK_1(Game::buyGongCallBack2, this));
	itemGong2->setAnchorPoint(Vec2::ZERO);
	itemGong2->setPosition(Vec2(456, 256.5));
	itemGong2->setScaleX(0.51f);
	itemGong2->setScaleY(0.5f);


	///////////////////////////kaijia
	auto itemKai1 = MenuItemImage::create("/equip/Kai_bin.png", "/equip/Kai_bin.png", CC_CALLBACK_1(Game::buyKaiCallBack1, this));
	itemKai1->setAnchorPoint(Vec2::ZERO);
	itemKai1->setPosition(Vec2(343.5, 218.5));
	itemKai1->setScaleX(0.51f);
	itemKai1->setScaleY(0.5f);
	auto itemKai2 = MenuItemImage::create("/equip/Kai_feng.jpg", "/equip/Kai_feng.png", CC_CALLBACK_1(Game::buyKaiCallBack2, this));
	itemKai2->setAnchorPoint(Vec2::ZERO);
	itemKai2->setPosition(Vec2(381, 218.5));
	itemKai2->setScaleX(0.55f);
	itemKai2->setScaleY(0.56f);
	auto itemKai3 = MenuItemImage::create("/equip/Kai_fuhuo.png", "/equip/Kai_fuhuo.png", CC_CALLBACK_1(Game::buyKaiCallBack3, this));
	itemKai3->setAnchorPoint(Vec2::ZERO);
	itemKai3->setPosition(Vec2(418.5, 218.5));
	itemKai3->setScaleX(0.51f);
	itemKai3->setScaleY(0.5f);
	auto itemKai4 = MenuItemImage::create("/equip/Kai_lan.png", "/equip/Kai_lan.png", CC_CALLBACK_1(Game::buyKaiCallBack4, this));
	itemKai4->setAnchorPoint(Vec2::ZERO);
	itemKai4->setPosition(Vec2(456, 218.5));
	itemKai4->setScaleX(0.51f);
	itemKai4->setScaleY(0.5f);
	auto itemKai5 = MenuItemImage::create("/equip/Kai_bu.png", "/equip/Kai_bu.png", CC_CALLBACK_1(Game::buyKaiCallBack5, this));
	itemKai5->setAnchorPoint(Vec2::ZERO);
	itemKai5->setPosition(Vec2(456, 294.5));
	itemKai5->setScaleX(0.51f);
	itemKai5->setScaleY(0.5f);

	/////////////////oe
	auto itemShoe1 = MenuItemImage::create("/equip/Shoe_hui.jpg", "/equip/Shoe_hui.jpg", CC_CALLBACK_1(Game::buyShoeCallBack1, this));
	itemShoe1->setAnchorPoint(Vec2::ZERO);
	itemShoe1->setPosition(Vec2(343.5, 180.5));
	itemShoe1->setScaleX(0.51f);
	itemShoe1->setScaleY(0.5f);
	auto itemShoe2 = MenuItemImage::create("/equip/Shoe_lan.jpg", "/equip/Shoe_lan.jpg", CC_CALLBACK_1(Game::buyShoeCallBack2, this));
	itemShoe2->setAnchorPoint(Vec2::ZERO);
	itemShoe2->setPosition(Vec2(381, 180.5));
	itemShoe2->setScaleX(0.55f);
	itemShoe2->setScaleY(0.56f);
	auto itemShoe3 = MenuItemImage::create("/equip/Shoe_lv.jpg", "/equip/Shoe_lv.jpg", CC_CALLBACK_1(Game::buyShoeCallBack3, this));
	itemShoe3->setAnchorPoint(Vec2::ZERO);
	itemShoe3->setPosition(Vec2(418.5, 180.5));
	itemShoe3->setScaleX(0.55f);
	itemShoe3->setScaleY(0.56f);
	/////////////////fa
	auto itemFa1 = MenuItemImage::create("/equip/fashu.png", "/equip/fashu.png", CC_CALLBACK_1(Game::buyFaCallBack1, this));
	itemFa1->setAnchorPoint(Vec2::ZERO);
	itemFa1->setPosition(Vec2(456, 180.5));
	itemFa1->setScaleX(0.51f);
	itemFa1->setScaleY(0.5f);
	auto itemFa2 = MenuItemImage::create("/equip/fashu_feng.jpg", "/equip/fashu_feng.jpg", CC_CALLBACK_1(Game::buyFaCallBack2, this));
	itemFa2->setAnchorPoint(Vec2::ZERO);
	itemFa2->setPosition(Vec2(343.5, 142.5));
	itemFa2->setScaleX(0.55f);
	itemFa2->setScaleY(0.56f);

	auto menu = Menu::create(shopCloseButton, itemDao1, itemDao2, itemDao3, itemDun1, itemDun2, itemGong1, itemGong2, itemKai1, itemKai2, itemKai3, itemKai4,
		itemKai5, itemShoe1, itemShoe2, itemShoe3, itemFa1, itemFa2, NULL);
	menu->setPosition(Vec2::ZERO);
	shopItem->addChild(menu, 1);


	this->addChild(shoplayer, 8, 911);
}

void Game::menuShopItemCloseCallback(cocos2d::Ref* pSender)
{
	this->getChildByTag(911)->removeFromParentAndCleanup(true);
}

void Game::buyDaoCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyDaoCallBack2(cocos2d::Ref* pSender)
{
}

void Game::buyDaoCallBack3(cocos2d::Ref* pSender)
{
}

void Game::buyDunCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyDunCallBack2(cocos2d::Ref* pSender)
{
}

void Game::buyGongCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyGongCallBack2(cocos2d::Ref* pSender)
{
}

void Game::buyKaiCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyKaiCallBack2(cocos2d::Ref* pSender)
{
}

void Game::buyKaiCallBack3(cocos2d::Ref* pSender)
{
}

void Game::buyKaiCallBack4(cocos2d::Ref* pSender)
{
}

void Game::buyKaiCallBack5(cocos2d::Ref* pSender)
{
}

void Game::buyShoeCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyShoeCallBack2(cocos2d::Ref* pSender)
{
}

void Game::buyShoeCallBack3(cocos2d::Ref* pSender)
{
}

void Game::buyFaCallBack1(cocos2d::Ref* pSender)
{
}

void Game::buyFaCallBack2(cocos2d::Ref* pSender)
{
}

/*void Game::levelchange(std::string level)
{
	Label *label1 = Label::createWithTTF(level, "fonts/Marker Felt.ttf", 30);
	label1->setPosition(Vec2(100, 50));
	addChild(label1, 1, 120);
}*/



//������ȴ
void Game::get_skill0()
{
	Sprite *pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2( 100*12+150, 60));
	this->addChild(pcdbeiSp);
	Sprite *pscdqianSp = Sprite::create("skill/skill_0.png");
	ProgressTimer *pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100*12+150,  60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 200); //tag����Ϊ200

}
void Game::get_skill1()
{
	Sprite *pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 13 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite *pscdqianSp = Sprite::create("skill/skill_1.png");
	ProgressTimer *pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 13 + 150,  60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 210); //tag����Ϊ210

}
void Game::get_skill2()
{
	Sprite *pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 14 + 150, 60));
	this->addChild(pcdbeiSp);
	Sprite *pscdqianSp = Sprite::create("skill/skill_2.png");
	ProgressTimer *pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 14 + 150,  60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 220); //tag����Ϊ220

}
void Game::get_skill3()
{
	Sprite *pcdbeiSp = Sprite::create("skill/skill_0.png");
	pcdbeiSp->setPosition(Vec2(100 * 15+ 150, 60));
	this->addChild(pcdbeiSp);
	Sprite *pscdqianSp = Sprite::create("skill/skill_3.png");
	ProgressTimer *pcdProGress = ProgressTimer::create(pscdqianSp);
	pcdProGress->setType(kCCProgressTimerTypeBar);
	pcdProGress->setBarChangeRate(Vec2(1, 0));
	pcdProGress->setMidpoint(Vec2(0, 0.5));
	pcdProGress->setPosition(Vec2(100 * 15 + 150, 60));
	pcdProGress->setPercentage(0);
	this->addChild(pcdProGress, 3, 230); //tag����Ϊ230

}

void Game::get_skill_cd0(float dt)
{
	auto progress = (ProgressTimer *)getChildByTag(200);
	progress->setPercentage((cd0 / cdmax) * 100);

	if (cd0>=cdmax)
	{
		if_skill_kind_0 = true;
	}
	if (cd0<cdmax)
	{
		cd0 += 10.000;
	}
}

void Game::get_skill_cd1(float dt)
{
	auto progress = (ProgressTimer *)getChildByTag(210);
	progress->setPercentage((cd1 / cdmax) * 100);
	if (cd1>=cdmax)
	{
		if_skill_kind_1 = true;
	
	}
	if (cd1<cdmax)
	{
		cd1 += 10.000;
	}
}

void Game::get_skill_cd2(float dt)
{
	auto progress = (ProgressTimer *)getChildByTag(220);
	progress->setPercentage((cd2 / cdmax) * 100);
	if (cd2>=cdmax)
	{
		v_my=70;
		if_skill_kind_2 = true;
	}
	if (cd2<cdmax)
	{
		cd2 += 10.000;
	}
}

void Game::get_skill_cd3(float dt)
{
	auto progress = (ProgressTimer *)getChildByTag(230);
	progress->setPercentage((cd3 / cdmax) * 100);
	if (cd3>=cdmax)
	{
	
		if_skill_kind_3 = true;
	}
	if (cd3<cdmax)
	{
		cd3 += 10.000;
	}
}


void Game::get_time()
{
	Label *labeltime = Label::createWithTTF(timecheck, "fonts/Marker Felt.ttf", 60);
	labeltime->setPosition(Vec2(1140, 50));
	this->addChild(labeltime, 3, 300);//!!!!!!!!!!!!

}

void Game::get_timename()
{
	Label *labeltime1 = Label::createWithTTF("Get Time By Seconds:", "fonts/Marker Felt.ttf", 60);
	labeltime1->setPosition(Vec2(880, 55));
	this->addChild(labeltime1, 3, 310);//!!!!!!!!!!!!

}

#ifndef __GameOnline_SCENE_H__
#define __GameOnline_SCENE_H__

#include "cocos2d.h"
#include"Tower.h"
#include "Hero.h"
#include"Bing.h"
#include"HeroAi.h"
#include"Jidi.h"
#include"TowerEnemy.h"
#include"JidiEnemy.h"
#include"BingEnemy.h"
#include"zhanji.h"
#include"coin.h"
#include"client.h"
USING_NS_CC;


class GameOnline : public cocos2d::Scene
{


public:
	static cocos2d::Scene* createScene();

	cocos2d::Vec2 runto_enemy;
	cocos2d::Vec2 runto_my;

	cocos2d::TMXLayer* pengzhuang_layer;
	cocos2d::TMXTiledMap* tile_map;

	Vector<Bing*> bing_my_vector;
	Vector<BingEnemy*> bing_enemy_vector;
	
	int  skill_kind;
	int bing_enemy_tag ;
	int bing_my_tag ;
	std::string timecheck ;
	std::string level;
	std::string levelenemy;
	float cdmax, cd1, cd2, cd3, cd0;

	int equip[9];
	int equipNumber;

	float power_fa_my, power_wu_my, power_wu_enemy, power_fa_enemy, defence_fa_my, defence_fa_enemy, defence_wu_my, defence_wu_enemy, hp_my, hp_enemy, v_my, v_enemy;

	//cocos2d::Sprite* hero_my;
	//cocos2d::Sprite* hero_enemy;

	Hero* hero_my_ol;

	Hero* hero_enemy_ol;

	TowerEnemy* tower_enemy_ol;
	Tower* tower_my_ol;

	JidiEnemy* jidi_enemy_ol;
	Jidi* jidi_my_ol;


	BingEnemy* bing_enemy_ol;
	Bing* bing_my_ol;

	Coin* coin_my_ol;
	Zhanji* zhanji_my_ol;

	cocos2d::Vec2 last_position;
	cocos2d::Vec2 touch_position;

	cocos2d::Vec2 tileCoordFromPosition(cocos2d::Vec2 position);

	bool  is_walk;

	bool if_attack=false;


	virtual bool GameOnline::init();


	bool touchBegin(cocos2d::Touch* touch, cocos2d::Event* event);
	bool kb_begin(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	//void pengzhuang_check();
	bool if_peng_stop();
	void get_coin_by_time(float dt);

	void set_hero_map();
	void set_jianting();

	void updateView(float dt);

	void rect_check_tower(float dt);
	void rect_check_bing(float dt);
	void rect_check_heroAi(float dt);

	void bing_shuaxin(float dt);

	void hurt_check(float dt);
	void hurt_check_hero_my();
	void hurt_check_hero_enemy();
	void hurt_check_bing_my();
	void hurt_check_bing_enemy();
	void hurt_check_tower_my();
	void hurt_check_tower_enemy();
	void hurt_check_jidi_my();
	void hurt_check_jidi_enemy();



	void hero_attack(Vec2 attack, Vec2 attacked);

	bool rect_contain_bing_enemy(Bing* it_attack);
	bool rect_contain_bing_my(BingEnemy* it_attack);
	bool Hero_ai_attack_bing(HeroAi* heri_ai);

	Animate* animate_dead(int tag, int direction);

	void tower_attack_enemy();
	void tower_attack_my();

	void bing_attack_enemy();
	void bing_attack_my();
	void heroAi_attack();
	void EXP_change(float dt);

	void EXP_check(float dt);
	////////////////商店
	void menuShopItemOpenCallback(cocos2d::Ref* pSender);         //商店按钮的回调函数
	void menuShopItemCloseCallback(cocos2d::Ref* pSender);        //商店按钮的回调函数
	void buyDaoCallBack1(cocos2d::Ref* pSender);
	void buyDaoCallBack2(cocos2d::Ref* pSender);
	void buyDaoCallBack3(cocos2d::Ref* pSender);

	void buyDunCallBack1(cocos2d::Ref* pSender);
	void buyDunCallBack2(cocos2d::Ref* pSender);

	void buyGongCallBack1(cocos2d::Ref* pSender);
	void buyGongCallBack2(cocos2d::Ref* pSender);

	void buyKaiCallBack1(cocos2d::Ref* pSender);
	void buyKaiCallBack2(cocos2d::Ref* pSender);
	void buyKaiCallBack3(cocos2d::Ref* pSender);
	void buyKaiCallBack4(cocos2d::Ref* pSender);
	void buyKaiCallBack5(cocos2d::Ref* pSender);

	void buyShoeCallBack1(cocos2d::Ref* pSender);
	void buyShoeCallBack2(cocos2d::Ref* pSender);
	void buyShoeCallBack3(cocos2d::Ref* pSender);

	void buyFaCallBack1(cocos2d::Ref* pSender);
	void buyFaCallBack2(cocos2d::Ref* pSender);


	void buyDaoOkBack1(cocos2d::Ref* pSender);
	void buyDaoOkBack2(cocos2d::Ref* pSender);
	void buyDaoOkBack3(cocos2d::Ref* pSender);

	void buyDunOkBack1(cocos2d::Ref* pSender);
	void buyDunOkBack2(cocos2d::Ref* pSender);

	void buyGongOkBack1(cocos2d::Ref* pSender);
	void buyGongOkBack2(cocos2d::Ref* pSender);

	void buyKaiOkBack1(cocos2d::Ref* pSender);
	void buyKaiOkBack2(cocos2d::Ref* pSender);
	void buyKaiOkBack3(cocos2d::Ref* pSender);
	void buyKaiOkBack4(cocos2d::Ref* pSender);
	void buyKaiOkBack5(cocos2d::Ref* pSender);

	void buyShoeOkBack1(cocos2d::Ref* pSender);
	void buyShoeOkBack2(cocos2d::Ref* pSender);
	void buyShoeOkBack3(cocos2d::Ref* pSender);

	void buyFaOkBack1(cocos2d::Ref* pSender);
	void buyFaOkBack2(cocos2d::Ref* pSender);


	bool if_skill_kind_1;
	bool if_skill_kind_2;
	bool if_skill_kind_3;
	bool if_skill_kind_0;
	void get_skill0();
	void get_skill1();
	void get_skill2();
	void get_skill3();
	void get_skill_cd0(float dt);
	void get_skill_cd1(float dt);
	void get_skill_cd2(float dt);
	void get_skill_cd3(float dt);
	void get_time();
	void get_timename();
	void get_time_change(float dt);
	//void Restart_my(float dt);
	//void Restart_enemy(float dt);


	int  skill_kind1;
	int bing_enemy_tag1 = 0;
	int bing_my_tag1 = 0;
	std::string timecheck1 = "0";
	std::string level1 = "0";
	std::string levelenemy1 = "0";
	std::string death_num1 = "0";
	std::string kill_num1 = "0";
	float cdmax1, cd11, cd21, cd31, cd01;

	float power_fa_my1, power_wu_my1, power_wu_enemy1, power_fa_enemy1, defence_fa_my1, defence_fa_enemy1, defence_wu_my1, defence_wu_enemy1, hp_my1, hp_enemy1, v_my1, v_enemy1;



	client* client1;
	bool if_begin;
	int id=0;
	float tx;//位置x
	float ty;//位置y
	int hp;//血量
	int dengji;//等级
	int gongji;//攻击力
	int yisu;//移速
	int hp_max=4000;
	int skill=0;//技能类型

	/*float tx_recv;//位置x
	float ty_recv;//位置y
	int hp_recv;//血量
	int dengji_recv;//等级
	int gongji_recv;//攻击力
	int yisu_recv;//移速
	int skill_recv = 0;*///技能类型


	char data_s[64] = { 0 };
	char data_r[64] = { 0 };
	void qingling();
	//void to_int(char arr[]);
	char* to_char();
	void Lian_wang(float dt);
	void set_hero_map_0();

	void check_begin(float dt);
	void jieshou_message();


	CREATE_FUNC(GameOnline);
};


#endif // __Setting_SCENE_H__#pragma once;
#pragma once

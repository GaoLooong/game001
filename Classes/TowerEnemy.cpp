#include"TowerEnemy.h"


void TowerEnemy::init_tower(int x, int y)
{
	tower = Sprite::create("tower/tower.png");
	tower->setPosition(Vec2(x, y));

	rect_attack = Rect(tower->getPositionX() - 200, tower->getPositionY() - 200, 400, 400);
	rect_attacked = Rect(tower->getPositionX() - 100, tower->getPositionY() - 100, 200, 200);
	hpLimit = hp_now = 1500;
	is_attacking = false;
	if_check_hurt = true;
	this->getBloodbar(tower, hpLimit, hp_now);
	addChild(tower);
}

Vec2 TowerEnemy::get_position()
{
	Vec2 tower_position = tower->getPosition();
	return tower_position;
}
Rect TowerEnemy::get_attack_rect()
{
	rect_attack = Rect(tower->getPositionX() - 200, tower->getPositionY() - 200, 400, 400);
	return rect_attack;
}
Rect TowerEnemy::get_attacked_rect()
{
	rect_attacked = Rect(tower->getPositionX() - 60, tower->getPositionY() - 60, 120, 120);
	return rect_attacked;
}
Sprite*TowerEnemy::get_sprite()
{
	return tower;
}
void TowerEnemy::getBloodbar(Sprite* tower, float hpLimit, float hp_now)
{
	Sprite *pBloodbeiSp = Sprite::create("on.png");
	pBloodbeiSp->setPosition(Vec2(tower->getPositionX() - 600, tower->getPositionY() - 300));
	tower->addChild(pBloodbeiSp);
	Sprite *pBloodqianSp = Sprite::create("off.png");
	ProgressTimer *pBloodProGress = ProgressTimer::create(pBloodqianSp);
	pBloodProGress->setType(kCCProgressTimerTypeBar);
	pBloodProGress->setBarChangeRate(Vec2(1, 0));
	pBloodProGress->setMidpoint(Vec2(0, 0.5));
	pBloodProGress->setPosition(Vec2(tower->getPositionX() - 600, tower->getPositionY() - 300));
	pBloodProGress->setPercentage(100);
	tower->addChild(pBloodProGress, 1, 110); //血条tag设置为110

}
void TowerEnemy::hurt_check()
{

	hp_now;
	auto progress = (ProgressTimer*)tower->getChildByTag(110);
	progress->setPercentage((hp_now / hpLimit) * 100); //

	if (progress->getPercentage() < 0)
	{
		if_check_hurt = false;

	}
}
void TowerEnemy::attack(Sprite* enemy)
{
	Sprite* attacked = enemy;
	Vec2 current_position = attacked->getPosition();
	Vec2 tower_position = this->get_position();
	Vec2 diff = tower_position - current_position;//是触点和和hero之间的距离
	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(this->get_position());
	addChild(skill);
	Animation* animation = Animation::create();
	char  file_name[20] = { 0 };
	sprintf(file_name, "skill/skill_0.png");
	animation->addSpriteFrameWithFile(file_name);
	animation->setDelayPerUnit(0.1f);
	animation->setLoops(juli * 5 / iv);
	Animate* animate = Animate::create(animation);
	auto* move_action = MoveTo::create(juli / iv, attacked->getPosition());
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);
}


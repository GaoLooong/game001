#pragma once
#include <iostream>
#include <cstdio>
#include <Winsock2.h>
#include <cstring>
#include<vector>
#include<string>
#include"cocos2d.h"
using namespace std;
#pragma comment(lib, "ws2_32.lib")

#define SIZE 64

extern int tx_recv, ty_recv, hp_recv, dengji_recv, gongji_recv, yisu_recv, skill_recv,skill,hp_max_recv;
extern bool if_move;
class client
{
public:
	client();
	~client();
	int Sending(char Message[SIZE]);//传递信息函数
	char* get_recv();
	int get_id();
	char bufRecv[64];
	DWORD  threadId;
	int ret;
	int IfStart = 0;
private:
	SOCKET ClientSock;                //客户端套接字
	SOCKADDR_IN ClientSock_Addr;      //客户端地址

};
extern client Cli;

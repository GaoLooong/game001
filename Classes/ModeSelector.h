#pragma once
#ifndef _ModeSelector_H_
#define _ModeSelector_H_

#include "cocos2d.h"

USING_NS_CC;

class Mode : public cocos2d::Scene
{
	bool _isAI;

public:
	static cocos2d::Scene* createScene();

	virtual bool init();


	void setAI() { _isAI = true; }
	void setHuman() { _isAI = false; }
	bool isAI() const { return _isAI; }

	void menuReadyCallback(cocos2d::Ref* pSender);


	CREATE_FUNC(Mode);

	void ListenIfStart(float dt);
};

#endif // !_ModeSelector_H_

#include "HeroAi.h"

void HeroAi::init_hero_ai(int x, int y, float runto_x, float runto_y)
{
	hero_ai = Sprite::create("hero/hero.png");
	hero_ai->setPosition(Vec2(x, y));

	//iv = 80;//iv是移动速度

	runto_position.x = runto_x;
	runto_position.y = runto_y;
	//hpLimit = hp_now = 1000;

	rect_attack = Rect(hero_ai->getPositionX() - 200, hero_ai->getPositionY() - 200, 400, 400);
	rect_attacked = Rect(hero_ai->getPositionX() - 40, hero_ai->getPositionY() - 40, 80, 80);

	is_attacking = false;
	if_check_hurt = true;
	direction = this->get_direction(runto_x - x, runto_y - y);
	this->getBloodbar(hero_ai, hpLimit, hp_now);

	addChild(hero_ai);
}

Vec2 HeroAi::get_position()
{
	Vec2 heroai_position = hero_ai->getPosition();
	return heroai_position;
}

int HeroAi::get_direction(int x, int y)//获得 行走的 方向
{
	/////////////////1-8是从12点方向顺时针//////////////
	int idirection = 0;
	if (x == 0 || y == 0)
		idirection = 1;
	else if (x > 0 && x * x / (y * y) > 10)		//RIGHT
		idirection = 3;
	else if (x < 0 && x * x / (y * y)>10)			//LEFT
		idirection = 7;
	else if (y > 0 && y * y / (x * x) > 10)			//UP
		idirection = 1;
	else if (y < 0 && y * y / (x * x)  >10)			//DOWN
		idirection = 5;
	else if (x > 0 && y < 0)					    //RIGHT-DOWN
		idirection = 4;
	else if (x < 0 && y < 0)					   //LEFT-DOWN
		idirection = 6;
	else if (x < 0 && y > 0)			          //LEFT-UP
		idirection = 8;
	else if (x > 0 && y > 0)					 //RIGHT-UP
		idirection = 2;

	direction = idirection;
	return idirection;
}

Animate* HeroAi::animate_walk(int idirection, const char* action, float time)//建立动作类动画
{
	Animation* animation = Animation::create();

	if (action == "run")//奔跑时的动画
	{
		char  file_name[30] = { 0 };
		for (int i = 0; i < 8; i++)
		{
			sprintf(file_name, "hero/run_%d_%d.png", idirection, i);
			animation->addSpriteFrameWithFile(file_name);
		}
	}
	else//八个方向上的最后站立
	{
		char  file_name[30] = { 0 };
		sprintf(file_name, "hero/stop_%d.png", idirection);
		animation->addSpriteFrameWithFile(file_name);
	}

	animation->setDelayPerUnit(0.1f);
	if (action == "stop")
		animation->setLoops(1);
	else
		animation->setLoops(time / 0.8);//几张图片的走路动画就/0.1*几

	Animate* animate = Animate::create(animation);
	return animate;
}

void HeroAi::run(Vec2 position)
{
	hero_ai->stopAllActions();
	Vec2 heroai_position = hero_ai->getPosition();
	Vec2 diff = position - heroai_position;
	float juli = sqrt(diff.x * diff.x + diff.y * diff.y);

	auto* move_action = MoveTo::create(juli / iv, position);
	auto* run_animate = animate_walk(get_direction(diff.x, diff.y), "run", juli / iv);
	auto* stop_animate = animate_walk(get_direction(diff.x, diff.y), "stop", 0);
	auto* sequence = Sequence::create(move_action, stop_animate, NULL);
	hero_ai->runAction(run_animate);
	hero_ai->runAction(sequence);
}


void HeroAi::auto_runto()
{
	hero_ai->stopAllActions();
	run(runto_position);
}

Rect HeroAi::get_attack_rect()
{
	rect_attack = Rect(hero_ai->getPositionX() - 200, hero_ai->getPositionY() - 200, 400, 400);
	return rect_attack;
}
Rect HeroAi::get_attacked_rect()
{
	rect_attacked = Rect(hero_ai->getPositionX() - 40, hero_ai->getPositionY() - 40, 80, 80);
	return rect_attacked;
}

Sprite* HeroAi::get_sprite()
{
	return hero_ai;
}
void HeroAi::getBloodbar(Sprite* hero_ai, float hpLimit, float hp_now)
{
	Sprite *pBloodbeiSp = Sprite::create("pbeiSp.png");
	pBloodbeiSp->setPosition(Vec2(hero_ai->getPositionX() - 1450, hero_ai->getPositionY() -640));
	hero_ai->addChild(pBloodbeiSp);
	Sprite *pBloodqianSp = Sprite::create("xuetiao.png");
	ProgressTimer *pBloodProGress = ProgressTimer::create(pBloodqianSp);
	pBloodProGress->setType(kCCProgressTimerTypeBar);
	pBloodProGress->setBarChangeRate(Vec2(1, 0));
	pBloodProGress->setMidpoint(Vec2(0, 0.5));
	pBloodProGress->setPosition(Vec2(hero_ai->getPositionX() - 1450, hero_ai->getPositionY() - 640));
	pBloodProGress->setPercentage(100);
	hero_ai->addChild(pBloodProGress, 1, 110); //血条tag设置为110

}
void HeroAi::hurt_check()
{

	hp_now;
	auto progress = (ProgressTimer*)hero_ai->getChildByTag(110);
	progress->setPercentage((hp_now /hpLimit) * 100); //

	if (progress->getPercentage() < 0)
	{
		if_check_hurt = false;
	}
}

void HeroAi::attack(Sprite* enemy, Rect rect)
{
	Sprite* attacked = enemy;
	Rect  rect_attacked = rect;
	Vec2 attacked_position = attacked->getPosition();
	Vec2 hero_position = this->get_position();
	Vec2 diff = attacked_position - hero_position;//是触点和和hero之间的距离

	//bing_enemy->run(diff);

	float iv = 500;//iv是移动速度
	float juli = sqrt(pow(diff.x, 2) + pow(diff.y, 2));

	if (rect_attacked.containsPoint(this->get_position()))//如果进入了对方受攻击范围,停止
	{
		hero_ai->stopAllActions();
		auto* stop = this->animate_walk(this->get_direction(diff.x, diff.y), "stop", 0);
		this->get_sprite()->runAction(stop);

	}
	else
	{
		hero_ai->stopAllActions();
		this->run(attacked_position);
	}

	Sprite* skill = Sprite::create("skill/skill_0.png");
	skill->setPosition(hero_position);
	addChild(skill);
	Animation* animation = Animation::create();
	char  file_name[20] = { 0 };
	sprintf(file_name, "skill/skill_0.png");
	animation->addSpriteFrameWithFile(file_name);
	animation->setDelayPerUnit(0.1f);
	animation->setLoops(juli * 5 / iv);
	Animate* animate = Animate::create(animation);
	auto* move_action = MoveTo::create(juli / iv, attacked_position);
	auto* xiaoshi = CallFunc::create([skill, this]() {skill->removeFromParent(); });
	auto* sequence = Sequence::create(move_action, xiaoshi, NULL);

	skill->runAction(animate);
	skill->runAction(sequence);

}


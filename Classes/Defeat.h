#ifndef _DEFEAT_H__
#define _DEFEAT_H__
#include"cocos2d.h"

class DefeatScene :public cocos2d::Scene
{
public:
	static cocos2d::Scene* createDefeatScene();

	virtual bool init();

	void OkButtonCallBack(cocos2d::Ref* pSender);

	CREATE_FUNC(DefeatScene);
};
#endif // !_DEFEAT_H__


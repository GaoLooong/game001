#ifndef _VICTORY_H__
#define _VICTORY_H__
#include"cocos2d.h"

class VictoryScene :public cocos2d::Scene
{
public:
	static cocos2d::Scene* createVictoryScene();

	virtual bool init();

	void OkButtonCallBack(cocos2d::Ref* pSender);

	CREATE_FUNC(VictoryScene);
};
#endif // _VICTORY_H__
#pragma once
#ifndef _ZHANJI_H_
#define _ZHANJI_H_

#include"cocos2d.h"
#include"Hero.h"
#include"HeroAi.h"
#include <string>

class Zhanji :public cocos2d::CCNode
{
public:
	Sprite* zhanji;
	void init_zhanji(int x, int y);
	Vec2 Zhanji::get_position();
	std::string death;
	std::string kill;
	void get_zhanji(std::string kill, std::string death);
	CREATE_FUNC(Zhanji);
};
#endif // !_ZHANJI_H_


#include "client.h"
#include"cocos2d.h"
#include "GameOnline.h"
#pragma warning(disable:4996)
#pragma comment(lib, "ws2_32.lib")

USING_NS_CC;
char data_recv[64];
int ID = 0;

int tx_recv = 0, ty_recv = 0, hp_recv = 0, dengji_recv = 0, gongji_recv = 0, yisu_recv = 0, skill_recv = 0,hp_max_recv=4000;
bool if_move = false;

void to_int(char arr[])
{
	int i, i1;
	char* ptr;
	char c_weizhi[16] = { 0 };
	if (arr[0] != 0)
	{
		for (i1 = 0, i = 0; arr[i] != '|'; i1++, i++)//位置x
		{
			c_weizhi[i] = arr[i];
		}
		tx_recv = strtod(c_weizhi, &ptr);

		char c_weizhi2[16] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//位置y
		{
			c_weizhi2[i1] = arr[i];
		}
		ty_recv = strtod(c_weizhi2, &ptr);

		char c[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//血量
		{
			c[i1] = arr[i];
		}
		hp_recv = strtod(c, &ptr);

		char c2[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//等级
		{
			c2[i1] = arr[i];
		}
		dengji_recv = strtod(c2, &ptr);

		char c3[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//移速
		{
			c3[i1] = arr[i];
		}
		yisu_recv = strtod(c3, &ptr);

		char c4[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//移速
		{
			c4[i1] = arr[i];
		}
		gongji_recv = strtod(c4, &ptr);

		char c5[6] = { 0 };
		for (i++, i1 = 0; arr[i] != '|'; i1++, i++)//移速
		{
			c5[i1] = arr[i];
		}
		hp_max_recv = strtod(c5, &ptr);

		i++;
		skill_recv = arr[i] - 48;
	}//技能类型

}
DWORD WINAPI ThreadFunc(LPVOID p)//线程函数,添加子线程
{
	SOCKET*ClientSock = (SOCKET*)p;
	while (true)
	{
		char bufrecv[64];
		memset(bufrecv, '/0', 64);
		recv(*ClientSock, bufrecv, sizeof(char) * strlen(bufrecv), 0);
		if (bufrecv[0] != 0)
		{
			if_move = true;
			to_int(bufrecv);
		}
	}
	closesocket(*ClientSock);   //关闭套接字
	WSACleanup();           //释放套接字资源;
}
client::client()
{

	IfStart = 0;
	ID = 0;
	WSADATA wsd;
	WSAStartup(MAKEWORD(2, 2), &wsd);
	if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0)
	{
		cout << "WSAStartup failed!" << endl;
	}
	ClientSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == ClientSock)
	{
		cout << "socket failed!" << endl;
		WSACleanup();//释放套接字资源
	}
	ClientSock_Addr.sin_family = AF_INET;
	ClientSock_Addr.sin_port = htons((short)4999);
	ClientSock_Addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(ClientSock, (LPSOCKADDR)&ClientSock_Addr, sizeof(ClientSock_Addr));
	if (SOCKET_ERROR == ret)
	{
		cout << "connect failed!" << endl;
		closesocket(ClientSock); //关闭套接字
		WSACleanup(); //释放套接字资源
	}

	if (ID == 0)
	{
		char bufrecv[64];
		memset(bufrecv, '/0', 64);
		recv(ClientSock, bufrecv, sizeof(char) * strlen(bufrecv), 0);
		if (bufrecv[0] == 'F')
			ID = 1;
		else if (bufrecv[0] == 'S')
			ID = 2;
	}
	CreateThread(NULL, 0, &ThreadFunc, &ClientSock, 0, &threadId);

}

client::~client()
{
	closesocket(ClientSock);
	WSACleanup();
	return;
}

int client::Sending(char Message[64])
{
	ret = send(ClientSock, Message, strlen(Message), 0);
	if (SOCKET_ERROR == ret)
	{
		cout << "send failed!" << endl;
		closesocket(ClientSock); //关闭套接字
		WSACleanup(); //释放套接字资源
		return -1;
	}

	return 0;
}

char*client::get_recv()
{
	return data_recv;
}
int client::get_id()
{
	return ID;
}

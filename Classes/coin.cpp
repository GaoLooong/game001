#include "coin.h"

void Coin::init_coin(int x, int y)
{
	coin = Sprite::create("coin.png");
	coin->setPosition(Vec2(x, y));
	number = "0";
	this->get_coin(number);
	addChild(coin);
}

void Coin::get_coin(std::string number)
{
	Label *label = Label::createWithTTF(number, "fonts/Marker Felt.ttf", 30);
	label->setPosition(Vec2(60, 20));
	coin->addChild(label, 1, 110);
	
}
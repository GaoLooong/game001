#include "cocos2d.h"

USING_NS_CC;

class TowerEnemy :public cocos2d::CCNode
{
public:
	Sprite* tower;
	cocos2d::Sprite* enemy_hero;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	Sprite* mubiao_sprite;

	float hpLimit;//最大血量
	float hp_now;//现在血量
	bool is_attacking;
	bool if_check_hurt;
	int mubiao;
	void init_tower(int x, int y);//用来初始化防御塔

	Sprite* TowerEnemy::get_sprite();
	Rect TowerEnemy::get_attack_rect();
	Rect TowerEnemy::get_attacked_rect();
	void TowerEnemy::getBloodbar(Sprite* tower, float hpLimit, float hp_now);
	Vec2 TowerEnemy::get_position();
	void TowerEnemy::hurt_check();
	void TowerEnemy::attack(Sprite* enemy);
	CREATE_FUNC(TowerEnemy);
};
#ifndef _HEROAI_H_
#define _HEROAI_H_ 



#include "cocos2d.h"

USING_NS_CC;

class HeroAi :public cocos2d::CCNode
{
public:
	Sprite* hero_ai;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	cocos2d::Vec2 runto_position;
	int mubiao;
	int direction;
	Sprite* mubiao_sprite;
	bool is_attacking;
	bool if_check_hurt;
	float iv;
	bool HeroAi::ifDead();
	float hpLimit;//最大血量
	float hp_now;//现在血量
	void HeroAi::init_hero_ai(int x, int y, float runto_x, float runto_y);//用来初始化防御塔
	void HeroAi::auto_runto();
	Vec2 HeroAi::get_position();
	Rect HeroAi::get_attack_rect();
	Rect HeroAi::get_attacked_rect();
	Sprite* HeroAi::get_sprite();
	int HeroAi::get_direction(int x, int y);
	Animate* HeroAi::animate_walk(int direction, const char* action, float time);
	void getBloodbar(Sprite* hero_ai, float hpLimit, float hp_now);
	void HeroAi::run(Vec2 position);
	void HeroAi::hurt_check();
	void HeroAi::attack(Sprite*enemy,Rect rect);

	CREATE_FUNC(HeroAi);
};
#endif //  _HEROAI_H_


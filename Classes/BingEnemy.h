#pragma once
#include "cocos2d.h"

USING_NS_CC;

class BingEnemy :public cocos2d::CCNode
{
public:
	Sprite* bing;
	cocos2d::Rect rect_attack;
	cocos2d::Rect rect_attacked;
	cocos2d::Vec2 runto_position;
	int mubiao;
	int direction;
	Sprite* mubiao_sprite;
	bool is_attacking;
	bool if_check_hurt;
	float iv;
	int tag;

	void BingEnemy::init_bing(int x, int y, float runto_x, float runto_y,int itag);//用来初始化防御塔
	void BingEnemy::auto_runto();
	Vec2 BingEnemy::get_position();
	Rect BingEnemy::get_attack_rect();
	Rect BingEnemy::get_attacked_rect();
	float hpLimit;//最大血量
	float hp_now;
	Sprite* BingEnemy::get_sprite();
	int BingEnemy::get_direction(int x, int y);
	Animate* BingEnemy::animate_walk(int direction, const char* action, float time);
	void BingEnemy::run(Vec2 diff);
	void BingEnemy::getBloodbar(Sprite* bing, float hpLimit, float hp_now);
	void BingEnemy::hurt_check();
	void BingEnemy::attack(Sprite* enemy,Rect rect);
	Animate* BingEnemy::animate_dead();

	CREATE_FUNC(BingEnemy);
};